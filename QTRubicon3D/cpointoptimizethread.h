#ifndef CPOINTOPTIMIZETHREAD_H
#define CPOINTOPTIMIZETHREAD_H

#include <QThread>
#include <QMutex>

class CPointCloud;

class CPointOptimizeThread : public QThread
{
    Q_OBJECT
public:
    explicit CPointOptimizeThread(QObject *parent = 0);
    ~CPointOptimizeThread();

    void start_process(CPointCloud *, float);

signals:
    void send_back(const int val);
    void send_result(CPointCloud *);

public slots:
    void stop_process();

protected:
    void run();

private:
    bool    _abort;
    QMutex  _mutex;
    float _dist_thres;
    CPointCloud * _in_cloud;

};

#endif // CPOINTOPTIMIZETHREAD_H

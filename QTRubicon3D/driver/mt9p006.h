#ifndef __MT9P006_H__
#define __MT9P006_H__

#include "types.h"

#define MT9P006_PIXEL_ARRAY_WIDTH			2752
#define MT9P006_PIXEL_ARRAY_HEIGHT			2004

#define MT9P006_CHIP_VERSION				0x00
#define		MT9P006_CHIP_VERSION_VALUE		0x1801
#define MT9P006_ROW_START				0x01
#define		MT9P006_ROW_START_MIN			0
#define		MT9P006_ROW_START_MAX			2004
#define		MT9P006_ROW_START_DEF			54
#define MT9P006_COLUMN_START				0x02
#define		MT9P006_COLUMN_START_MIN		0
#define		MT9P006_COLUMN_START_MAX		2750
#define		MT9P006_COLUMN_START_DEF		16
#define MT9P006_WINDOW_HEIGHT				0x03
#define		MT9P006_WINDOW_HEIGHT_MIN		2
#define		MT9P006_WINDOW_HEIGHT_MAX		2006
#define		MT9P006_WINDOW_HEIGHT_DEF		1944
#define MT9P006_WINDOW_WIDTH				0x04
#define		MT9P006_WINDOW_WIDTH_MIN		2
#define		MT9P006_WINDOW_WIDTH_MAX		2752
#define		MT9P006_WINDOW_WIDTH_DEF		2592
#define MT9P006_HORIZONTAL_BLANK			0x05
#define		MT9P006_HORIZONTAL_BLANK_MIN		0
#define		MT9P006_HORIZONTAL_BLANK_MAX		4095
#define MT9P006_VERTICAL_BLANK				0x06
#define		MT9P006_VERTICAL_BLANK_MIN		0
#define		MT9P006_VERTICAL_BLANK_MAX		4095
#define		MT9P006_VERTICAL_BLANK_DEF		25
#define MT9P006_OUTPUT_CONTROL				0x07
#define		MT9P006_OUTPUT_CONTROL_CEN		2
#define		MT9P006_OUTPUT_CONTROL_SYN		1
#define		MT9P006_OUTPUT_CONTROL_DEF		0x1f82
#define MT9P006_SHUTTER_WIDTH_UPPER			0x08
#define MT9P006_SHUTTER_WIDTH_LOWER			0x09
#define		MT9P006_SHUTTER_WIDTH_MIN		1
#define		MT9P006_SHUTTER_WIDTH_MAX		1048575
#define		MT9P006_SHUTTER_WIDTH_DEF		1943
#define	MT9P006_PLL_CONTROL				0x10
#define		MT9P006_PLL_CONTROL_PWROFF		0x0050
#define		MT9P006_PLL_CONTROL_PWRON		0x0051
#define		MT9P006_PLL_CONTROL_USEPLL		0x0052
#define	MT9P006_PLL_CONFIG_1				0x11
#define	MT9P006_PLL_CONFIG_2				0x12
#define MT9P006_PIXEL_CLOCK_CONTROL			0x0a
#define MT9P006_FRAME_RESTART				0x0b
#define MT9P006_SHUTTER_DELAY				0x0c
#define MT9P006_RST					0x0d
#define		MT9P006_RST_ENABLE			1
#define		MT9P006_RST_DISABLE			0
#define MT9P006_READ_MODE_1				0x1e
#define MT9P006_READ_MODE_2				0x20
#define		MT9P006_READ_MODE_2_ROW_MIR		(1 << 15)
#define		MT9P006_READ_MODE_2_COL_MIR		(1 << 14)
#define		MT9P006_READ_MODE_2_ROW_BLC		(1 << 6)
#define MT9P006_ROW_ADDRESS_MODE			0x22
#define MT9P006_COLUMN_ADDRESS_MODE			0x23
#define MT9P006_GLOBAL_GAIN				0x35
#define		MT9P006_GLOBAL_GAIN_MIN			8
#define		MT9P006_GLOBAL_GAIN_MAX			1024
#define		MT9P006_GLOBAL_GAIN_DEF			8
#define		MT9P006_GLOBAL_GAIN_MULT		(1 << 6)
#define MT9P006_ROW_BLACK_DEF_OFFSET			0x4b
#define MT9P006_TEST_PATTERN				0xa0
#define		MT9P006_TEST_PATTERN_SHIFT		3
#define		MT9P006_TEST_PATTERN_ENABLE		(1 << 0)
#define		MT9P006_TEST_PATTERN_DISABLE		(0 << 0)
#define MT9P006_TEST_PATTERN_GREEN			0xa1
#define MT9P006_TEST_PATTERN_RED			0xa2
#define MT9P006_TEST_PATTERN_BLUE			0xa3
#define MT9P006_TEST_PATTERN_BAR_WIDTH 0xa4

#define MT9P006_GREEN_1_GAIN		0x2b
#define MT9P006_BLUE_GAIN		0x2c
#define MT9P006_RED_GAIN		0x2d
#define MT9P006_GREEN_2_GAIN		0x2e

//#define MT9P006_DEBUG
#undef MT9P006_DEBUG
#ifdef MT9P006_DEBUG
#define DPRINTK_DRIVER(format, ...)                             \
        printk(KERN_INFO "_MT9P006_DRIVER: \n" format, ## __VA_ARGS__)
#else
#define DPRINTK_DRIVER(format, ...)
#endif

struct mt9p006_pll_divs
{
  u32 ext_freq;
  u32 target_freq;
  u8 m;
  u8 n;
  u8 p1;
};

struct mt9p006_frame_size
{
  u16 width;
  u16 height;
};

enum mt9p006_image_size
{
  VGA_BIN_30FPS,
  HDV_720P_30FPS,
  HDV_1080P_30FPS,
  MT9P006_THREE_MP,
  MT9P006_FIVE_MP,
};

#endif // --MT9P006_H_

#include "fr101.h"
#include "mt9p006.h"
#include <stdio.h>
#include <stdlib.h>

FT_HANDLE glbFtHandle;

FT_HANDLE getFtHandler(void)
{
  return glbFtHandle;
}

FT_STATUS connectToModule(void)
{
  FT_STATUS ftStatus;
  DWORD numDevs;
  FT_HANDLE ftHandle;
  FT_DEVICE_LIST_INFO_NODE *devInfo; 
  int device = -1;
  u8 Mask = 0xff;
  u8 Mode;
  u8 LatencyTimer = 16; //our default setting is 16
  int i;
  
  ftStatus = FT_CreateDeviceInfoList(&numDevs);
  if (ftStatus == FT_OK)
  {
    printf("Number of devices is %d\n", numDevs);
  }
  if (numDevs > 0)
  {
    // allocate storage for list based on numDevs 
    devInfo = (FT_DEVICE_LIST_INFO_NODE*) malloc(sizeof (FT_DEVICE_LIST_INFO_NODE) * numDevs);
    // get the device information list 
    ftStatus = FT_GetDeviceInfoList(devInfo, &numDevs);
    if (ftStatus == FT_OK)
    {
      for (i = 0; i < numDevs; i++)
      {
        int vid;
        int pid;        
        
        pid = devInfo[i].ID & 0xFFFF;
        vid = (devInfo[i].ID >> 16) & 0xFFFF;        
        
        printf("Dev %d:\n", i);
        printf(" Flags=0x%x\n", devInfo[i].Flags);
        printf(" Type=0x%x\n", devInfo[i].Type);
        printf(" ID=0x%x\n", devInfo[i].ID);
        printf(" LocId=0x%x\n", devInfo[i].LocId);
        printf(" SerialNumber=%s\n", devInfo[i].SerialNumber);
        printf(" Description=%s\n", devInfo[i].Description);
        printf(" ftHandle=0x%x\n", devInfo[i].ftHandle);
        
        if (pid == FT232H_PID  &&  vid == FT232H_VID)
        {
          device = i;
          break;
        }         
      }
    }
  }

  if (device < 0)
  {
    //
    printf("Can't find FT232H device\n");
    return FT_DEVICE_NOT_FOUND;
  }  
  
  ftStatus = FT_Open(device, &ftHandle);
  if (ftStatus != FT_OK)
  {
    // FT_Open failed return;
    printf("Can't open FT232H device\n");
    return ftStatus;
  }
  Mode = 0x00; //reset mode
  ftStatus = FT_SetBitMode(ftHandle, Mask, Mode);
  usleep(10000);
  Mode = 0x40; //Sync FIFO mode
  ftStatus = FT_SetBitMode(ftHandle, Mask, Mode);
  if (ftStatus == FT_OK)
  {
    ftStatus = FT_SetLatencyTimer(ftHandle, LatencyTimer);
    ftStatus = FT_SetUSBParameters(ftHandle, 0x10000, 0x10000);
    ftStatus = FT_SetFlowControl(ftHandle, FT_FLOW_RTS_CTS, 0, 0);
    FT_Purge(ftHandle, FT_PURGE_RX);
    //access data from here
  } 
  else
  {
    // FT_SetBitMode FAILED!
    printf("Can't set Sync FIFO Mode for FT232H device\n");
    return ftStatus;
  }

  glbFtHandle = ftHandle;  
  return FT_OK;
  
}

FT_STATUS writeMemory(FT_HANDLE ftHandle, u32 address, u16 data)
{
  u8 buffer[7];
  FT_STATUS ftStatus;
  DWORD bytes_written;
  DWORD bytes_received;
  
  buffer[0] = CMD_WRITE;
  buffer[1] = address & 0xFF;
  buffer[2] = (address >> 8) & 0xFF;
  buffer[3] = (address >> 16) & 0xFF;
  buffer[4] = (address >> 24) & 0xFF;
  buffer[5] = data & 0xFF;
  buffer[6] = (data >> 8) & 0xFF;
  
  ftStatus = FT_Write(ftHandle, buffer, 7, &bytes_written);
  if (ftStatus != FT_OK)
    return ftStatus;
  ftStatus = FT_Read(ftHandle, buffer, 1, &bytes_received);
  return ftStatus;
}

FT_STATUS readMemory(FT_HANDLE ftHandle, u32 address, u16* data)
{
  u8 buffer[5];
  FT_STATUS ftStatus;
  DWORD bytes_written;
  DWORD bytes_received;
  
  buffer[0] = CMD_READ;
  buffer[1] = address & 0xFF;
  buffer[2] = (address >> 8) & 0xFF;
  buffer[3] = (address >> 16) & 0xFF;
  buffer[4] = (address >> 24) & 0xFF;
  
  ftStatus = FT_Write(ftHandle, buffer, 5, &bytes_written);
  if (ftStatus != FT_OK)
    return ftStatus;
  ftStatus = FT_Read(ftHandle, buffer, 3, &bytes_received);
  *data = buffer[0] | (buffer[1] << 8);
  return ftStatus;  
}

FT_STATUS readBlock(FT_HANDLE ftHandle, u32 address, u8* block_buffer, u32 offset)
{
  u8 buffer[5];
  FT_STATUS ftStatus;
  DWORD bytes_written;
  DWORD bytes_received;
  DWORD rx_len;
  int cnt;
  int read_tries = 0;
  int max_read_tries = 130;


  //FILE *fp;
  //fp=fopen("./Rubicon3D_readblock_log.txt", "a+");
  //fprintf(fp, "begin readBlock\n");
  //fflush(fp);

  buffer[0] = CMD_BLOCK_READ;
  buffer[1] = address & 0xFF;
  buffer[2] = (address >> 8) & 0xFF;
  buffer[3] = (address >> 16) & 0xFF;
  buffer[4] = (address >> 24) & 0xFF;

  //fprintf(fp, "call FT_Write\n");
  //fflush(fp);
  ftStatus = FT_Write(ftHandle, buffer, 5, &bytes_written);
  if (ftStatus != FT_OK)
    return ftStatus;

  //fprintf(fp, "start read cycle\n");
  //fflush(fp);
  for (cnt = 0; cnt < BLOCK_SIZE; cnt += bytes_received)
  {

      //fprintf(fp, "handle = %x b-cnt= %d ... ", ftHandle, BLOCK_SIZE - cnt);
      //fflush(fp);

      FT_SetTimeouts(ftHandle, 2000, 0);
      ftStatus = FT_Read(ftHandle, block_buffer+ offset+ cnt, BLOCK_SIZE - cnt, &bytes_received);
      read_tries += 1;

      //fprintf(fp, "try %i: status - %i, read bytes - %i \n", read_tries, ftStatus, bytes_received);
      //fflush(fp);

      if(ftStatus == FT_OK)
      {
        if(read_tries >= max_read_tries)
        {
            //fprintf(fp, "exceeds max try and return\n");
            //fflush(fp);
            break;
            // return ftStatus;
        }
      }

      if (ftStatus != FT_OK)
      {
          //fprintf(fp, "incorrect reading\n");
          //fflush(fp);
          break;
          // return ftStatus;
      }
  }

  //fprintf(fp, "get queue status\n");
  //fflush(fp);
  ftStatus = FT_GetQueueStatus(getFtHandler(), &rx_len);

  //fprintf(fp, "call FT_Purge\n");
  //fflush(fp);

  FT_Purge(getFtHandler(), FT_PURGE_RX | FT_PURGE_TX);
  //fprintf(fp, "end readBlock\n\n");
  //fflush(fp);


  return FT_OK;
}


/* I2C status register bits
 * ------------------------ */
#define I2C_TIP        (1<<3) // transfer in progress 
#define I2C_INTR       (1<<2) // interrupt pending    
#define I2C_BUSY       (1<<1) // I2C busy             
#define I2C_LRA        (1<<0) // last received ack    

/* I2C control register bits
 * ------------------------- */
#define I2C_IRQ_ENABLE (1<<5) // interrupt enable    
#define I2C_WRITE      (1<<4) // write               
#define I2C_READ       (1<<3) // read                
#define I2C_START      (1<<2) // start               
#define I2C_STOP       (1<<1) // stop                
#define I2C_NO_ACK     (1<<0) // no ack to send         
#define I2C_ACK        (0<<0) // ack to send

/* I2C error codes
 * --------------- */
#define I2C_ENODEV     1      // no device
#define I2C_EBADACK    2      // bad acknowledge received


/* I2C Clock Divisor Standard Values
 * --------------------------------- */

/* 50 kHz */
#define I2C_SLOW   0xA3

/* 100 kHz */
#define I2C_NORMAL 0x54

/* 400 kHz */ 
#define I2C_FAST   0x15

/* wait a milisecond */
#define N_DELAY usleep(1000)

/* wait some microseconds */
#define WAIT usleep(2)

/* wait for end of transfer */
#define WAIT_FOR_EOT(i2c_base,t) { while ((t=IORD(i2c_base,I2C_STATUS_REG)) & I2C_TIP) WAIT; }

void IOWR(u32 base, u32 reg, u16 value)
{
  writeMemory(getFtHandler(), base + reg, value);
}

u16 IORD(u32 base, u32 reg)
{
  u16 value;

  readMemory(getFtHandler(), base + reg, &value);
  return value;
}

/*
 * Set Serial Clock Speed in hz 
 */
void i2c_set_speed(u32 i2c_base, int speed_in_hz)
{
  int div = CLK_FREQ / (4000 * speed_in_hz);

  if (CLK_FREQ % (4000 * speed_in_hz))
    div++;

  IOWR(i2c_base, I2C_CLKDIV_REG, (unsigned char) div);

  /* wait a milisecond */
  N_DELAY;
}

/* 
 * Set FAST mode (400 kHz)
 */
void i2c_fastmode(u32 i2c_base)
{
  i2c_set_speed(i2c_base, 400);
}

/*
 * Set NORMAL mode (100 kHz)
 */
void i2c_normal_mode(u32 i2c_base)
{
  i2c_set_speed(i2c_base, 100);
}

int i2c_double_write(u32 i2c_base, u8 index, u16 value)
{
  unsigned char t;

  IOWR(i2c_base, I2C_CAMERA_REG, index);
  IOWR(i2c_base, I2C_DATA_REG, value);
  IOWR(i2c_base, I2C_CTRL_REG, I2C_WRITE);
  N_DELAY;
  return 0;
}

int i2c_double_read(u32 i2c_base, u8 index, u16 *value)
{
  u16 rd_data;
  
  IOWR(i2c_base, I2C_CAMERA_REG, index);
  IOWR(i2c_base, I2C_CTRL_REG, I2C_READ);
  N_DELAY;
  rd_data = IORD(i2c_base, I2C_DATA_REG);
  (*value) = rd_data;
  return 0;
}

void writeCameraReg(u8 index, u16 value)
{
  i2c_double_write(I2C_BASE, index, value);
}

u16 readCameraReg(u8 index)
{
  u16 data;
  
  i2c_double_read(I2C_BASE, index, &data);
  return data;
}

void initCameraRegs(void)
{
  // set snapshot mode
  writeCameraReg(MT9P006_READ_MODE_1, 0x4006 | (1 << 8));    
  
  // test pattern enable
//  writeCameraReg(MT9P006_TEST_PATTERN, 1 | (8 << 3));
//  writeCameraReg(MT9P006_TEST_PATTERN, 0);
//  writeCameraReg(MT9P006_TEST_PATTERN_BAR_WIDTH, 15);
  usleep(50000);
}

int getSnapshotWidth(void)
{
  u16 width;
  
 // i2c_double_read(I2C_BASE, MT9P006_WINDOW_WIDTH, &width);
  width = 2592;
  return width;
}

int getSnapshotHeight(void)
{
  u16 height;
  
  height = 1944;
//  i2c_double_read(I2C_BASE, MT9P006_WINDOW_HEIGHT, &height);
  return height;
}

int getSnapshotBufferSize(void)
{
  return 3* getSnapshotWidth()* getSnapshotHeight() / 2;
}

void loadSnapshotData(u8* buffer)
{
 ///   FILE *fp;
 ///   fp=fopen("./Rubicon3D_load_log.txt", "a+");
 ///  fprintf(fp, "starting loadSnapshotData\n");
 ///   fflush(fp);

  int offset;
  int size = getSnapshotBufferSize();
  
//  for (offset = 0; offset < size; offset++)
//    buffer[offset] = 0;
  
  for (offset = 0; offset < size; offset += BLOCK_SIZE)
  {
 ///   fprintf(fp, "readBlock %d\n\n", offset);
///    fflush(fp);
    readBlock(glbFtHandle, offset, buffer, offset);
    break;
  }  

 ///   fprintf(fp, "end loadSnapshotData\n\n");
 ///   fflush(fp);
 ///   fclose(fp);


}

void doSnapshot(u8* buffer)
{
 /// FILE *fp;

  u16 data;
  int i;
  

///  fp=fopen("./Rubicon3D_snap_log.txt", "a+");

//  readMemory(glbFtHandle, MT9P_BASE, &data);
  
//  for (i = 0; i < 64; i+= 2)
//    writeMemory(glbFtHandle, i, 0);
  
  // start snapshot
///  fprintf(fp, "starting read cycle\n");
///  fprintf(fp, "call writeMemory()\n");
///  fflush(fp);
  writeMemory(glbFtHandle, MT9P_BASE, 0x0001);
  
//  readMemory(glbFtHandle, MT9P_BASE, &data);

  // wait for capture end
  do
  {
 ///     fprintf(fp, "start capture data\n");
 ///     fflush(fp);
      readMemory(glbFtHandle, MT9P_BASE, &data);
///      fprintf(fp, "end capture data\n");
///      fprintf(fp, "chek data\n");
///      fflush(fp);
  }
  while ((data & 0x0001) != 0);

///  fprintf(fp, "snapshot ready\n");
///  fflush(fp);

  loadSnapshotData(buffer);


/*
  for (i = 0; i < 64; i+= 2)
  {
    data = buffer[i] | (buffer[i+ 1] << 8);
    printf("%04x ", data);
    
    if ((i+2) % 16 == 0)
      printf("\n");
  }
 */   
 //// fprintf(fp, "snaphot data loaded\n");
////  fflush(fp);

 //// fclose(fp);
}

FT_STATUS disconnectFromModule(void)
{
  return FT_Close(glbFtHandle);
}        

void pwmSetPrescaler(u16 value)
{
  writeMemory(glbFtHandle, PWM_BASE + PWM_PRESCALER, value);
}

void pwmSetBrightness(int channel, u16 value)
{
  writeMemory(glbFtHandle, PWM_BASE + channel, value);
}

void stepMotorSetPrescaler(u16 value)
{
  writeMemory(glbFtHandle, STEP_BASE + STEP_PRESCALER, value);
}

void stepMotorSetValue(int channel, u16 value)
{
  writeMemory(glbFtHandle, STEP_BASE + channel, value);
}

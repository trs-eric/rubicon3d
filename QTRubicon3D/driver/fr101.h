#ifndef __FR101_H
#define __FR101_H

#include "ftd2xx.h"
#include "types.h"

// module main frequency, 60 MHz
#define CLK_FREQ  60000000

// addresses in module address space
#define PWM_BASE   0x10000000
#define I2C_BASE   0x10000100
#define MT9P_BASE  0x10000200
#define STEP_BASE  0x10000400

// camera I2C address (for internal use only)
#define MT9P_I2C_ADDR 0x90

// FT232H identification values, used internally to detect module on USB
#define FT232H_VID 0x0403
#define FT232H_PID 0x6014

// commands to FT232H bus interface (for internal use only)
#define CMD_READ 1
#define CMD_WRITE 2
#define CMD_BLOCK_READ 3

// size of block in packet memory transfer from module to PC memory
// must be <= than snapshot full frame size (2592 * 1944 * 12 bit)
#define BLOCK_SIZE 7558272  // in bytes

FT_HANDLE getFtHandler(void);

// low-level functions for access to internal address space of module
// address - in module internal address space
FT_STATUS writeMemory(FT_HANDLE ftHandle, u32 address, u16 data);
FT_STATUS readMemory(FT_HANDLE ftHandle, u32 address, u16* data);
// block_buffer - pointer to data buffer� to which data will be transferred
// offset - offset from buffer start (and address in internal address space of module)
FT_STATUS readBlock(FT_HANDLE ftHandle, u32 address, u8* block_buffer, u32 offset);

// I2C module internal registers
#define I2C_CAMERA_REG  0
#define I2C_CTRL_REG    2
#define I2C_DATA_REG    4
#define I2C_STATUS_REG  4
#define I2C_CLKDIV_REG  6

// Fast I2Cc mode (400 kHz) 
// u2c_base - address of I2C controller in module address space (= I2C_BASE by default)
extern void i2c_fastmode(u32 i2c_base);

// Normal I2C mode (100 kHz) 
extern void i2c_normal_mode(u32 i2c_base);

// write 16-bit value to I2C (for internal use)
// index - register number
// value - 16-bit value
// in success, returns 0
int i2c_double_write(u32 i2c_base, u8 index, u16 value);
// read 16-bit value form I2C (for internal use)
int i2c_double_read(u32 i2c_base, u8 index, u16 *value);

// write 16-bit value to camera sensor register
// index - register number
// value - 16-bit value
void writeCameraReg(u8 index, u16 value);

// read 16-bit value from camera sensor register
// index - register number
// return register value
u16 readCameraReg(u8 index);

// function must be called before any data transfer with module
FT_STATUS connectToModule(void);
// function must be called before exit from program
FT_STATUS disconnectFromModule(void);

void initCameraRegs(void);
int getSnapshotWidth(void);
int getSnapshotHeight(void);
int getSnapshotBufferSize(void);

// Format of sensor RAW data
// RAW data from sensor are 12-bit values packed to 16-bit words for compactness
// even pixels packed in two consecutive bytes :
//    Byte N :            s2              s1
//    Byte N + 1 :      next pixel s1     s3
//  s3 s2 s1 -  4-bit slices of 12-bit sensor data 
//
// odd pixels packed in two consecutive  bytes :
//
//    Byte N :           s1            previous pixel s3
//    Byte N + 1 :       S3            s2
//
// one row is 2592 pixels =  (12 / 8) * 259 2 = 3888 bytes

// auxiliary function, which transfer data from module SDRAM to buffer in PC memory
// called internally from doSnapshot()
void loadSnapshotData(u8* buffer);

// main function, starts the snapshot, waits end of exposure and 
//   transfers snapshot RAW data to buffer in PC memory
void doSnapshot(u8* buffer);

// PWM registers (offsets from PWM_BASE in module address space)
#define PWM_PRESCALER           0
#define PWM_LED1_BRIGHTNESS     2
#define PWM_LED2_BRIGHTNESS     4
#define PWM_LASER1_BRIGHTNESS   6
#define PWM_LASER2_BRIGHTNESS   8

// value from 1 to 65535
// PWM period will be value * 100 * (1/60 us)
// default value = 10 (after reset)
void pwmSetPrescaler(u16 value);

// channel - PWM_LED1_BRIGHTNESS, ... , PWM_LASER2_BRIGHTNESS
// value from 0 to 100
void pwmSetBrightness(int channel, u16 value);

// PWM registers (offsets from PWM_BASE in module address space)
#define STEP_PRESCALER           0
#define STEP_M1                  2
#define STEP_M2                  4
#define STEP_M3                  6
#define STEP_M4                  8

// value from 1 to 65535
// PWM period will be value * 100 * (1/60 us)
// default value = 10 (after reset)
void stepMotorSetPrescaler(u16 value);

// set step motor PWM output (duty cycle)
// channel - STEP_M1, STEP_M2, STEP_M3, STEP_M4
// value from 0 to 200
void stepMotorSetValue(int channel, u16 value);

#endif // __FR101_H

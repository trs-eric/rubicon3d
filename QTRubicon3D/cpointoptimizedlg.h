#ifndef CPOINTOPTIMIZEDLG_H
#define CPOINTOPTIMIZEDLG_H

#include <QDialog>

class CGLWidget;
class CPointOptimizeThread;
class CPointCloud;

namespace Ui {
class CPointOptimizeDlg;
}

class CPointOptimizeDlg : public QDialog
{
    Q_OBJECT

public:
    explicit CPointOptimizeDlg(CGLWidget * glWidget, QWidget *parent = 0);
    ~CPointOptimizeDlg();

protected:
    void showEvent(QShowEvent * event);
 //   void closeEvent(QCloseEvent * event);

private:
    Ui::CPointOptimizeDlg *ui;

    CGLWidget *_glWidget;

    float _min_dist;
    float _max_dist;
    float _mdist_value;

    bool _in_process;
    bool _on_apply;
    bool _is_result_valid;
    CPointOptimizeThread * _thread;

private slots:
    void onCancelBtn();
    void onPreviewBtn();
    void onApplyBtn();

    void onMakeProgressStep(int val);
    void onCompleteProcessing(CPointCloud * );
    void onStopProcessing();

    void slider2edit();
    void edit2slider(QString str);
    void close_dialog();


};

#endif // CPOINTOPTIMIZEDLG_H

#ifndef CMOVROTSCADLG_H
#define CMOVROTSCADLG_H

#include <QDialog>

class CGLWidget;

namespace Ui {
class CMovRotScaDlg;
}

class CTriangularMesh;

class CMovRotScaDlg : public QDialog
{
    Q_OBJECT

public:
    explicit CMovRotScaDlg(CGLWidget * glWidget, QWidget *parent = 0);
    ~CMovRotScaDlg();
protected:
    void showEvent(QShowEvent * event);
    void restore_transforms();
    CTriangularMesh * make_transforms();

    float _tx, _ty, _tz;
    float _rx, _ry, _rz;
    float _rotx, _roty, _rotz;

    bool _valid_res;

    void init_gui();
    void init_param();

    CTriangularMesh * _ref_mesh;

signals:
    void send_result(CTriangularMesh *);

private:
    Ui::CMovRotScaDlg *ui;

    CGLWidget *_glWidget;


private slots:
    void onCancelBtn();
    void onApplyBtn();
    void onPreviewBtn();


    void onValTXChanged(double );
    void onValTYChanged(double );
    void onValTZChanged(double );

    void onValRXChanged(double );
    void onValRYChanged(double );
    void onValRZChanged(double );

    void onValSXChanged(double );
    void onValSYChanged(double );
    void onValSZChanged(double );

    void onLockScale(bool val);
};

#endif // CMOVROTSCADLG_H

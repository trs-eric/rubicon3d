#include "ccameraadjustmentdlg.h"
#include "cglwidget.h"
#include "ui_ccameraadjustmentdlg.h"
#include <QMessageBox>
#include <QFileDialog>
#include<QMouseEvent>
#include "./png/lodepng.h"
#include "messages.h"
#include "./settings/psettings.h"

#include <iostream>
#include <QTime>
#include <QTimer>
#include <QThread>

#include "./img2space/edgedetection.h"
#include "./img2space/image2spacemodel.h"




#define __WINDOWS_TYPES__
extern "C"
{
#include "driver_aux_utils.h"
}


CCameraAdjustmentDlg::CCameraAdjustmentDlg(CGLWidget * glWidget, QWidget *parent) :
    QDialog(parent),_glWidget(glWidget),
    ui(new Ui::CCameraAdjustmentDlg), _raw_img(NULL), _processed_img(NULL), _defisheye_img(NULL),
    _diff_img(NULL), _edges_img(NULL), _scale_factor(1.0), _auto_snap(false), is_device(false), _device_busy(false),
    _buffer(NULL)
{

    ui->setupUi(this);

    ///////////////////////////////////////////////////////
     FT_STATUS ftStatus = connectToModule();
    if (ftStatus != FT_OK)
    {
      disconnectFromModule();
      QMessageBox::information(this, tr("Information"), err_DeviceConnection);
    }
    else
    {
        int size = getSnapshotBufferSize() + 16;
        _buffer = (u8*)calloc(size, 1);

        is_device = true;
        initCameraRegs();
    }

    int reg_count = 256;
    for(int i = 0; i < reg_count; i++)
          ch_reg[i] = false;


    _current_img = _raw_img;

    _image_label = new QLabel;
    _image_label->setBackgroundRole(QPalette::Base);
    _image_label->setSizePolicy(QSizePolicy::Ignored, QSizePolicy::Ignored);
    _image_label->setScaledContents(true);

    ui->scrollArea->setBackgroundRole(QPalette::Dark);

    ui->scrollArea->setWidget(_image_label);
    ui->scrollArea->setWidgetResizable(false);

    //////////// lasers section ////////////////////
    ui->btnLeftLaser->setCheckable(true);
    connect(ui->btnLeftLaser, SIGNAL(clicked()), this, SLOT(leftLaserToggled()) );
    ui->btnRightLaser->setCheckable(true);
    connect(ui->btnRightLaser, SIGNAL(clicked()), this, SLOT(rightLaserToggled()) );
    ////////////////////////////////////////////////


    //////////// led section ////////////////////
    /*

    ui->ledBrightSlid1->setValue(CGlobalSettings::_left_led_bright);
    ui->lblLeft->setText(QString::number(CGlobalSettings::_left_led_bright));
    ui->ledBrightSlid2->setValue(CGlobalSettings::_right_led_bright);
    ui->lblRight->setText(QString::number(CGlobalSettings::_right_led_bright));

    connect(ui->ledBrightSlid1,   SIGNAL(valueChanged(int)), this, SLOT(led1_bright_changed(int )));
    connect(ui->ledBrightSlid2,   SIGNAL(valueChanged(int)), this, SLOT(led2_bright_changed(int )));

     ui->btnLED1->setCheckable(true);
    connect(ui->btnLED1, SIGNAL(clicked()), this, SLOT(LED1Toggled()) );

    ui->btnLED2->setCheckable(true);
    connect(ui->btnLED2, SIGNAL(clicked()), this, SLOT(LED2Toggled()));
    */
    ////////////////////////////////////////////////


    ////////////// exposure /////////////////////////
    connect(ui->slidExposure, SIGNAL(valueChanged(int)), this, SLOT(exposure_changed(int)));
    ui->slidExposure->setValue(CCCDCamera::_exposure);
    ui->lblExposure->setText(QString::number(CCCDCamera::_exposure));
    if(is_device)
               writeCameraReg(CCCDCamera::regExposure, CCCDCamera::_exposure);

    ////////////// colors /////////////////////////
    /// red
    connect(ui->slidRed, SIGNAL(valueChanged(int)), this, SLOT(redgain_changed(int)));
    ui->slidRed->setValue(CCCDCamera::_red_gain);
    ui->lblRed->setText(QString::number(CCCDCamera::_red_gain));
    if(is_device)
               writeCameraReg(CCCDCamera::regRed, CCCDCamera::_red_gain);
    /// green
    connect(ui->slidGreen, SIGNAL(valueChanged(int)), this, SLOT(gregain_changed(int)));
    ui->slidGreen->setValue(CCCDCamera::_gre_gain);
    ui->lblGreen->setText(QString::number(CCCDCamera::_gre_gain));
    if(is_device)
    {
               writeCameraReg(CCCDCamera::regGreen1, CCCDCamera::_gre_gain);
               writeCameraReg(CCCDCamera::regGreen2, CCCDCamera::_gre_gain);
    }
    /// blue
    connect(ui->slidBlue, SIGNAL(valueChanged(int)), this, SLOT(blugain_changed(int)));
    ui->slidBlue->setValue(CCCDCamera::_blu_gain);
    ui->lblBlue->setText(QString::number(CCCDCamera::_blu_gain));
    if(is_device)
               writeCameraReg(CCCDCamera::regBlue, CCCDCamera::_blu_gain);



    /*connect(ui->slidExposureTex, SIGNAL(valueChanged(int)), this, SLOT(exposure_tex_changed(int)));
    ui->slidExposureTex->setValue(CCCDCamera::_exposure_tex);
    ui->lblExposureTex->setText(QString::number(CCCDCamera::_exposure_tex));


    ui->rdExp->setChecked(true);

    connect(ui->rdExp, SIGNAL(clicked()),this, SLOT(exposure_changed()));
    */

    //connect(ui->rdExpTex, SIGNAL(clicked()),this,SLOT(exposure_tex_changed()));


    ////////////////////////////////////////////////

    connect(ui->devReadBtn,  SIGNAL(clicked()), this, SLOT(dev_reg_read()));
    connect(ui->devWriteBtn, SIGNAL(clicked()), this, SLOT(dev_reg_write()));

    ui->devRegTbl->setColumnCount(2);
    ui->devRegTbl->setRowCount(256);
    for(int i = 0; i < 256; i++)
        ui->devRegTbl->setRowHeight(i,14);
    ui->devRegTbl->setColumnWidth(0, 50);
    ui->devRegTbl->setColumnWidth(1, 190);
    QStringList tblHeader;
    tblHeader << "Reg #" << "Value";
    ui->devRegTbl->setHorizontalHeaderLabels(tblHeader);
    ui->devRegTbl->verticalHeader()->setVisible(false);
    ui->devRegTbl->setItem(CCCDCamera::regExposure, 0, new QTableWidgetItem(QString::number(CCCDCamera::regExposure)));
    ui->devRegTbl->setItem(CCCDCamera::regExposure, 1, new QTableWidgetItem(QString::number(CCCDCamera::_exposure)));

    ui->devRegTbl->setItem(CCCDCamera::regRed, 0, new QTableWidgetItem(QString::number(CCCDCamera::regRed)));
    ui->devRegTbl->setItem(CCCDCamera::regRed, 1, new QTableWidgetItem(QString::number(CCCDCamera::_red_gain)));

    ui->devRegTbl->setItem(CCCDCamera::regGreen1, 0, new QTableWidgetItem(QString::number(CCCDCamera::regGreen1)));
    ui->devRegTbl->setItem(CCCDCamera::regGreen1, 1, new QTableWidgetItem(QString::number(CCCDCamera::_gre_gain)));

    ui->devRegTbl->setItem(CCCDCamera::regGreen2, 0, new QTableWidgetItem(QString::number(CCCDCamera::regGreen2)));
    ui->devRegTbl->setItem(CCCDCamera::regGreen2, 1, new QTableWidgetItem(QString::number(CCCDCamera::_gre_gain)));

    ui->devRegTbl->setItem(CCCDCamera::regBlue, 0, new QTableWidgetItem(QString::number(CCCDCamera::regBlue)));
    ui->devRegTbl->setItem(CCCDCamera::regBlue, 1, new QTableWidgetItem(QString::number(CCCDCamera::_blu_gain)));

    connect(ui->devRegTbl, SIGNAL(itemChanged(QTableWidgetItem *)), this, SLOT(on_regvalue_changed(QTableWidgetItem *)));
    ////////////////////////////////////////////////


    ////////////// edge detection /////////////////////////
    ui->edgThSlid->setValue(CGlobalSettings::_lcthresh);
    ui->lblEThres->setText(QString::number(CGlobalSettings::_lcthresh));
    connect(ui->edgThSlid,   SIGNAL(valueChanged(int)), this, SLOT(edge_thresh_changed(int )));
    //////////////////////////////////////////////
    ui->gussSigmSlid->setValue(CGlobalSettings::_gauss_sigma * 100 / CGlobalSettings::_gauss_sigma_max );
    ui->lblGaussSigm->setText(QString::number(CGlobalSettings::_gauss_sigma));
    connect(ui->gussSigmSlid,   SIGNAL(valueChanged(int)), this, SLOT(gauss_sigma_changed(int )));

    //float CGlobalSettings::_gauss_sigma_max = 5.0;
    //float CGlobalSettings::_gauss_sigma = 0.0;


   // ui->laserBrightSlid1->setValue(0);
   // ui->laserBrightSlid2->setValue(50);

   // ui->focusEdt->setText(QString::number(CCCDCamera::_focal_legth));

    connect(ui->SnapBtn, SIGNAL(clicked()), this, SLOT(mode_selector()));

    connect(ui->ZoomInBtn,   SIGNAL(clicked()), this, SLOT(zoom_in()));
    connect(ui->ZoomOutBtn,  SIGNAL(clicked()), this, SLOT(zoom_out()));
    connect(ui->ZoomNormBtn, SIGNAL(clicked()), this, SLOT(zoom_norm()));


    ui->imModeCombo->setCurrentIndex(1);

    connect(ui->imModeCombo, SIGNAL(currentIndexChanged(int)), this, SLOT(current_image_changed(int)));

    //connect(ui->tstMotorBtn, SIGNAL(clicked()), this, SLOT(test_motor()));
    //connect(ui->tstSnapBtn,  SIGNAL(clicked()), this, SLOT(test_snapshot()));


    connect(ui->AutoSnapChk, SIGNAL(clicked(bool)), this, SLOT(set_auto_snap(bool)));

    connect(ui->SaveBtn,  SIGNAL(clicked()), this, SLOT(save_image()));


   // connect(ui->laserBrightSlid1, SIGNAL(valueChanged(int)), this, SLOT(laser1_bright_changed(int )));
  //  connect(ui->laserBrightSlid2, SIGNAL(valueChanged(int)), this, SLOT(laser2_bright_changed(int )));

   // connect(ui->focusEdt,  SIGNAL(textEdited(QString)), this, SLOT(on_focus_edt(QString)));

    //connect(ui->motorTstBtn, SIGNAL(clicked()), this, SLOT(smooth_motor_rotation()));

    /// connect(ui->singlFrameBtn, SIGNAL(clicked()), this, SLOT(test_process_series()));
    ///
    connect(ui->btnOK, SIGNAL(clicked()), this, SLOT(close()));


    ui->btnRotCenter->setCheckable(true);
    connect(ui->btnRotCenter, SIGNAL(clicked()), this, SLOT(set_rotation_center()));
    /// connect(ui->scrollArea, SIGNAL(clicked()), this, SLOT(image_label_clicked()));
    /// main window mouse press event!
    /// convert QPoint mappedPos = myLabel->mapFromParent(myWindow, mouseEvent->pos());
    /// http://stackoverflow.com/questions/8355195/set-a-cross-in-qimage-at-the-position-where-mousepressed
    /// http://qt-project.org/forums/viewthread/5749

}

void CCameraAdjustmentDlg::image_label_clicked()
{
    QMessageBox::information(this, tr("Information"), tr("image_label_clicked()"));
}

void draw_vertical_line(QImage * img, int xpos)
{
    QRgb gpix = qRgb(0, 255, 0);
    int ys = img->height();
    for(int y = 0;  y < ys ; y++)
        img->setPixel(xpos, y, gpix);
}

void CCameraAdjustmentDlg::mousePressEvent(QMouseEvent *event)
{
    if((event->buttons() & Qt::LeftButton)  &&
       (ui->imModeCombo->currentIndex() == cmbPROCESSED) &&
        ui->btnRotCenter->isChecked() &&
         _processed_img != NULL   )
    {
        QPoint mPos = _image_label->mapFromParent( event->pos());

        QRgb rpix = qRgb(255, 0, 0);
        float mx = (mPos.x()-12)/_scale_factor;
        float my = (mPos.y()-12)/_scale_factor;

        float midx = _current_img->width()/2;
        CGlobalSettings::_delta_x_proc = CGlobalSettings::_delta_x_proc + (midx - mx);
        compute_processed_image();

        draw_vertical_line(_current_img, _current_img->width()/2 );
        _image_label->setPixmap(QPixmap::fromImage(*_current_img));
        _image_label->resize(_scale_factor * _image_label->pixmap()->size());
    }
    //
    // _mouseLastPos = event->pos();
    // updateGL();
}

void CCameraAdjustmentDlg::set_rotation_center()
{
    if( ui->imModeCombo->currentIndex() != cmbPROCESSED)
    {
        ui->btnRotCenter->setChecked(false);
        return;
    }

    if(_current_img == NULL) return;

    if(ui->btnRotCenter->isChecked())
        draw_vertical_line(_current_img, _current_img->width()/2 );
    else
        compute_processed_image();

    _image_label->setPixmap(QPixmap::fromImage(*_current_img));
    _image_label->resize(_scale_factor * _image_label->pixmap()->size());
}

void CCameraAdjustmentDlg::exposure_changed()
{
    if(is_device)
               writeCameraReg(CCCDCamera::regExposure, CCCDCamera::_exposure);
    ui->devRegTbl->setRowHeight(CCCDCamera::regExposure,14);
    ui->devRegTbl->setItem(CCCDCamera::regExposure, 0, new QTableWidgetItem(QString::number(CCCDCamera::regExposure)));
    ui->devRegTbl->setItem(CCCDCamera::regExposure, 1, new QTableWidgetItem(QString::number(CCCDCamera::_exposure)));
}

/*void CCameraAdjustmentDlg::exposure_tex_changed()
{
    if(is_device)
               writeCameraReg(CCCDCamera::regExposure, CCCDCamera::_exposure_tex);
    ui->devRegTbl->setRowHeight(CCCDCamera::regExposure,14);
    ui->devRegTbl->setItem(CCCDCamera::regExposure, 0, new QTableWidgetItem(QString::number(CCCDCamera::regExposure)));
    ui->devRegTbl->setItem(CCCDCamera::regExposure, 1, new QTableWidgetItem(QString::number(CCCDCamera::_exposure_tex)));
}*/

void CCameraAdjustmentDlg::exposure_changed(int val)
{
    CCCDCamera::_exposure=val;
    ui->lblExposure->setText(QString::number(CCCDCamera::_exposure));
    /// ui->rdExp->setChecked(true);
    exposure_changed();
}

void CCameraAdjustmentDlg::redgain_changed(int val)
{
    CCCDCamera::_red_gain=val;
    ui->lblRed->setText(QString::number(CCCDCamera::_red_gain));
    if(is_device)
               writeCameraReg(CCCDCamera::regRed, CCCDCamera::_red_gain);

    ui->devRegTbl->setRowHeight(CCCDCamera::regRed,14);
    ui->devRegTbl->setItem(CCCDCamera::regRed, 0, new QTableWidgetItem(QString::number(CCCDCamera::regRed)));
    ui->devRegTbl->setItem(CCCDCamera::regRed, 1, new QTableWidgetItem(QString::number(CCCDCamera::_red_gain)));
}

void CCameraAdjustmentDlg::gregain_changed(int val)
{
    CCCDCamera::_gre_gain=val;
    ui->lblGreen->setText(QString::number(CCCDCamera::_gre_gain));
    if(is_device)
    {
        writeCameraReg(CCCDCamera::regGreen1, CCCDCamera::_gre_gain);
        writeCameraReg(CCCDCamera::regGreen2, CCCDCamera::_gre_gain);
    }

    ui->devRegTbl->setRowHeight(CCCDCamera::regGreen1,14);
    ui->devRegTbl->setRowHeight(CCCDCamera::regGreen2,14);

    ui->devRegTbl->setItem(CCCDCamera::regGreen1, 0, new QTableWidgetItem(QString::number(CCCDCamera::regGreen1)));
    ui->devRegTbl->setItem(CCCDCamera::regGreen1, 1, new QTableWidgetItem(QString::number(CCCDCamera::_gre_gain)));

    ui->devRegTbl->setItem(CCCDCamera::regGreen2, 0, new QTableWidgetItem(QString::number(CCCDCamera::regGreen2)));
    ui->devRegTbl->setItem(CCCDCamera::regGreen2, 1, new QTableWidgetItem(QString::number(CCCDCamera::_gre_gain)));


}

void CCameraAdjustmentDlg::blugain_changed(int val)
{
    CCCDCamera::_blu_gain=val;
    ui->lblBlue->setText(QString::number(CCCDCamera::_blu_gain));
    if(is_device)
               writeCameraReg(CCCDCamera::regBlue, CCCDCamera::_blu_gain);

    ui->devRegTbl->setRowHeight(CCCDCamera::regBlue,14);
    ui->devRegTbl->setItem(CCCDCamera::regBlue, 0, new QTableWidgetItem(QString::number(CCCDCamera::regBlue)));
    ui->devRegTbl->setItem(CCCDCamera::regBlue, 1, new QTableWidgetItem(QString::number(CCCDCamera::_blu_gain)));
}





/*void CCameraAdjustmentDlg::exposure_tex_changed(int val)
{
    CCCDCamera::_exposure_tex=val;
    ui->lblExposureTex->setText(QString::number(CCCDCamera::_exposure_tex));
    ui->rdExpTex->setChecked(true);

    exposure_tex_changed();
}*/

void CCameraAdjustmentDlg::on_focus_edt(QString str)
{
    bool ok = false;
    int  cval = str.toFloat(&ok);
    if(ok)
    {
        CCCDCamera::_focal_legth = cval;
        //compute_defisheye_image();
    }
}

/*
void CCameraAdjustmentDlg::led1_on()
{
    if(!is_device) return;
    pwmSetBrightness(PWM_LED1_BRIGHTNESS,  CGlobalSettings::_left_led_bright);
}

void CCameraAdjustmentDlg::led1_off()
{
    if(!is_device) return;
    pwmSetBrightness(PWM_LED1_BRIGHTNESS,  0);
}

void CCameraAdjustmentDlg::led2_on()
{
    if(!is_device) return;
    pwmSetBrightness(PWM_LED2_BRIGHTNESS,  CGlobalSettings::_right_led_bright);
}

void CCameraAdjustmentDlg::led2_off()
{
    if(!is_device) return;
    pwmSetBrightness(PWM_LED2_BRIGHTNESS,  0);
}

void CCameraAdjustmentDlg::led1_bright_changed(int v)
{
    CGlobalSettings::_left_led_bright = v;
    ui->lblLeft->setText(QString::number(v));
    if(ui->btnLED1->isChecked()) led1_on();


    QPoint p = mapFromGlobal(QCursor::pos());
    ui->lblExposure->setText(QString::number(p.x()));
}

void CCameraAdjustmentDlg::led2_bright_changed(int v)
{

   // if(!is_device) return;
   // pwmSetBrightness(PWM_LED2_BRIGHTNESS, v);
    CGlobalSettings::_right_led_bright = v;
    ui->lblRight->setText(QString::number(v));
    if(ui->btnLED2->isChecked()) led2_on();

}

void CCameraAdjustmentDlg::LED1Toggled()
{
    if(ui->btnLED1->isChecked())
    {
        _led1_timer.start(1000, this);
        _led1_timer_cnt = 10;
        ui->btnLED1->setText(QString::number(_led1_timer_cnt));
        led1_on();
    }
    else
    {
        _led1_timer.stop();
        ui->btnLED1->setText("Turn ON");
        led1_off();
    }
}

void CCameraAdjustmentDlg::LED2Toggled()
{
    if(ui->btnLED2->isChecked())
    {
        _led2_timer.start(1000, this);
        _led2_timer_cnt = 10;
        ui->btnLED2->setText(QString::number(_led2_timer_cnt));
        led2_on();
    }
    else
    {
        _led2_timer.stop();
        ui->btnLED2->setText("Turn ON");
        led2_off();
    }
}
*/

void CCameraAdjustmentDlg::laser1_bright_changed(int v)
{
    if(!is_device) return;
    pwmSetBrightness(PWM_LASER1_BRIGHTNESS, v);
}

void CCameraAdjustmentDlg::laser2_bright_changed(int v)
{
    if(!is_device) return;
    pwmSetBrightness(PWM_LASER2_BRIGHTNESS, v);
}

void CCameraAdjustmentDlg::leftLaserToggled()
{
    if(ui->btnLeftLaser->isChecked())
        laser1_bright_changed(50);
    else
        laser1_bright_changed(0);
}

void CCameraAdjustmentDlg::rightLaserToggled()
{
    if(ui->btnRightLaser->isChecked())
        laser2_bright_changed(50);
    else
        laser2_bright_changed(0);
}

void CCameraAdjustmentDlg::on_regvalue_changed(QTableWidgetItem * itm)
{
    int raw= itm->row();
    if(raw >= 0 && raw <256 )
        ch_reg[raw] = true;
}

void CCameraAdjustmentDlg::dev_reg_read()
{
  if(!is_device) return;
    int reg_count = 256;
    for(int i = 0; i < reg_count; i++)
    {
        ui->devRegTbl->setRowHeight(i,14);
        ui->devRegTbl->setItem(i, 0, new QTableWidgetItem(QString::number(i)));

        int value = readCameraReg(i);
        ui->devRegTbl->setItem(i, 1, new QTableWidgetItem(QString::number(value)));
        ch_reg[i] = false;

    }

}

void CCameraAdjustmentDlg::dev_reg_write()
{
    if(!is_device) return;

    int reg_count = 256;
    int reg_changed = 0;
    for(int i = 0; i < reg_count; i++)
    {
        if(ch_reg[i])
        {
            QTableWidgetItem * itm = ui->devRegTbl->item(i, 1);
            QString str = itm->text();
            int value = str.toInt();
            writeCameraReg(i, value);
            ch_reg[i] = false;
            reg_changed++;
        }
    }

    QString str = QString::number(reg_changed);
    QString msg = "Number of registers changed is " + str;
    QMessageBox::information(this, tr("Information"), msg);


}

void CCameraAdjustmentDlg::timerEvent(QTimerEvent *event)
{
    if (event->timerId() == _snap_timer.timerId() && _auto_snap &&
            (ui->imModeCombo->currentIndex() == cmbRAW ||
             ui->imModeCombo->currentIndex() == cmbPROCESSED ))
    {
       _snap_timer.stop();
       // load_raw_img();
       mode_selector();

      /// std::cout << "auto snap"<< std::endl;
    }

    /*
    if(event->timerId() == _led1_timer.timerId() && _led1_timer_cnt >0)
    {
        _led1_timer_cnt --;
        ui->btnLED1->setText(QString::number(_led1_timer_cnt));

    }
    if(event->timerId() == _led1_timer.timerId() && _led1_timer_cnt <= 0)
    {
        ui->btnLED1->setChecked(false);
        LED1Toggled();
    }

    if(event->timerId() == _led2_timer.timerId() && _led2_timer_cnt >0)
    {
        _led2_timer_cnt --;
        ui->btnLED2->setText(QString::number(_led2_timer_cnt));

    }
    if(event->timerId() == _led2_timer.timerId() && _led2_timer_cnt <= 0)
    {
        ui->btnLED2->setChecked(false);
        LED2Toggled();
    }
    */
}

void CCameraAdjustmentDlg::set_auto_snap(bool v)
{
    _auto_snap = v;
   if(_auto_snap )
       _snap_timer.start(800, this);
    else
       _snap_timer.stop();
}

CCameraAdjustmentDlg::~CCameraAdjustmentDlg()
{
    delete ui;
    delete _image_label;

    if(_buffer != NULL) free(_buffer);


    //led1_off();
    //led2_off();
    clean_images();
    disconnectFromModule();
}

void CCameraAdjustmentDlg::clean_images()
{
    if(_raw_img)
    {
        delete _raw_img;
        _raw_img = NULL;
    }

    if(_processed_img)
    {
        delete _processed_img;
        _processed_img = NULL;
    }

    if(_defisheye_img)
    {
        delete _defisheye_img;
        _defisheye_img = NULL;
    }

    if(_diff_img)
    {
        delete _diff_img;
        _diff_img = NULL;
    }

    if(_edges_img)
    {
        delete _edges_img;
        _edges_img = NULL;
    }
}


void CCameraAdjustmentDlg::load_raw_img()
{

    int scroll_pos_x = ui->scrollArea->horizontalScrollBar()->value();
    int scroll_pos_y = ui->scrollArea->verticalScrollBar()  ->value();
    ui->SnapBtn->setEnabled(false);
    ui->SnapBtn->blockSignals(true);

    clean_images();
    if(!is_device)
        _raw_img = load_raw_img_from_hdisk();
    else
        _raw_img = load_raw_img_from_device();
    _current_img = _raw_img;

    ui->imModeCombo->setCurrentIndex(cmbRAW);
    scale_image();

    ui->SnapBtn->setEnabled(true);
    ui->SnapBtn->blockSignals(false);
    ui->scrollArea->horizontalScrollBar()->setValue(scroll_pos_x);
    ui->scrollArea->verticalScrollBar()  ->setValue(scroll_pos_y);

    if(_auto_snap)
        _snap_timer.start(500, this);

}


void CCameraAdjustmentDlg::mode_selector()
{

    clean_images();

    switch(ui->imModeCombo->currentIndex())
    {
        case cmdEDGES:
            compute_edges_image();
        break;

        case cmbPROCESSED:
            compute_processed_image();
        break;

        case cmdDEFISHEYE:
            compute_defisheye_image();
        break;

        case cmbRAW:
            load_raw_img();
        break;
    }

    if(_current_img != NULL)
    {
        _image_label->setPixmap(QPixmap::fromImage(*_current_img));
        _image_label->resize(_scale_factor * _image_label->pixmap()->size());
    }

}

QImage * CCameraAdjustmentDlg::load_raw_img_from_device()
{
    if(!is_device) return NULL;

    // get camera image
    doSnapshot(_buffer);

    // form resulting image
    int width  = getSnapshotWidth();
    int height = getSnapshotHeight();

    QImage * img = new QImage(height, width,  QImage::Format_RGB32);
    for (int y = 0; y < height; y += 2)
    {
      for (int x = 0; x < width; x += 2)
      {
        u16 g1 = getSnapshotPixelValue(_buffer, x,     y,     width);
        u16 r  = getSnapshotPixelValue(_buffer, x + 1, y,     width);
        u16 b  = getSnapshotPixelValue(_buffer, x,     y + 1, width);
        u16 g2 = getSnapshotPixelValue(_buffer, x + 1, y + 1, width);

        u16 g18 = g1 >> 4;
        u16 r8  = r  >> 4;
        u16 b8  = b  >> 4;
        u16 g28 = g2 >> 4;

        QRgb p00 = qRgb( 0, g18,  0);
        QRgb p10 = qRgb(r8,   0,  0);
        QRgb p01 = qRgb( 0,   0, b8);
        QRgb p11 = qRgb( 0, g28,  0);

        // image mirroring / change y and y + 1? due to green?
        img->setPixel(y + 1, width - x -1,        p00);
        img->setPixel(y + 1,  width - ( x + 1) - 1,    p10);
        img->setPixel(    y, width - x -1,      p01);
        img->setPixel(   y, width - (x + 1) - 1,  p11);

      }
    }

    return img;
}

QImage * CCameraAdjustmentDlg::load_raw_img_from_hdisk()
{
    return new QImage("1.png");
    /*QString filname("./test.png");
    QImage * img = new QImage(filname);

    QSize sz = img->size();
    int xs = sz.width();
    int ys = sz.height();

    QImage * res_img = new QImage(2*sz, img->format());
    for(int y = 0; y < ys; y++)
    {
        for(int x =0; x < xs; x++ )
        {
            QRgb value = img->pixel(x,y);

            QRgb p00 = qRgb(0, qGreen(value), 0);
            QRgb p10 = qRgb(qRed(value), 0, 0);
            QRgb p01 = qRgb(0, 0, qBlue(value));

            res_img->setPixel(2*x,     2*y,   p00);
            res_img->setPixel(2*x + 1, 2*y,   p10);
            res_img->setPixel(2*x,     2*y + 1, p01);
            res_img->setPixel(2*x + 1, 2*y + 1,   p00);
        }
    }
    delete img;
    return res_img;*/
}

void CCameraAdjustmentDlg::adjust_scroll_bar(QScrollBar *scrollBar, float factor)
{
    scrollBar->setValue(int(factor * scrollBar->value()
                            + ((factor - 1) * scrollBar->pageStep()/2)));
}

void CCameraAdjustmentDlg::scale_image()
{
    if(_current_img == NULL) return;

    _image_label->setPixmap(QPixmap::fromImage(*_current_img));
    _image_label->resize(_scale_factor * _image_label->pixmap()->size());
    adjust_scroll_bar(ui->scrollArea->horizontalScrollBar(), _scale_factor);
    adjust_scroll_bar(ui->scrollArea->verticalScrollBar(),   _scale_factor);

    update();
}

void CCameraAdjustmentDlg::zoom_in()
{
    if(_scale_factor > 2.0) return;
    _scale_factor += .25;
     scale_image();
}

void CCameraAdjustmentDlg::zoom_out()
{
    if(_scale_factor < 0.5) return;
    _scale_factor -= .25;
     scale_image();
}

void CCameraAdjustmentDlg::zoom_norm()
{
    _scale_factor = 1.0;
     scale_image();
}

void CCameraAdjustmentDlg::current_image_changed(int idx)
{

    ui->btnRotCenter->setChecked(false);

    switch(idx)
    {
        case cmdEDGES:
            ui->AutoSnapChk->setEnabled(false);
            compute_edges_image();
        break;

        case cmbPROCESSED:
            ui->AutoSnapChk->setEnabled(true);
            compute_processed_image();
        break;

        case cmdDEFISHEYE:
            ui->AutoSnapChk->setEnabled(false);
            compute_defisheye_image();

        break;

        case cmbRAW:
        delaulft:
            //if(_raw_img == NULL)
             //    load_raw_img();
             ui->AutoSnapChk->setEnabled(true);
            _current_img = _raw_img;
        break;
    }

    if(_current_img != NULL)
    {
        _image_label->setPixmap(QPixmap::fromImage(*_current_img));
        _image_label->resize(_scale_factor * _image_label->pixmap()->size());
    }
    else
        _image_label->clear();

    // scale_image();
}

void CCameraAdjustmentDlg::compute_processed_image()
{
    if(_processed_img != NULL && _current_img != _processed_img)
    {
        _current_img = _processed_img;
        return;
    }

    if(_raw_img == NULL)
    {
        //ui->imModeCombo->setCurrentIndex(cmbRAW);
        //QMessageBox::information(this, tr("Information"), err_loadRawImage);
        //return;
        load_raw_img();
    }

    if(_processed_img != NULL)
        delete _processed_img;

    _processed_img = CCCDCamera::processed_image(_raw_img);
    _current_img = _processed_img;

    ui->imModeCombo->setCurrentIndex(cmbPROCESSED);
//    _scale_factor = 1.0;

    if(_auto_snap)
        _snap_timer.start(500, this);
}

void CCameraAdjustmentDlg::compute_defisheye_image()
{
    /* if(_defisheye_img != NULL )
    {
        delete _defisheye_img ;
        _defisheye_img = NULL;
    } */

    if(_defisheye_img != NULL && _current_img != _defisheye_img)
    {
        _current_img = _defisheye_img;
        return;
    }

    if(_processed_img == NULL)
    {
       /* ui->imModeCombo->setCurrentIndex(cmbRAW);
        QMessageBox::information(this, tr("Information"), err_computeProcImage);
        return;
       */
        compute_processed_image();
    }

    if(_defisheye_img != NULL) delete _defisheye_img ;

    _defisheye_img = CCCDCamera::defisheye_image(_processed_img);
    _current_img = _defisheye_img;

    ui->imModeCombo->setCurrentIndex(cmdDEFISHEYE);
 //   _scale_factor = 1.0;
}


void CCameraAdjustmentDlg::compute_edges_image()
{

    // original compute edge
    if(_diff_img == NULL) compute_diff_img();

    if(_edges_img != NULL )
    {
        delete _edges_img ;
        _edges_img = NULL;
    }

    _edges_img = process_single_diff_image();
    _current_img = _edges_img;

    ui->imModeCombo->setCurrentIndex(cmdEDGES);
    _image_label->setPixmap(QPixmap::fromImage(*_current_img));
    _image_label->resize(_scale_factor * _image_label->pixmap()->size());

/*

    QImage * proc_img_lasr = new QImage("./d2.png");
    QImage * proc_img_bckg = new QImage("./d1.png");

    CCCDCamera::difference_image(proc_img_lasr, proc_img_bckg);

    QImage * edge_img = proc_img_lasr;
    std::vector<CIPixel> pix = get_edge_points_lines_hills_1D(edge_img);

    std::vector<CIPixel>::iterator it = pix.begin();
    for( ; it != pix.end(); it++)
    {
        QRgb p = qRgb(0, 255, 0);
        edge_img->setPixel(it->_u, it->_v, p);
    }

    _current_img = edge_img;

    ui->imModeCombo->setCurrentIndex(cmdEDGES);
    _image_label->setPixmap(QPixmap::fromImage(*_current_img));
    _image_label->resize(_scale_factor * _image_label->pixmap()->size());
*/
}


void CCameraAdjustmentDlg::compute_diff_img()
{
    if(_diff_img != NULL) delete _diff_img;

    if(!is_device)
        _diff_img = new QImage("./diff.png");
    else
    {
        // turn off everething
        /// led1_off();
        /// led2_off();
        pwmSetBrightness(PWM_LASER1_BRIGHTNESS,  0);
        pwmSetBrightness(PWM_LASER2_BRIGHTNESS,  0);
        QImage * raw_img_bckg = load_raw_img_from_device();
        QImage * proc_img_bckg = CCCDCamera::processed_image(raw_img_bckg);

        pwmSetPrescaler(10);
        pwmSetBrightness(PWM_LASER1_BRIGHTNESS,  100);
        QImage * raw_img_lasr = load_raw_img_from_device();
        pwmSetBrightness(PWM_LASER1_BRIGHTNESS,  0);
        QImage * proc_img_lasr =  CCCDCamera::processed_image(raw_img_lasr);

        CCCDCamera::difference_image(proc_img_lasr, proc_img_bckg);

        QImage * defish_diff = CCCDCamera::defisheye_image(proc_img_lasr);

        delete raw_img_bckg;
        delete proc_img_bckg;
        delete raw_img_lasr;
        delete proc_img_lasr;

        _diff_img = defish_diff;

    }



}

void CCameraAdjustmentDlg::test_motor()
{
  if(!is_device) return;

  pwmSetPrescaler(10);
  pwmSetBrightness(PWM_LED1_BRIGHTNESS, 20);
  pwmSetBrightness(PWM_LED2_BRIGHTNESS, 5);
  pwmSetBrightness(PWM_LASER1_BRIGHTNESS, 0);
  pwmSetBrightness(PWM_LASER2_BRIGHTNESS, 50);


  stepMotorTest();

  QMessageBox::information(this, tr("Information"), tr("success"));

}

void CCameraAdjustmentDlg::test_snapshot()
{
    //ui->tstSnapBtn->setEnabled(false);

    std::cout<< "test" << std::endl;
    return;

    /* int i;
    u16 data;
    int result;
    FT_STATUS ftStatus;

    ftStatus = connectToModule();
    if (ftStatus != FT_OK)
    {
      disconnectFromModule();
      QMessageBox::information(this, tr("Information"), tr("failure"));
      return;
    }

    pwmSetPrescaler(10);
    pwmSetBrightness(PWM_LED1_BRIGHTNESS, 20);
    pwmSetBrightness(PWM_LED2_BRIGHTNESS, 5);
    pwmSetBrightness(PWM_LASER1_BRIGHTNESS, 0);
    pwmSetBrightness(PWM_LASER2_BRIGHTNESS, 50);

    initCameraRegs();
    testSnapshot();

    disconnectFromModule();

    QMessageBox::information(this, tr("Information"), tr("success"));*/
}

void CCameraAdjustmentDlg::save_image()
{
    if(_current_img == NULL) return;

    QString fileName = QFileDialog::getSaveFileName(this,
                tr("Save image ..."), "", "Image PNG (*.png)");

    if (fileName.isEmpty()) return;

    if(!_current_img->save(fileName))
        QMessageBox::information(this, tr("Warning"), err_cannotSaveFile );

    //generate some image
    /* unsigned width  = _current_img->width();
    unsigned height = _current_img->height();

    std::vector<unsigned char> saving_image;
    saving_image.resize(width * height * 4);

    for(unsigned y = 0; y < height; y++)
    {
        for(unsigned x = 0; x < width; x++)
        {
            QRgb value = _current_img->pixel(x,y);

            saving_image[4 * width * y + 4 * x + 0] = qRed(value);
            saving_image[4 * width * y + 4 * x + 1] = qGreen(value);
            saving_image[4 * width * y + 4 * x + 2] = qBlue(value);
            saving_image[4 * width * y + 4 * x + 3] = 255;
        }
    }

    unsigned error = lodepng::encode(fileName.toStdString(), saving_image, width, height);
    if(error)
        QMessageBox::information(this, tr("Warning"), err_cannotSaveFile );
    */
}


int CCameraAdjustmentDlg::ComboToChannelIdx(QString str)
{
    if(str == "1")
        return STEP_M1;
    if(str == "2")
        return STEP_M2;
    if(str == "3")
        return STEP_M3;

    return STEP_M4;
}

void mwait(int msec)
{
    // QEventLoop loop; QTimer::singleShot(msec, &loop, SLOT(quit())); loop.exec();
    QThread::msleep(msec);
}

void CCameraAdjustmentDlg::smooth_motor_rotation()
{
    if(!is_device) return;

    int dInt = 10;
    int cyclNum = 25;
    unsigned int delayMin    =    1;
    unsigned int delayTime   =    10;

    int fi_channel = STEP_M1;
    int se_channel = STEP_M2;
    int th_channel = STEP_M3;
    int fo_channel = STEP_M4;

   stepMotorSetPrescaler(10);

   for(int c = 0; c < cyclNum; c++)
   {
       for ( int i = 0; i<=200; i +=dInt )
       {
           stepMotorSetValue(fi_channel, 200-i);
           stepMotorSetValue(se_channel, 0);
           stepMotorSetValue(th_channel, i);
           stepMotorSetValue(fo_channel, 0);

         //  mwait(delayMin);
       }
       //mwait(delayTime);

       for ( int i = 0; i<=200; i=i+dInt )
       {

           stepMotorSetValue(fi_channel, 0);
           stepMotorSetValue(se_channel, i);
           stepMotorSetValue(th_channel, 200-i);
           stepMotorSetValue(fo_channel, 0);

          // mwait(delayMin);
       }
       //mwait(delayTime);

       for ( int i = 0; i<=200; i=i+dInt )
       {
           stepMotorSetValue(fi_channel, 0);
           stepMotorSetValue(se_channel, 200-i);
           stepMotorSetValue(th_channel, 0);
           stepMotorSetValue(fo_channel, i);

          // mwait(delayMin);
       }
       //mwait(delayTime);

       for ( int i = 0; i<=200; i=i+dInt )
       {
           stepMotorSetValue(fi_channel, i);
           stepMotorSetValue(se_channel, 0);
           stepMotorSetValue(th_channel, 0);
           stepMotorSetValue(fo_channel, 200-i);

          // mwait(delayMin);
       }
       // mwait(delayTime);
   }

  /*  stepMotorSetPrescaler(10);
      stepMotorSetValue(STEP_M1, 50);
      stepMotorSetValue(STEP_M2, 100);
      stepMotorSetValue(STEP_M3, 150);
      stepMotorSetValue(STEP_M4, 180);

      return;

    int fi_channel = ComboToChannelIdx(ui->cmb_fi_channel->currentText());
    int se_channel = ComboToChannelIdx(ui->cmb_se_channel->currentText());
    int th_channel = ComboToChannelIdx(ui->cmb_th_channel->currentText());
    int fo_channel = ComboToChannelIdx(ui->cmb_fo_channel->currentText());

    unsigned int delayMin    =  (ui->dMinEdt->text().toInt());
    unsigned int delayTime   =  (ui->dTimeEdt->text().toInt());
    int dInt        = ui->deltaIEdt ->text().toInt();
    int cyclNum     = ui->timRepEdt ->text().toInt();

   // std::cout << fi_channel << " " << se_channel << " ";
   // std::cout << th_channel << " " << fo_channel << std::endl;
   // std::cout << delayMin  << " " << delayTime << std::endl;
   // std::cout << dInt << " " << cyclNum << std::endl;

    if(!is_device) return;

   // QTime t1 = QTime::currentTime();
    stepMotorSetPrescaler(10);
    stepMotorSetValue(STEP_M1, ui->m1Edt->text().toInt());
    stepMotorSetValue(STEP_M2, ui->m2Edt->text().toInt());
    stepMotorSetValue(STEP_M3, ui->m3Edt->text().toInt());
    stepMotorSetValue(STEP_M4, ui->m4Edt->text().toInt());
    mwait(delayTime);*/
/*for(int c = 0; c < cyclNum; c++)
{
    for ( int i = 0; i<=200; i +=dInt )
    {
        stepMotorSetValue(fi_channel, i);
        stepMotorSetValue(se_channel, 200);
        stepMotorSetValue(th_channel, 200-i);
        stepMotorSetValue(fo_channel, 200);

        mwait(delayMin);
    }
    mwait(delayTime);

    for ( int i = 0; i<=200; i=i+dInt )
    {

        stepMotorSetValue(fi_channel, 200);
        stepMotorSetValue(se_channel, 200-i);
        stepMotorSetValue(th_channel, i);
        stepMotorSetValue(fo_channel, 200);

        mwait(delayMin);
    }
    mwait(delayTime);

    for ( int i = 0; i<=200; i=i+dInt )
    {
        stepMotorSetValue(fi_channel, 200);
        stepMotorSetValue(se_channel, i);
        stepMotorSetValue(th_channel, 200);
        stepMotorSetValue(fo_channel, 200-i);

        mwait(delayMin);
    }
    mwait(delayTime);

    for ( int i = 0; i<=200; i=i+dInt )
    {
        stepMotorSetValue(fi_channel, 200-i);
        stepMotorSetValue(se_channel, 200);
        stepMotorSetValue(th_channel, 200);
        stepMotorSetValue(fo_channel, i);

        mwait(delayMin);
    }
    mwait(delayTime);
}
*/
//QTime t2 = QTime::currentTime();

//QString msg = "Total loop time is " + QString::number(t1.msecsTo(t2)) + "msecs";

//QMessageBox::information(this, tr("Information"), msg);

//stepMotorSetValue(fi_channel, 200);
//stepMotorSetValue(se_channel, 200);
//stepMotorSetValue(th_channel, 200);
//stepMotorSetValue(fo_channel, 200);

}

QImage * CCameraAdjustmentDlg::process_single_diff_image()
{
    QImage * edge_img = new QImage(*_diff_img);

    CGlobalSettings::_lcthresh = ui->edgThSlid->value();

    std::vector<CIPixel> pix = get_edge_points_lines_hills_1D(edge_img);

    std::vector<CIPixel>::iterator it = pix.begin();
    for( ; it != pix.end(); it++)
    {
        QRgb p = qRgb(0, 255, 0);
        edge_img->setPixel(it->_u, it->_v, p);
    }
    return edge_img;
}

void CCameraAdjustmentDlg::edge_thresh_changed(int val)
{

    CGlobalSettings::_lcthresh = val;
    ui->lblEThres->setText(QString::number(CGlobalSettings::_lcthresh));

    if(ui->imModeCombo->currentIndex() == cmdEDGES)
        compute_edges_image();
}

void CCameraAdjustmentDlg::gauss_sigma_changed(int val)
{
    CGlobalSettings::_gauss_sigma = float(val) * CGlobalSettings::_gauss_sigma_max / 100.0;
    ui->lblGaussSigm->setText(QString::number(CGlobalSettings::_gauss_sigma));

    if(ui->imModeCombo->currentIndex() == cmdEDGES)
        compute_edges_image();
}

void CCameraAdjustmentDlg::test_process_series()
{

    int first_idx = 1;
    int last_idx  = 800;
    CGlobalSettings::_lcthresh = ui->edgThSlid->value();

    std::vector<C3DPoint> points;
    for(int i = first_idx; i <= last_idx; i++ )
    {
        //////// left points
        {
        QString fname = "./diff.png";

        // fname.sprintf("D:\\_Running_Projects\\_Elance_ScanRecoSoft\\TestImages\\ma\\left\\ma_%.4d.png", i);
        //fname.sprintf("D:\\_Running_Projects\\_Elance_ScanRecoSoft\\TestImages\\cylmv\\cm_%.4d.png", i);
        QImage diff_img(fname);

        std::vector<CIPixel> pix = get_edge_points_lines_hills_1D(&diff_img);
        std::cout << fname.toStdString() << std::endl;


        std::vector<C3DPoint> cpoints = image2space_conv(pix, CGlobalSettings::_d,
        CGlobalSettings::_h, CGlobalSettings::_alpha_x, CGlobalSettings::_alpha_z,
        CGlobalSettings::_delta_x, CGlobalSettings::_delta_y, CGlobalSettings::_betta,
        CGlobalSettings::_scan_step_angle, diff_img.width(), diff_img.height(), i-first_idx, false,
                                                         CGlobalSettings::_maximal_radius_pnts, NULL, false);

        points.insert(points.end(), cpoints.begin(), cpoints.end());
        }
        /* ////////// right points
        { // bug in soft or in images?
        QString fname;
        // fname.sprintf("D:\\_Running_Projects\\_Elance_ScanRecoSoft\\TestImages\\ma\\right\\ma_right_%.4d.png", i);
        fname.sprintf("D:\\_Running_Projects\\_Elance_ScanRecoSoft\\TestImages\\cylmv\\cm_right_%.4d.png", i);

        QImage diff_img(fname);

        std::vector<CIPixel> pix = get_edge_points_lines_hills_1D(&diff_img);
        std::cout << fname.toStdString() << std::endl;


        std::vector<C3DPoint> cpoints = image2space_conv(pix, CGlobalSettings::_d,
        CGlobalSettings::_h, CGlobalSettings::_alpha_x, CGlobalSettings::_alpha_z,
        CGlobalSettings::_delta_x, CGlobalSettings::_delta_y, CGlobalSettings::_betta,
        CGlobalSettings::_scan_step_angle, diff_img.width(), diff_img.height(), i-first_idx, true);

        points.insert(points.end(), cpoints.begin(), cpoints.end());
        }*/
    }

    if(points.size())
    {
        _glWidget->construct_single_scan(points);
        close();
    }
}

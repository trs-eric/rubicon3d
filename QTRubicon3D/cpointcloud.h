#ifndef CPOINTCLOUD_H
#define CPOINTCLOUD_H

#include<QString>
#include<vector>

#include "cglobject.h"
#include "graphicutils.h"
#include "ciostate.h"

class CPointCloud : public CGLObject, public CIOState
{
public:
    CPointCloud();

    void create_glList();

    bool import(QString & file);
    bool pexport(QString & file);

    int get_points_count(){return _points.size();}
    std::vector<C3DPoint> get_points_cpy(){return _points;}
    void set_points(std::vector<C3DPoint> & pnts){_points = pnts;}
    void add_points(std::vector<C3DPoint> & pnts){
        _points.insert(_points.end(), pnts.begin(), pnts.end());}

    void save(std::ofstream & ofile);
    void load(std::ifstream & ifile);

    void scale(float sx, float sy, float sz);
    void translate(float dx, float dy, float dz);
    void rotate(float rx, float ry, float rz);

protected:
    bool import_from_file_xyz(std::string fname);
    bool export_xyz_pnts(std::string fname);

    void create_glList_using_greference();
    void create_glList_ignoring_greference();

    std::vector<C3DPoint> _points;

    void rotateX(float cos_alpha, float sin_alpha, float * x, float * y, float * z);
    void rotateY(float cos_alpha, float sin_alpha, float * x, float * y, float * z);
    void rotateZ(float cos_alpha, float sin_alpha, float * x, float * y, float * z);
};

#endif // CPOINTCLOUD_H

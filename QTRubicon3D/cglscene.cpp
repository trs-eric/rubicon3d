#include "cglscene.h"
#include <QMessageBox>
#include <QFileInfo>
#include "cglsampletrivtable.h"
#include "cfreeglcamera.h"
#include "messages.h"
CGLCamera * gReferencePoint;


const char gRUBident[6] = "RUB1.3";
extern int global_compound_object_index;
CGLScene::CGLScene(QWidget * treew) : _treew(treew)
{
    _table = NULL;
    _camera = NULL;
    _currentCObject = NULL;

    _cobj_left = NULL;
    _cobj_right = NULL;

    CreateScene();
}

CGLScene::~CGLScene()
{
    DeleteScene();
}

std::vector<CGLCompoundObject*> CGLScene::get_list_of_valid_meshes()
{
    std::vector<CGLCompoundObject*>  res;
    std::vector<CGLCompoundObject *>::iterator it = _cglobjects.begin();
    for(; it != _cglobjects.end(); it++)
    {
        CTriangularMesh * vv = (*it)->get_mesh();
        if(vv != NULL)
               res.push_back(*it);
    }
    return res;

}

void CGLScene::CreateScene()
{
    DeleteScene();

    _table  = new CGLSampleTrivTable;

    _camera = new CFreeGLCamera;
    gReferencePoint = _camera;
}

void CGLScene::DeleteScene()
{
    if(_table)
    {
        delete _table;
        _table = NULL;
    }
    if(_camera)
    {
        delete _camera;
        _camera = NULL;
    }

    DeleteObjects();
}

void CGLScene::DeleteObjects()
{
    std::vector<CGLCompoundObject *>::iterator it = _cglobjects.begin();
    for(; it != _cglobjects.end(); it++)
        delete *it;
}

void CGLScene::GLRenderScene()
{
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();

    if(_camera)
        _camera->set_view();

    if(_table)
        _table->glDraw();

    std::vector<CGLCompoundObject *>::iterator it = _cglobjects.begin();
    for(; it != _cglobjects.end(); it++)
        (*it)->glDraw();

}

bool CGLScene::load_from_rub_file(QString &fname)
{

    std::ifstream ifile;
    ifile.open(fname.toUtf8().constData(), std::ios::in | std::ios::binary);
    if(!ifile.is_open()) return false;

    char fformat[6];
    ifile.read(reinterpret_cast<char *>(&fformat), sizeof(gRUBident));
    if(QString::fromUtf8(fformat,6) != QString::fromUtf8(gRUBident,6))
        return false;

    int obj_num = 0;
    ifile.read(reinterpret_cast<char *>(&obj_num), sizeof(int));

    global_compound_object_index = 0;
    DeleteObjects();
    _currentCObject = NULL;

    for(int i =0; i < obj_num; i++)
    {
        CGLCompoundObject * cobj = new CGLCompoundObject(_treew, false);
        cobj->load(ifile);
        cobj->update_treew_children();
        if(cobj->is_current())
            _currentCObject = cobj;
        _cglobjects.push_back(cobj);
    }
    ifile.close();
    return true;
}

void CGLScene::save_into_rub_file(QString &fname)
{
    std::ofstream ofile;
    ofile.open(fname.toUtf8().constData(), std::ios::out | std::ios::binary);
    if(!ofile.is_open()) return;

    ofile.write(reinterpret_cast<const char *>(&gRUBident), sizeof(gRUBident));

    int obj_num = _cglobjects.size();
    ofile.write(reinterpret_cast<const char *>(&obj_num ), sizeof(int));

    std::vector<CGLCompoundObject *>::iterator it = _cglobjects.begin();
    for(; it != _cglobjects.end(); it++)
        (*it)->save(ofile);

    ofile.close();
}


bool CGLScene::ImportScene(QString &fname)
{

    CGLCompoundObject * cobj = new CGLCompoundObject(_treew, true);
    bool result = false;
    if(fname.endsWith(".xyz", Qt::CaseInsensitive))
        result = cobj->import_raw_points(fname);
    else if (fname.endsWith(".obj", Qt::CaseInsensitive))
        result = cobj->import_mesh(fname);

    if(!result)
    {
        delete cobj;
        return false;
    }

    if(_currentCObject != NULL)
    {
        _currentCObject->select_object(false);
        _currentCObject->update_treew_children();
    }
    _currentCObject = cobj;

    _cglobjects.push_back(cobj);
    return true;
}

CGLCompoundObject * CGLScene::ConstructSingleScan(std::vector<C3DPoint> & pnts)
{
    CGLCompoundObject * cobj = new CGLCompoundObject(_treew, true);
    cobj->set_raw_points(pnts);
    if(_currentCObject != NULL)
    {
        _currentCObject->select_object(false);
        _currentCObject->update_treew_children();
    }
    _currentCObject = cobj;
    _cglobjects.push_back(cobj);
    return cobj;
}

bool CGLScene::ConstructNextScan(std::vector<C3DPoint> & pnts)
{
    if(_currentCObject == NULL || (_cglobjects.back() != _currentCObject))
        return ConstructSingleScan(pnts);
    _currentCObject->add_raw_points(pnts);
    return true;
}

void CGLScene::PrepaireNewScan()
{
    _cobj_right = NULL;
    _cobj_left  = NULL;
}
void CGLScene::ConstructNextScanRight(std::vector<C3DPoint> & pnts)
{
   if(_cobj_right == NULL )
    {
       _cobj_right = ConstructSingleScan(pnts);
       _cobj_right ->extend_title("_right");
       _cobj_right ->update_treew_children();
       return;
    }
    _cobj_right->add_raw_points(pnts);
}

void CGLScene::ConstructNextScanLeft(std::vector<C3DPoint> & pnts)
{
    if(_cobj_left == NULL  )
     {
        _cobj_left = ConstructSingleScan(pnts);
        _cobj_left->extend_title("_left");
        _cobj_left->update_treew_children();
        return;
     }
    _cobj_left->add_raw_points(pnts);
}

void CGLScene::ConstructNextTextureLeft(CRawTexture * tex)
{
    if(_cobj_left != NULL)
        _cobj_left->add_raw_texture(tex);
}

void CGLScene::ConstructNextTextureRight(CRawTexture * tex)
{
    if(_cobj_right != NULL)
        _cobj_right->add_raw_texture(tex);
}

void CGLScene::ExportScene(QString &fname)
{
    if(_currentCObject == NULL)
    {
        QMessageBox::warning(NULL, QObject::tr("Warning"), wrn_CannotExportCObject);
        return;
    }
    /// if(fname.endsWith(".xyz", Qt::CaseInsensitive))
    ///    _currentCObject->export_raw_points(fname);
    /// else
    if(fname.endsWith(".obj", Qt::CaseInsensitive))
        _currentCObject->export_mesh(fname);
    else if(fname.endsWith(".objs", Qt::CaseInsensitive))
    {

        QFileInfo info_fname(fname);
        QString working_dir = info_fname.absolutePath();
        QString base_name   = info_fname.baseName();

        std::vector<CGLCompoundObject *>::iterator it = _cglobjects.begin();
        for(; it != _cglobjects.end(); it++)
        {

            QString scan_name = (*it)->get_title();
            ///scan_name.simplified();
            scan_name.replace(" ", "_");

            QString scan_obj_name =  working_dir + "//" + base_name + "_" + scan_name + ".obj";

            ///std::cout << scan_name.toStdString() << std::endl;
            ///std::cout << scan_obj_name.toStdString() << std::endl;
            (*it)->export_mesh(scan_obj_name);
        }

    }




}


void CGLScene::change_current_cobject(CGLCompoundObject * nit)
{
    if(_currentCObject != NULL)
    {
        _currentCObject->select_object(false);
        _currentCObject->update_treew_children();
    }
    _currentCObject = nit;
    _currentCObject->select_object(true);
    _currentCObject->update_treew_children();

}

void CGLScene::update_selection_state(QTreeWidgetItem * item, int col)
{
    std::vector<CGLCompoundObject *>::iterator it = _cglobjects.begin();
    for(; it != _cglobjects.end(); it++)
    {
        if((*it)->is_update_root_selection_state(item, col) &&
               _currentCObject != (*it)  )
        {          
            change_current_cobject(*it);
        }
        else
        {
            if((*it)->is_update_child_selection_state(item, col) &&
                 _currentCObject != (*it)   )
                change_current_cobject(*it);

            (*it)->update_selection_state(item, col);
        }
    }
}

void CGLScene::on_single_click_treew_switch(QTreeWidgetItem * item, int col)
{
    if(is_romove_action(item, col))
    {
        QMessageBox::StandardButton reply =
                QMessageBox::warning(_treew, QObject::tr("Warning"),wrn_RemoveScan, QMessageBox::Yes|QMessageBox::No);
         if (reply == QMessageBox::No)
                      return;
        update_remove_scan_state(item, col);
        return;
    }

    std::vector<CGLCompoundObject *>::iterator it = _cglobjects.begin();
    for(; it != _cglobjects.end(); it++)
    {
        if((*it)->is_update_child_selection_state(item, col) &&
             _currentCObject != (*it))
            change_current_cobject(*it);
    }

    update_surface_state(item,col);
    update_scan_visability_state(item,col);

}

bool CGLScene::is_romove_action(QTreeWidgetItem * item, int col)
{
    std::vector<CGLCompoundObject *>::iterator it = _cglobjects.begin();
    for(; it != _cglobjects.end(); it++)
        if((*it)->is_remove_state(item, col))
            return true;

    return false;
}

void CGLScene::update_remove_scan_state(QTreeWidgetItem * item, int col)
{

    std::vector<CGLCompoundObject *>::iterator it = _cglobjects.begin();
    for(; it != _cglobjects.end(); it++)
    {
        if((*it)->is_remove_state(item, col))
        {
            CGLCompoundObject * dl = (*it);
            if( dl == _currentCObject)
            {
                _currentCObject->select_object(false);
                _currentCObject=NULL;
            }
            _cglobjects.erase(it);
            delete dl;

            return;
        }

    }

}
void CGLScene::update_scan_visability_state(QTreeWidgetItem * item, int col)
{
    std::vector<CGLCompoundObject *>::iterator it = _cglobjects.begin();
    for(; it != _cglobjects.end(); it++)
        (*it)->update_visability_state(item, col);
}

void CGLScene::update_surface_state(QTreeWidgetItem * item, int col)
{
    std::vector<CGLCompoundObject *>::iterator it = _cglobjects.begin();
    for(; it != _cglobjects.end(); it++)
        (*it)->update_surface_state(item, col);
}

void CGLScene::update_referece_dependant_glRepo()
{
    // changing camera position some of the scene features
    // has to be colored differently

    std::vector<CGLCompoundObject *>::iterator it = _cglobjects.begin();
    for(; it != _cglobjects.end(); it++)
        (*it)->update_refdependant_glRepo();
}

CPointCloud * CGLScene::get_raw_poins_of_current_object()
{
    if(_currentCObject == NULL)
            return NULL;
    return _currentCObject->get_raw_points();
}

CPointCloud * CGLScene::get_optim_points_of_current_object()
{
    if(_currentCObject == NULL)
            return NULL;
    return _currentCObject->get_optim_points();

}

CNormalCloud * CGLScene::get_normals_of_current_object()
{
    if(_currentCObject == NULL)
            return NULL;
    return _currentCObject->get_normals();
}

CTriangularMesh * CGLScene::get_mesh_of_current_object()
{
    if(_currentCObject == NULL)
            return NULL;
    return _currentCObject->get_mesh();
}


void CGLScene::set_raw_poins_of_current_object(CPointCloud * pcld)
{
     if(_currentCObject == NULL) return;
     _currentCObject->set_raw_points(pcld);
}


void CGLScene::set_optim_poins_of_current_object(CPointCloud * pcld)
{
     if(_currentCObject == NULL) return;
     _currentCObject->set_optim_points(pcld);
}

void CGLScene::set_normal_poins_of_current_object(CNormalCloud * ncld)
{
    if(_currentCObject == NULL) return;
    _currentCObject->set_normal_points(ncld);
}

void CGLScene::set_mesh_of_current_object(CTriangularMesh * mesh)
{
    if(_currentCObject == NULL) return;
    _currentCObject->set_mesh(mesh);
}

void CGLScene::create_new_cobject(CTriangularMesh * mesh)
{
    CGLCompoundObject * cobj = new CGLCompoundObject(_treew, false, "POISSON");

    cobj->set_mesh(mesh);
    cobj->update_treew_children();
    _cglobjects.push_back(cobj);

    std::vector<CGLCompoundObject *>::iterator it = _cglobjects.begin();
    for(  ; it != _cglobjects.end() ; it++)
               (*it)->set_view_mode(CGLCompoundObject::NONE);
    cobj->set_view_mode(CGLCompoundObject::MESH);
}

void CGLScene::chainge_view_mode_of_current_object(CGLCompoundObject::ViewingMode vm)
{
     if(_currentCObject == NULL) return;
     _currentCObject->set_view_mode(vm);
}

void CGLScene::chainge_mesh_rmode_of_current_object(CTriangularMesh::RenderingMode rm)
{
    if(_currentCObject == NULL) return;
    _currentCObject->set_mesh_render_mode(rm);
}

void CGLScene::CleanCurrentScan()
{
    if(_cobj_right)
    {
        _cobj_right->delete_raw_points();
        _cobj_right->set_view_mode(CGLCompoundObject::RAW_POINTS);
    }

    if(_cobj_left)
    {
        _cobj_left->delete_raw_points();
        _cobj_left->set_view_mode(CGLCompoundObject::RAW_POINTS);
    }

}

void CGLScene::discard_raw_points_of_current_object()
{
    discard_optim_points_of_current_object();
    if(_currentCObject == NULL) return;
    _currentCObject->delete_raw_points();
    _currentCObject->set_view_mode(CGLCompoundObject::RAW_POINTS);
}

void CGLScene::discard_optim_points_of_current_object()
{
    discard_normal_points_of_current_object();
    if(_currentCObject == NULL) return;
    _currentCObject->delete_optim_points();
    _currentCObject->set_view_mode(CGLCompoundObject::RAW_POINTS);
}

void CGLScene::discard_normal_points_of_current_object()
{
    discard_mesh_of_current_object();
    if(_currentCObject == NULL) return;
    _currentCObject->delete_normal_points();
    _currentCObject->set_view_mode(CGLCompoundObject::OPTIM_POINTS);
}

void CGLScene::discard_mesh_of_current_object()
{
    if(_currentCObject == NULL) return;
    _currentCObject->delete_mesh();
    _currentCObject->set_view_mode(CGLCompoundObject::NORMALS);
}

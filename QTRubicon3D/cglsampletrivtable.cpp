#include "cglsampletrivtable.h"
#include "GraphicUtils.h"

CGLSampleTrivTable::CGLSampleTrivTable()
{
    _color[0] =  90.0f/255.0f;
    _color[1] =  90.0f/255.0f;
    _color[2] = 140.0f/255.0f;
}

CGLSampleTrivTable::~CGLSampleTrivTable()
{
}

void CGLSampleTrivTable::create_glList()
{
    delete_glLists();

    // create new display list
    _oglid = glGenLists(1);
    glNewList(_oglid, GL_COMPILE);
     glDisable(GL_LIGHTING);
     glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
    float side = 5;
    float nmax = 5;
    for (int r = side, n = nmax; n > 0 ; n--, r -= side/nmax)
        draw_circle(r);
    draw_tangents(side);
     glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
     glEnable(GL_LIGHTING);
    glEndList();
}

void CGLSampleTrivTable::draw_circle(GLfloat radius)
{
    glBegin(GL_LINE_LOOP);
    for (int i=0; i < 360; i++)
    {
        glColor3f(_color[0],_color[0], _color[0]);
        glVertex3f(cos(BGM((GLfloat)i))*radius, 0.f, sin(BGM((GLfloat)i))*radius);
    }
    glEnd();
}

void CGLSampleTrivTable::draw_tangents(GLfloat radius)
{
    glBegin(GL_LINES);
    int step = 30;
    for (int i=0; i < 360; i+=step )
    {
        glColor3f(_color[0],_color[0], _color[0]);
        glVertex3f( 0.f, 0.f, 0.f);
        glVertex3f(cos(BGM((GLfloat)i))*radius, 0.f, sin(BGM((GLfloat)i))*radius);
    }
    glEnd();
}

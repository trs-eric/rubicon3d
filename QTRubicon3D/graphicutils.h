#ifndef __GRAPHICUTILS_H__
#define __GRAPHICUTILS_H__

#include <cmath>
#include <vector>
#include "ciostate.h"
#include "carray2d.h"
#include <QImage>

static const float cPI = 3.1415926535897932384626433832795f;
static const float cPI2 = 6.283185307179586476925286766559f;


inline float BGM(float v){return v/360.0f*cPI2;}
inline float IBGM(float v) {return v*360.0f/cPI2;}

struct CIPixel
{
    CIPixel(float u = 0, float v = 0) : _u(u), _v (v){}
    CIPixel(const CIPixel & src)
    {
        _u = src._u;
        _v = src._v;
    }

    float  _u; // varies in the range [0, Nx-1]
    float  _v; // varies in the range [0, Ny-1]
};

// absract class representing 3D point
struct C3DPoint : public CIOState
{
    float _angle;
    float _x;
    float _y;
    float _z;

    float _r;
    float _g;
    float _b;

    float _nx;
    float _ny;
    float _nz;

    int _scanline;
    static const float _ioresv[2];
    std::vector<int> _faceidx;

    C3DPoint(float x=0, float y=0, float z=0) :
       _x(x), _y(y), _z(z), _r(1), _g(1), _b(1), _angle(0),
       _nx(0), _ny(0), _nz(0), _scanline(0)
        {}

    C3DPoint(const C3DPoint & src)
    {
        _x = src._x;
        _y = src._y;
        _z = src._z;

        _nx = src._nx;
        _ny = src._ny;
        _nz = src._nz;

        _r = src._r;
        _g = src._g;
        _b = src._b;

        _scanline = src._scanline;

        _angle = src._angle;
        _faceidx = src._faceidx;
    }
    void load(std::ifstream &ifile);
    void save(std::ofstream &ofile);
};


struct Vector2f : public CIOState
{
    float x, y;

    Vector2f() : x(0), y(0) {}

    Vector2f(const Vector2f & src)
    {
        x = src.x;
        y = src.y;
    }
    void load(std::ifstream &ifile);
    void save(std::ofstream &ofile);
};

struct Vector3f : public CIOState
{
    float x, y, z;
    Vector3f() : x(0), y(0), z(0) {}

    void load(std::ifstream &ifile);
    void save(std::ofstream &ofile);
};

struct C3DPointIdx : public CIOState
{
    std::vector<int> pnt_index;
    std::vector<int> tex_index;
    C3DPoint tri_normal;

    C3DPointIdx() : pnt_index(3,0), tex_index(3,0)
    {
    }

    ~C3DPointIdx()
    {
    }

    C3DPointIdx(const C3DPointIdx & src) : pnt_index(3,0), tex_index(3,0)
    {
        pnt_index[0] = src.pnt_index[0];
        pnt_index[1] = src.pnt_index[1];
        pnt_index[2] = src.pnt_index[2];

        tex_index[0] = src.tex_index[0];
        tex_index[1] = src.tex_index[1];
        tex_index[2] = src.tex_index[2];

        tri_normal = src.tri_normal;
    }


    C3DPointIdx& operator=( const C3DPointIdx& src )
    {
        pnt_index[0] = src.pnt_index[0];
        pnt_index[1] = src.pnt_index[1];
        pnt_index[2] = src.pnt_index[2];

        tex_index[0] = src.tex_index[0];
        tex_index[1] = src.tex_index[1];
        tex_index[2] = src.tex_index[2];

        tri_normal = src.tri_normal;

        return *this;
    }

    void load(std::ifstream &ifile);
    void save(std::ofstream &ofile);

};

struct CTriMesh : public CIOState
{
    // the points contains also normals information
    std::vector<C3DPoint>	 _points;
    std::vector<C3DPointIdx> _triangles;
    std::vector<Vector2f>    _tex_coords;

    QImage * _texture;
    std::string _texture_name;

    float _tex_bb_maxy;
    float _tex_bb_miny;
    float _tex_init_angle;

    Vector3f _mass_center;
    Vector3f _trans;
    Vector3f _scale;
    Vector3f _rotat;


    CTriMesh();
    CTriMesh(const CTriMesh & src);

    ~CTriMesh();
    void replace_texture(QImage * tex);
    void remove_texture();

    void init_mass_center();
    void translate(float dx, float dy, float dz);
    void scale(float sx, float sy, float sz);
    void rotate(float rx, float ry, float rz);
    void rotate_rev(float rx, float ry, float rz);

    inline void rotateX(float cos_alpha, float sin_alpha, float * x, float * y, float * z);
    inline void rotateY(float cos_alpha, float sin_alpha, float * x, float * y, float * z);
    inline void rotateZ(float cos_alpha, float sin_alpha, float * x, float * y, float * z);

    void load(std::ifstream &ifile);
    void save(std::ofstream &ofile);
};

C3DPoint compute_avg_pnt(std::vector<C3DPoint> pnts);
C3DPoint compute_avg_pnt(C3DPoint & p1, C3DPoint & p2);

C3DPoint compute_vector_rotation(float alpha, float betta, C3DPoint & v);
void compensate_normal(C3DPoint & v, float r, C3DPoint & n);
C3DPoint add_points(C3DPoint & v1, C3DPoint & v2);

inline float cos_alpha_btw_vectors(float x1, float x2,  float x3, float y1, float y2, float y3)
{
    return x1 * y1 + x2 * y2+x3 * y3;
}

C3DPoint compute_normal(C3DPoint & p1, C3DPoint & p2, C3DPoint & p3);

void normalize3(float & x, float & y, float & z);

CArray2D<double>  convert_RGB_to_gray(QImage * img);

float compute_sq_dist(C3DPoint & p1, C3DPoint & p2);

float circular_modulo(float angle);

inline int changeBrightness( int value, int brightness) {
    return qBound<int>(0, value + brightness, 255);
}

inline int changeContrast( int value, int contrast ) {
    return qBound<int>(0, int(( value - 127 ) * contrast / 127 ) + 127, 255 );
}

inline int changeVibrance( int value, int vibrance ) {
    return qBound<int>(0, int( pow( value / 255.0, 100.0 / (vibrance) ) * 255 ), 255 );
}


inline int changeHue( int value, int hue) {
    return qBound<int>(0, value + hue, 359 );
}


inline int changeSaturation( int value, int sat) {
    return qBound<int>(0, value + sat, 255 );
}

#endif // __GRAPHICUTILS_H__

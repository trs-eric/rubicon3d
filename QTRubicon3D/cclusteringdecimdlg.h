#ifndef CCLUSTERINGDECIMDLG_H
#define CCLUSTERINGDECIMDLG_H

#include <QDialog>

class CGLWidget;
class CTriangularMesh;
class CClusteringDecimThread;

namespace Ui {
class CClusteringDecimDlg;
}

class CClusteringDecimDlg : public QDialog
{
    Q_OBJECT

public:
    explicit CClusteringDecimDlg(CGLWidget * glWidget, QWidget *parent = 0);
    ~CClusteringDecimDlg();

protected:
    void showEvent(QShowEvent * event);
    CTriangularMesh * restore_mesh();
private:
    Ui::CClusteringDecimDlg *ui;

    CGLWidget *_glWidget;
    bool _in_process;
    bool _on_apply;
    bool _is_result_valid;

    ///CTriangularMesh * _inmesh;

    CClusteringDecimThread * _thread;
private slots:
    void onCancelBtn();
    void onPreviewBtn();
    void onApplyBtn();

    void onMakeProgressStep(int val);
    void onCompleteProcessing(CTriangularMesh * );
    void onStopProcessing();

    void close_dialog();

protected:

};

#endif // CCLUSTERINGDECIMDLG_H

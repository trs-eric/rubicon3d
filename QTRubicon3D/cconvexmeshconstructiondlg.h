#ifndef CCONVEXMESHCONSTRUCTIONDLG_H
#define CCONVEXMESHCONSTRUCTIONDLG_H

#include <QDialog>

class CGLWidget;
class CTriangularMesh;
class CConvexMeshConstructionThread;

namespace Ui {
class CConvexMeshConstructionDlg;
}

class CConvexMeshConstructionDlg : public QDialog
{
    Q_OBJECT

public:
    explicit CConvexMeshConstructionDlg(CGLWidget * glWidget, QWidget *parent = 0);
    ~CConvexMeshConstructionDlg();

protected:
    void showEvent(QShowEvent * event);

private:
    Ui::CConvexMeshConstructionDlg *ui;

    CGLWidget *_glWidget;
    bool _in_process;
    bool _on_apply;
    bool _is_result_valid;

    CConvexMeshConstructionThread * _thread;


private slots:
    void onCancelBtn();
    void onPreviewBtn();
    void onApplyBtn();

    void onMakeProgressStep(int val);
    void onCompleteProcessing(CTriangularMesh * );
    void onStopProcessing();

    void close_dialog();

protected:
    void process_2_scan_lines();
    void process_2_scan_lines_bckp();

};

#endif // CCONVEXMESHCONSTRUCTIONDLG_H

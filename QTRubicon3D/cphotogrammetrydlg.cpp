#include "cphotogrammetrydlg.h"
#include "ui_cphotogrammetrydlg.h"
#include <QFileDialog>

CPhotogrammetryDlg::CPhotogrammetryDlg(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::CPhotogrammetryDlg), _ccd_width(6.2)
{
    ui->setupUi(this);

    QDir dir;
    _img_dir = dir.absolutePath();

    ui->imgPathEdt->setText(_img_dir);
    ui->ccdWEdt->setText(QString::number(_ccd_width));

    connect(ui->imgBrowseBtn, SIGNAL(clicked()), this, SLOT(onImgBrowseBtn()));

    connect(ui->ccdWEdt, SIGNAL(textChanged(const QString &)),   this, SLOT(set_ccd_width(const QString &)));
    connect(ui->cancelBtn,  SIGNAL(clicked()), this, SLOT(onCancelBtn()));

    // set _img_dir as app_path
}

CPhotogrammetryDlg::~CPhotogrammetryDlg()
{
    delete ui;
}

void CPhotogrammetryDlg::onImgBrowseBtn()
{
    // set _img_dir as start point
    QString dir = QFileDialog::getExistingDirectory(this, tr("Open Directory"),
                                                 _img_dir,
                                                 QFileDialog::ShowDirsOnly
                                                 | QFileDialog::DontResolveSymlinks);

    _img_dir = dir;
    ui->imgPathEdt->setText(dir);
}

void CPhotogrammetryDlg::set_ccd_width(const QString & str)
{
    bool ok = false;
    float cval = str.toFloat(&ok);
    if(ok)
        _ccd_width = cval;
        // ui->oct_depth->setText(QString::number(cval));
}

void CPhotogrammetryDlg::onCancelBtn()
{
    close();
}

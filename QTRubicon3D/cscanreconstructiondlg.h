#ifndef CSCANRECONSTRUCTIONDLG_H
#define CSCANRECONSTRUCTIONDLG_H

#include <QDialog>

class CGLWidget;
class CScanReconstructionThread;

namespace Ui {
class CScanReconstructionDlg;
}

class CScanReconstructionDlg : public QDialog
{
    Q_OBJECT

public:
    explicit CScanReconstructionDlg(CGLWidget * glWidget, QWidget *parent = 0);
    ~CScanReconstructionDlg();

protected:
    void showEvent(QShowEvent * event);

private:
    Ui::CScanReconstructionDlg *ui;
    CGLWidget *_glWidget;

    bool _in_process;
    CScanReconstructionThread * _thread;

    enum ComboAngles {cmb045=0, cmb09, cmd18};
    enum ComboLasers {cmbLeft=0, cmbRight=1, cmdBoth=2};


private slots:
    void onCancelBtn();
    void onStartBtn();

    void onMakeTimeElapsed(QString);
    void onMakeTimeETA(QString);
    void onMakeProgressStep(int val);
    void onCompleteProcessing();
    void onStopProcessing();


    void close_dialog();
    void angle_step_changed(int);

    void slidMPowchanged(int);
    void slidMStepchanged(int);
    //void slidPausechanged(int);
};

#endif // CSCANRECONSTRUCTIONDLG_H

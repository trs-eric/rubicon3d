#include "mainwindow.h"
#include "ui_mainwindow.h"
#include<QPushButton>
#include<QLineEdit>
#include<QFormLayout>
#include<QToolBar>
#include<QMenuBar>
#include<QMessageBox>
#include<QFileDialog>
#include <QCloseEvent>

#include "cglwidget.h"
#include "messages.h"
#include "cimportmanager.h"
#include "cexportmanager.h"
#include "cpointoptimizedlg.h"
#include "cnormalestimationdlg.h"
#include "cmeshconstructiondlg.h"
#include "cconvexmeshconstructiondlg.h"
#include "cphotogrammetrydlg.h"
#include "cdevicecontroldlg.h"
#include "ccameraadjustmentdlg.h"
#include "cscanreconstructiondlg.h"
#include "ccropdiskdlg.h"
#include "cclusteringdecimdlg.h"
#include "cmovrotscadlg.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow), _is_actul_scene(true)
{
    createActions();
    createMenus();
    createToolBars();
    createStatusBar();

    ui->setupUi(this);

    glWidget = new CGLWidget(ui->treeWidget);
    setCentralWidget(glWidget);

    createAuxDialogs();

    _appName            = "Rubicon3D";
    _current_project    = "";
    _title              = _appName;
    this->setWindowTitle(_title);

    ///////////////////////////////////////////////////

    ui->dockWidget->setWidget(ui->treeWidget); //fill central widget

    ui->treeWidget->setItemsExpandable(false); // donot expand at all
    ui->treeWidget->setRootIsDecorated(false); // hide [-] sign

    //ui->treeWidget->set
    //ui->treeWidget->setStyleSheet( "QTreeView::branch {  border-image: url(none.png); }" );
    ui->treeWidget->setColumnCount(5);
    //ui->treeWidget->header()->setStretchLastSection(false);

    ui->treeWidget->setColumnWidth(0, 20);
    //ui->treeWidget->header()->setSectionResizeMode(4,  QHeaderView::Stretch);
    ui->treeWidget->setColumnWidth(1, 20);
    //ui->treeWidget->header()->setSectionResizeMode(1,  QHeaderView::Interactive);
    ui->treeWidget->setColumnWidth(2, 20);
    //ui->treeWidget->header()->setSectionResizeMode(2,  QHeaderView::Interactive);
    ui->treeWidget->setColumnWidth(3, 20);
    //ui->treeWidget->header()->setSectionResizeMode(3,  QHeaderView::Interactive);
    ui->treeWidget->setColumnWidth(4, 20);
    //ui->treeWidget->header()->setSectionResizeMode(4,  QHeaderView::Interactive);


    ui->treeWidget->setSelectionMode(QAbstractItemView::SingleSelection);
    ui->treeWidget->setSelectionBehavior(QAbstractItemView::SelectItems);
    ui->treeWidget->setHeaderHidden(true);


    ui->treeWidget->expandAll();
    //////////////////////////////////////////////////

    connect(ui->treeWidget,
            SIGNAL(itemDoubleClicked(QTreeWidgetItem *, int)),
            glWidget, SLOT(on_double_click_treew_item(QTreeWidgetItem *, int)));
    connect(ui->treeWidget,
            SIGNAL(itemClicked(QTreeWidgetItem *, int)),
            glWidget, SLOT(on_single_click_treew_item(QTreeWidgetItem *, int)));

}

MainWindow::~MainWindow()
{
    delete ui;
    delete newAct;

}

void MainWindow::closeEvent(QCloseEvent *event)
{
    if(!_is_actul_scene)
    {
          QMessageBox::StandardButton reply =
                  QMessageBox::warning(this, tr("Warning"),
                                       wrn_DiscardSceneChanges, QMessageBox::Yes|QMessageBox::No);
           if (reply == QMessageBox::No)
           {
                event->ignore();
                return;
           }
    }

    delete _poptDlg;
    delete _normeDlg;
    delete _meshCDlg;
    delete _scanDlg;
    delete _cropDlg;
    delete _cvxMeshCDlg;
    delete _clDesimDlg;
    delete _mrsDlg;

    QMainWindow::closeEvent(event);
}

void MainWindow::createAuxDialogs()
{
    _poptDlg  = new CPointOptimizeDlg(glWidget);
    _normeDlg = new CNormalEstimationDlg(glWidget);
    _meshCDlg = new CMeshConstructionDlg(glWidget);
    _cvxMeshCDlg = new CConvexMeshConstructionDlg(glWidget);
    _scanDlg = new CScanReconstructionDlg(glWidget);
    _cropDlg = new CCropDiskDlg(glWidget);
    _clDesimDlg = new CClusteringDecimDlg(glWidget);
    _mrsDlg = new CMovRotScaDlg(glWidget);
}

void MainWindow::createActions()
{
    newAct = new QAction(QIcon(":/images/icons/ico_new.png"), tr("&New"), this);
    newAct->setShortcuts(QKeySequence::New);
    newAct->setStatusTip(msg_newScene);
    connect(newAct, SIGNAL(triggered()), this, SLOT(new_scene()));

    openAct = new QAction(QIcon(":/images/icons/ico_open.png"), tr("&Open..."), this);
    openAct->setShortcuts(QKeySequence::Open);
    openAct->setStatusTip(tr("Open an existing file"));
    connect(openAct, SIGNAL(triggered()), this, SLOT(load_project()));

    saveAct = new QAction(QIcon(":/images/icons/ico_save.png"), tr("&Save"), this);
    saveAct->setShortcuts(QKeySequence::Save);
    saveAct->setStatusTip(tr("Save the document to disk"));
    connect(saveAct, SIGNAL(triggered()), this, SLOT(save_project()));

    saveAsAct = new QAction(tr("Save &As..."), this);
    saveAsAct->setShortcuts(QKeySequence::SaveAs);
    saveAsAct->setStatusTip(tr("Save the document under a new name"));
    connect(saveAsAct, SIGNAL(triggered()), this, SLOT(save_as_project()));

    importAct = new QAction(tr("Import..."), this);
    //importAct->setShortcuts(QKeySequence::SaveAs);
    importAct->setStatusTip(tr("Import geometry into the scene"));
    connect(importAct, SIGNAL(triggered()), this, SLOT(import_scene()));

    exportAct = new QAction(tr("Export..."), this);
    //importAct->setShortcuts(QKeySequence::SaveAs);
    exportAct->setStatusTip(tr("Export geometry into an extrenal file"));
    connect(exportAct, SIGNAL(triggered()), this, SLOT(export_scene()));

    aboutAct = new QAction(tr("&About Rubicon3D ..."), this);
    aboutAct->setStatusTip(tr("Show the application's About box"));
    connect(aboutAct, SIGNAL(triggered()), this, SLOT(about()));

    exitAct = new QAction(tr("E&xit"), this);
    exitAct->setShortcuts(QKeySequence::Quit);
    exitAct->setStatusTip(tr("Exit the application"));
    connect(exitAct, SIGNAL(triggered()), this, SLOT(close()));

    pptAct = new QAction(tr("Import images ..."), this);
    //ppt->setShortcuts(QKeySequence::SaveAs);
    pptAct->setStatusTip(tr("Construct point cloud for a series of images"));
    connect(pptAct, SIGNAL(triggered()), this, SLOT(import_photogrammetry()));

    scanAct= new QAction(QIcon(":/images/icons/ico_scan.png"), tr("Scan"), this);
    // scanAct->setShortcuts(QKeySequence::New);
    scanAct->setStatusTip(tr("Scanning"));
    connect(scanAct, SIGNAL(triggered()), this, SLOT(scanning()));

    optimAct= new QAction(QIcon(":/images/icons/ico_opt.png"), tr("Optimize"), this);
    // optimAct->setShortcuts(QKeySequence::New);
    optimAct->setStatusTip(tr("Point clouds optimization"));
    connect(optimAct, SIGNAL(triggered()), this, SLOT(optimizePntsDlg()));

    pnormAct= new QAction(QIcon(":/images/icons/ico_norm.png"), tr("Normals"), this);
    // pnormAct->setShortcuts(QKeySequence::New);
    pnormAct->setStatusTip(tr("Point clouds normal estimation"));
    connect(pnormAct, SIGNAL(triggered()), this, SLOT(normalsEstDlg()));

    meshAct= new QAction(QIcon(":/images/icons/ico_mesh.png"), tr("Poisson Mesh"), this);
    // meshAct->setShortcuts(QKeySequence::New);
    meshAct->setStatusTip(tr("Mesh recontsruction with Poisson algorithm"));
    connect(meshAct, SIGNAL(triggered()), this, SLOT(meshConstDlg()));

    meshActS= new QAction(QIcon(":/images/icons/ico_mesh.png"), tr("Convex Mesh"), this);
    // meshAct->setShortcuts(QKeySequence::New);
    meshActS->setStatusTip(tr("Mesh convex recontsruction (simple)"));
    connect(meshActS, SIGNAL(triggered()), this, SLOT(delaunay_mesh_test()));


    meshActD = new QAction(QIcon(":/images/icons/ico_mesh.png"), tr("Optim Mesh"), this);
    meshActD->setStatusTip(tr("Mesh optimization via decimation"));
    connect(meshActD, SIGNAL(triggered()), this, SLOT(mesh_decimation()));

    meshOpt = new QAction(tr("Mesh optimization"), this);
    meshOpt->setStatusTip(tr("Mesh optimization with clustering decimation"));
    connect(meshOpt, SIGNAL(triggered()), this, SLOT(tobeimplented()));

    meshLSmooth = new QAction(tr("Mesh smoothing"), this);
    meshLSmooth->setStatusTip(tr("Mesh smoothing with Laplacian operator"));
    connect(meshLSmooth, SIGNAL(triggered()), this, SLOT(tobeimplented()));

    texAct= new QAction(QIcon(":/images/icons/ico_texture.png"), tr("Texture"), this);
    // texAct->setShortcuts(QKeySequence::New);
    texAct->setStatusTip(tr("Texture reconstruction"));
    connect(texAct, SIGNAL(triggered()), this, SLOT(tobeimplented()));

    calibAct= new QAction(QIcon(":/images/icons/ico_calib.png"), tr("Calibrate"), this);
    // calibAct->setShortcuts(QKeySequence::New);
    calibAct->setStatusTip(tr("Camera calibration"));
    connect(calibAct, SIGNAL(triggered()), this, SLOT(device_calibration()));


    movAct = new QAction(QIcon(":/images/icons/tool_move.png"), tr("Move, Rotate, Scale"), this);
    movAct->setStatusTip(tr("Move, rotate, scale of mesh"));
    connect(movAct, SIGNAL(triggered()), this, SLOT(transform_mesh()));

    rotAct= new QAction(QIcon(":/images/icons/tool_rotate.png"), tr("Rotate"), this);
    rotAct->setStatusTip(tr("Rotate point cloud"));
    connect(rotAct, SIGNAL(triggered()), this, SLOT(tobeimplented()));

    cropAct = new QAction(QIcon(":/images/icons/tool_crop.png"), tr("Trim"), this);
    cropAct->setStatusTip(tr("Crop"));
    connect(cropAct, SIGNAL(triggered()), this, SLOT(cropDiskDlg()));

    erasAct = new QAction(QIcon(":/images/icons/tool_erase.png"), tr("Erase"), this);
    erasAct->setStatusTip(tr("Erase"));
    connect(erasAct, SIGNAL(triggered()), this, SLOT(tobeimplented()));

    aligAct = new QAction(QIcon(":/images/icons/tool_magner.png"), tr("Align"), this);
    aligAct->setStatusTip(tr("Align"));
    connect(aligAct, SIGNAL(triggered()), this, SLOT(tobeimplented()));

}

void MainWindow::new_scene()
{
    if(!_is_actul_scene)
    {
          QMessageBox::StandardButton reply =
                  QMessageBox::warning(this, tr("Warning"),
                                       wrn_DiscardSceneChanges, QMessageBox::Yes|QMessageBox::No);
           if (reply == QMessageBox::No)
                        return;
    }

    glWidget->make_new_scene();

    _is_actul_scene = true;
    _title = _appName;
    _current_project = "";
    this->setWindowTitle(_title);
}

void MainWindow::device_calibration()
{
    CCameraAdjustmentDlg dlg(glWidget);
    dlg.exec();
}
void MainWindow::scanning()
{
    _is_actul_scene = false;
   if(_scanDlg)
       _scanDlg->show();
}

void MainWindow::load_project()
{
    if(!_is_actul_scene)
    {
          QMessageBox::StandardButton reply =
                  QMessageBox::warning(this, tr("Warning"),
                                       wrn_DiscardSceneChanges, QMessageBox::Yes|QMessageBox::No);
           if (reply == QMessageBox::No)
                        return;
    }

   QString fileName = QFileDialog::getOpenFileName(this,
         tr("Open project ..."), "", "Rubicon3D project (*.rub)");

   if (fileName.isEmpty()) return;

   _current_project = fileName;
   glWidget->make_new_scene();
   glWidget->load_project(_current_project);
   _is_actul_scene = true;
   _title = _appName + " - " + _current_project;
   setWindowTitle(_title);
}

void MainWindow::save_project()
{
    if(_current_project.isEmpty())
    {
        QString fileName = QFileDialog::getSaveFileName(this,
             tr("Save project ..."), "", "Rubicon3D project (*.rub)");

        if (fileName.isEmpty())
                              return;
        _current_project = fileName;
    }

    glWidget->save_project(_current_project);
    _is_actul_scene = true;
    _title = _appName + " - " + _current_project;
    setWindowTitle(_title);
}

void MainWindow::save_as_project()
{
    QString fileName = QFileDialog::getSaveFileName(this,
             tr("Save project as ..."), "", "Rubicon3D project (*.rub)");

    if (fileName.isEmpty()) return;

    _current_project = fileName;
    glWidget->save_project(_current_project);
    _is_actul_scene = true;
    _title = _appName + " - " + _current_project;
    setWindowTitle(_title);
}


void MainWindow::optimizePntsDlg()
{
    _is_actul_scene = false;
    if(glWidget->get_raw_points_of_current_object() == NULL)
    {
        QMessageBox::warning(this, tr("Warning"), wrn_NothingOptim);
        return;
    }
    if(_poptDlg)
        _poptDlg->show();
}

void MainWindow::cropDiskDlg()
{
    if(glWidget->get_raw_points_of_current_object() == NULL)
    {
        QMessageBox::warning(this, tr("Warning"), wrn_NoRawPoints);
        return;
    }

    _is_actul_scene = false;
    if(_cropDlg)
        _cropDlg->show();
}

void MainWindow::import_photogrammetry()
{
    CPhotogrammetryDlg pptDlg;
    pptDlg.exec();

}
void MainWindow::normalsEstDlg()
{
     _is_actul_scene = false;
    if(glWidget->get_optim_points_of_current_object() == NULL)
    {
        QMessageBox::warning(this, tr("Warning"), wrn_NothingNormal);
        return;
    }
    if(_normeDlg)
        _normeDlg->show();
}

void MainWindow::meshConstDlg()
{
    _is_actul_scene = false;
    if(glWidget->get_mesh_of_current_object() == NULL)
    {
        QMessageBox::warning(this, tr("Warning"), wrn_NothingMesh);
        return;
    }

    if(_meshCDlg)
        _meshCDlg->show();
}

void MainWindow::createMenus()
{
    fileMenu = menuBar()->addMenu(tr("&File"));
    fileMenu->addAction(newAct);
    fileMenu->addAction(openAct);
    fileMenu->addAction(saveAct);
    fileMenu->addAction(saveAsAct);
    fileMenu->addSeparator();
    fileMenu->addAction(importAct);
    fileMenu->addAction(exportAct);
    //fileMenu->addSeparator();
    //fileMenu->addAction(pptAct);
    fileMenu->addSeparator();
    fileMenu->addAction(exitAct);

    actMenu = menuBar()->addMenu(tr("&Action"));
    actMenu->addAction(scanAct);
    actMenu->addAction(cropAct);
    actMenu->addAction(meshActS);
    actMenu->addAction(meshActD);
    //actMenu->addAction(aligAct);
    //actMenu->addAction(meshOpt);
    //actMenu->addAction(meshLSmooth);
    actMenu->addAction(movAct);
    actMenu->addAction(meshAct);

    viewMenu = menuBar()->addMenu(tr("&View"));
    //viewMenu->addAction(newAct);
    //viewMenu->addAction(copyAct);

    menuBar()->addSeparator();

    helpMenu = menuBar()->addMenu(tr("&Help"));
    helpMenu->addAction(aboutAct);
    //helpMenu->addAction(aboutQtAct);
}

void MainWindow::createToolBars()
{
    mainToolBar = addToolBar(tr("Main"));
    mainToolBar->addAction(newAct);
    mainToolBar->addAction(openAct);
    mainToolBar->addAction(saveAct);


    toolsToolBar = addToolBar(tr("Tools"));
    toolsToolBar->addAction(scanAct);
    ///toolsToolBar->addAction(optimAct);
    ///toolsToolBar->addAction(pnormAct);
    ///toolsToolBar->addAction(meshAct);
    toolsToolBar->addAction(meshActS);
    //toolsToolBar->addAction(meshActD);
    ///toolsToolBar->addAction(texAct);
    toolsToolBar->addAction(calibAct);

    //toolsAuxToolBar = addToolBar(tr("Individual tools"));
    //toolsAuxToolBar->addAction(movAct);
    //toolsAuxToolBar->addAction(rotAct);
    //toolsAuxToolBar->addAction(cropAct);
    //toolsAuxToolBar->addAction(erasAct);
    //toolsAuxToolBar->addAction(aligAct);


}
void MainWindow::createStatusBar()
{
}


void MainWindow::about()
{
   QMessageBox::about(this, tr("About Rubicon3D ... "),
            tr("The <b>Rubicon3D</b> v.1.0<br><br>"
               "Copyright (C) 2014<br><br>"
               "Developed by Anton Myagotin<br>"
               "<a href=\"mailto:anton.myagotin@gmail.com\">anton.myagotin@gmail.com</a>"));
}


void MainWindow::tobeimplented()
{
    QMessageBox::about(this, tr("Warning"),
             tr("This feature will be implemented soon"));
}


void MainWindow::import_scene()
{
    _is_actul_scene = false;
    QString fileName = QFileDialog::getOpenFileName(this,
         tr("Import data ..."), "", CImportManager::importFileTypes);

    if (!fileName.isEmpty())
        glWidget->import_scene(fileName);
}

void MainWindow::export_scene()
{
    QString fileName = QFileDialog::getSaveFileName(this,
         tr("Export data ..."), "", CExportManager::exportFileTypes);

   if (!fileName.isEmpty())
        glWidget->export_scene(fileName);

}


void MainWindow::delaunay_mesh_test()
{

    if(glWidget->get_raw_points_of_current_object() == NULL)
    {
        QMessageBox::warning(this, tr("Warning"), wrn_NoRawPoints);
        return;
    }

    _is_actul_scene = false;
    if(_cvxMeshCDlg)
        _cvxMeshCDlg->show();
}


void MainWindow::mesh_decimation()
{

    if(glWidget->get_mesh_of_current_object() == NULL)
    {
        QMessageBox::warning(this, tr("Warning"), wrn_NoMesh);
        return;
    }

    _is_actul_scene = false;
    if(_clDesimDlg)
        _clDesimDlg->show();


}

void MainWindow::transform_mesh()
{
    if(glWidget->get_mesh_of_current_object() == NULL)
    {
        QMessageBox::warning(this, tr("Warning"), wrn_NoMesh);
        return;
    }

    _is_actul_scene = false;
    if(_mrsDlg)
        _mrsDlg->show();
}



#include "cnormalcloud.h"
#include "graphicutils.h"

CNormalCloud::CNormalCloud()
{
    _color[0] = 1.0f;
    _color[1] = 1.0f;
    _color[2] = 1.0f;

    _rx = 0.05f;
    _ry = 0.05f;
    _rz = 0.05f;

    _alphaN = 24;
    _bettaN = 24;
    _aux_s = 1;
    _aux_r = 2;

}

int CNormalCloud::find_proper_normal(C3DPoint & p)
{
    std::vector<C3DPoint>::iterator it = _tnormals.begin();
    float cos_max = 0;
    int idx = 0;
    for( int i =0 ; it != _tnormals.end(); it++, i++)
    {
        float cval = cos_alpha_btw_vectors(it->_x, it->_y, it->_z, p._nx, p._ny, p._nz);
        if(cval > cos_max)
        {
            idx = i;
            cos_max =cval;
        }
    }
    return idx;
}

void CNormalCloud::construct_nv_table()
{
    _tnormals.clear();
    _tv0.clear();
    _tv1.clear();
    _tv2.clear();
    _tv3.clear();

    C3DPoint nt;
    nt._x = 0; nt._y = 0; nt._z = 1;

    C3DPoint v0, v1, v2, v3;
    v0._x =  _aux_s; v0._y =  _aux_s; v0._z = _aux_r;
    v1._x =  _aux_s; v1._y = -_aux_s; v1._z = _aux_r;
    v2._x = -_aux_s; v2._y = -_aux_s; v2._z = _aux_r;
    v3._x = -_aux_s; v3._y =  _aux_s; v3._z = _aux_r;

    for(int a = 0; a <= _alphaN ; a++)
        for(int b =0; b <= _bettaN; b++)
        {
            float alpha = float(a) *     cPI / float(_alphaN);
            float betta = float(b) * 2 * cPI / float(_bettaN);

            C3DPoint rnt  = compute_vector_rotation(alpha, betta, nt);
            _tnormals.push_back(rnt);

            C3DPoint rv0 = compute_vector_rotation(alpha, betta, v0);
            compensate_normal(rv0, _aux_r, rnt);
            _tv0.push_back(rv0);

            C3DPoint rv1 = compute_vector_rotation(alpha, betta, v1);
            compensate_normal(rv1, _aux_r, rnt);
            _tv1.push_back(rv1);

            C3DPoint rv2 = compute_vector_rotation(alpha, betta, v2);
            compensate_normal(rv2, _aux_r, rnt);
            _tv2.push_back(rv2);

            C3DPoint rv3 = compute_vector_rotation(alpha, betta, v3);
            compensate_normal(rv3, _aux_r, rnt);
            _tv3.push_back(rv3);

        }

}

/* void CNormalCloud::create_glList()
{
    // create new display list
    _oglid = glGenLists(1);
    glNewList(_oglid, GL_COMPILE);

 // TODO remake
 glDisable(GL_LIGHTING);

    glBegin(GL_POINTS);
    std::vector<C3DPoint>::iterator it = _normals.begin();
    for( ; it != _normals.end(); it++)
    {
       glColor3f(_color[0], _color[1], _color[2]);
       glVertex3f(it->_x, it->_y, it->_z);
    }
    glEnd();

    glBegin(GL_LINES);
    it = _normals.begin();
    for( ; it != _normals.end(); it++)
    {
       glColor3f(1, 1, 1);
       glVertex3f(it->_x, it->_y, it->_z);
       glColor3f(1, 0, 0);
       glVertex3f(it->_x+2*it->_nx, it->_y+2*it->_ny, it->_z+2*it->_nz);

    }
    glEnd();

// TODO remake
glEnable(GL_LIGHTING);
    glEndList();
}*/

void CNormalCloud::save(std::ofstream & ofile)
{
    int npoint_count = _normals.size();
    ofile.write(reinterpret_cast<const char *>(&npoint_count), sizeof(int));

    std::vector<C3DPoint>::iterator it = _normals.begin();
    for( ; it != _normals.end(); it++)
        it->save(ofile);
}

void CNormalCloud::load(std::ifstream & ifile)
{
    delete_glLists();
    _normals.clear();

    int npoint_count = 0;
    ifile.read(reinterpret_cast<char *>(&npoint_count), sizeof(int));
    for(int i = 0; i < npoint_count ; i++)
    {
        C3DPoint n;
        n.load(ifile);
        _normals.push_back(n);
    }
}

float CNormalCloud::find_quick_smallest_cloud_distance()
{
    int max_check = 1000;
    int cur_size = _normals.size();
    int check_size = (cur_size < max_check )?cur_size:max_check;

    float min_dist_sq;
    std::vector<C3DPoint>::iterator it = _normals.begin();
    for( int i = 0 ; i < check_size; it++, i++)
    {
            std::vector<C3DPoint>::iterator itn = it+1;
            for( int j = i+1  ; j < check_size; itn++, j++)
            {

                float cur_dist =(it->_x - itn->_x)*(it->_x - itn->_x) +
                                (it->_y - itn->_y)*(it->_y - itn->_y) +
                                (it->_z - itn->_z)*(it->_z - itn->_z);
                if(i == 0 && j == 1)
                {
                   min_dist_sq = cur_dist;
                   continue;
                }
                if(  cur_dist < min_dist_sq)
                            min_dist_sq = cur_dist;

            }
    }
    return min_dist_sq;
}

void CNormalCloud::create_glList()
{

    delete_glLists();

    _aux_s = 0.35 * sqrt(find_quick_smallest_cloud_distance());
    construct_nv_table();


    // create new display list
    _oglid = glGenLists(1);
    glNewList(_oglid, GL_COMPILE);

    // TODO remake

    glBegin(GL_QUADS);
    std::vector<C3DPoint>::iterator it = _normals.begin();
    for( ; it != _normals.end(); it++)
    {
        int idx = find_proper_normal(*it);

        C3DPoint t_normal; // = _tnormals[idx];
        t_normal._x = it->_nx;
        t_normal._y = it->_ny;
        t_normal._z = it->_nz;


        glNormal3f (t_normal._x, t_normal._y, t_normal._z );
        glColor3f(1, 1, 1);
        C3DPoint p0 = add_points(_tv0[idx], *it);
        glVertex3f(p0._x, p0._y, p0._z);

        glNormal3f (t_normal._x, t_normal._y, t_normal._z );
        glColor3f(1, 1, 1);
        C3DPoint p1 = add_points(_tv1[idx], *it);
        glVertex3f(p1._x, p1._y, p1._z);

        glNormal3f (t_normal._x, t_normal._y, t_normal._z );
        glColor3f(1, 1, 1);
        C3DPoint p2 = add_points(_tv2[idx], *it);
        glVertex3f(p2._x, p2._y, p2._z);

        glNormal3f (t_normal._x, t_normal._y, t_normal._z );
        glColor3f(1, 1, 1);
        C3DPoint p3 = add_points(_tv3[idx], *it);
        glVertex3f(p3._x, p3._y, p3._z);
    }

    glEnd();
   glEndList();
}

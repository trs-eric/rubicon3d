#ifndef CRAWTEXTURE_H
#define CRAWTEXTURE_H

#include <QImage>
#include <QString>
#include "ciostate.h"

struct CRawTexture  : public CIOState
{
    CRawTexture() : _img(NULL){}
    ~CRawTexture(){ if(_img != NULL) delete _img;}
    QImage * _img;
    QString _name;

    void load(std::ifstream &ifile);
    void save(std::ofstream &ofile);


};
#endif // CRAWTEXTURE_H

#ifndef EDGEDETECTION_H
#define EDGEDETECTION_H

#include <vector>
#include <QImage>
#include "graphicutils.h"


std::vector<CIPixel> get_edge_points_lines_1D(QImage * img);
std::vector<CIPixel> get_edge_points_lines_hills_1D(QImage * img);
float * gauss_kernel(int & sz, float sigma);

#endif // EDGEDETECTION_H

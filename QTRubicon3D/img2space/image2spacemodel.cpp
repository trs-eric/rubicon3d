#include "Image2SpaceModel.h"
#include <cmath>


float image_to_space_u(float u, int Nx, float alpha_x, float delta_x, float d, float * Delta_x )
{
    float ux;
    if(Nx % 2)
        ux = float(Nx)/2.f - u;
    else
        ux = float(Nx-1)/2.f - u;

    float K = (2.0f / float(Nx) ) * d * (float)tan(alpha_x / 2.0f);
    *Delta_x = delta_x  * K;
    float ix = ux * K +  *Delta_x;

    return ix;
}


float image_to_space_v(float v, int Ny, float alpha_z, float delta_y, float d)
{
    float vz;
    if(Ny % 2)
        vz = float(Ny)/2.f + delta_y - v;
    else
        vz = float(Ny-1)/2.f + delta_y - v;

    float K = (2.0f / float(Ny) ) * d * (float)tan(alpha_z / 2.0f);
    float iz = vz * K ;

    return iz;
}

float get_point_height(float iz, float py,  float d, float h)
{
    float ph = iz * (d-py) / d;
    float pz = ph + h;
    return pz;

}

void segment_intersection_pnt(float x1, float y1, float x2, float y2, float x3,
                              float y3, float x4, float y4, float * xres, float * yres)
{

    float up = ((x4 - x3)*(y1 - y3) - (y4 - y3) * (x1 - x3) );
    float div = ((y4 - y3)*(x2 - x1) - (x4 - x3) * (y2 - y1) );
    if (div == 0) std::cout << div << std::endl;

    float ua = ((x4 - x3)*(y1 - y3) - (y4 - y3) * (x1 - x3) )/
               ((y4 - y3)*(x2 - x1) - (x4 - x3) * (y2 - y1) );

    (*xres) = x1 + ua * (x2 - x1);
    (*yres) = y1 + ua * (y2 - y1);

}

void point_XY_rotation(float * x, float * y, float angle)
{

    float x0 = *x;
    float y0 = *y;


    (*x) = x0 * (float)cos(angle) - y0 * (float)sin(angle);
    (*y) = x0 * (float)sin(angle) + y0 * (float)cos(angle);

}


std::vector<C3DPoint> image2space_conv
(std::vector<CIPixel> pxls,
 float d, float h,
 float alpha_x, float alpha_z,
 float delta_x, float delta_y,
 float betta,   float scan_step_angle,
 int Nx, int Ny, int angle_step, bool right_laser,
 float max_radii, QImage * src, bool use_col)
{

    std::vector<C3DPoint> res_3d_points;

    //std::vector<CIPixel>::iterator it = pxls.begin();
    //for ( ; it != pxls.end(); it ++)


    int psz = pxls.size();
    QRgb * iBits   = (QRgb*)src->bits();
    int imwidth = src->width();

    //#pragma omp parallel for
    for(int pi = 0; pi < psz ; pi++)
    {
        float pi_u = pxls[pi]._u;
        float pi_v = pxls[pi]._v;

        float Delta_x = 0.0;
        float ix = image_to_space_u(pi_u, Nx, alpha_x, delta_x, d, &Delta_x);

        float xp =0.0, yp =0.0, zp = 0.0;
        segment_intersection_pnt(Delta_x, d, // camera point
                                ix, 0.0f,   // image pixel
                                d * tan(((right_laser)?-betta:betta)), d, // laser point
                                0.0, 0.0, // rotation origin
                                &xp, &yp);


        float iz = image_to_space_v(pi_v, Ny, alpha_z, delta_y, d);
        zp = get_point_height( iz, yp, d, h);


        float rotation_angle = float(angle_step) * scan_step_angle;
        if(right_laser)
            rotation_angle -= 4 * betta; // magic constant 4
        point_XY_rotation(&xp, &yp, rotation_angle);


        C3DPoint pnt_res;
        pnt_res._x = xp;
        pnt_res._y = zp;
        pnt_res._z = yp	;
        pnt_res._angle  = rotation_angle;

        QRgb pix = iBits[int(pi_v) * imwidth + int(pi_u)];

        int pxr = qRed  (pix);
        int pxg = qGreen(pix);
        int pxb = qBlue (pix);

        if(use_col)
        {
            pnt_res._r = float(pxr)/255.f;
            pnt_res._g = float(pxg)/255.f;
            pnt_res._b = float(pxb)/255.f;
        }
        else
        {
            pnt_res._r = 1.0;
            pnt_res._g = 1.0;
            pnt_res._b = 1.0;
        }


        // check this
        // #pragma omp critical
        if( (pnt_res._z*pnt_res._z + pnt_res._x*pnt_res._x) < max_radii*max_radii)
            res_3d_points.push_back(pnt_res);
    }

    return res_3d_points;
}

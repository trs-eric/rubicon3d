#ifndef IMAGE2SPACEMODEL_H
#define IMAGE2SPACEMODEL_H

#include <vector>
#include "graphicutils.h"

float image_to_space_u(float u,  // pixel horizontal index
                       int Nx, // image pixel width
                       float alpha_x, // horizontal camera FOV
                       float delta_x, // horizontal camera-rot offset
                       float d,		  // camera-ceneter distance
                       float * Delta_x // delta_x in mm
                       );

float image_to_space_v(float v,  // pixel vertical index
                       int Ny, // image pixel height
                       float alpha_z, // vertical camera FOV
                       float delta_y, // vertical camera-rot offset
                       float d); // camera-ceneter distance

float get_point_height(float iz, // world pix coorf
                       float py, // real point Y coord
                       float d, // camera-ceneter distance
                       float h); // camera evaliation

void segment_intersection_pnt(float x1, // first segment endpoints
                              float y1,
                              float x2,
                              float y2,
                              float x3, // scond segment endpoints
                              float y3,
                              float x4,
                              float y4,
                              float * xres, // resulting point
                              float * yres
                              );
void point_XY_rotation(float * x, float * y, float angle /*(rad)*/);

std::vector<C3DPoint> image2space_conv (std::vector<CIPixel> pxls,
 float d, float h, float alpha_x, float alpha_z,
 float delta_x, float delta_y,  float betta,   float scan_step_angle,
 int Nx, int Ny, int angle_step, bool right_laser,
                                        float max_radii, QImage *, bool);

#endif // IMAGE2SPACEMODEL_H

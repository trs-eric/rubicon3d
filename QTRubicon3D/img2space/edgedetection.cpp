#include "edgedetection.h"
#include "../settings/psettings.h"

#define Eu_Number 2.71828182845904523536

std::vector<CIPixel> get_edge_points_lines_1D(QImage * img)
{

    std::vector<CIPixel> res;

    int xs = img->width();
    int ys = img->height();

    CArray2D<double> nimg = convert_RGB_to_gray(img);

    double sum_weights = 0;
    double sum_pos = 0;
    for(int y = 0 ; y < ys; y++)
    {
        bool in_line = false;
        for(int x = 0; x < xs; x++)
        {

            if (nimg._data[y][x] < CGlobalSettings::_lcthresh && in_line)
            {
                in_line = false;
                res.push_back(CIPixel(sum_pos/sum_weights, float(y)));
                continue;
            }

            if (nimg._data[y][x] < CGlobalSettings::_lcthresh)
                continue;

            if ( !in_line)
            {
                // found new line
                in_line = true;

                sum_weights = 0;
                sum_pos = 0;
            }

            //if (in_line)
            {
                sum_weights += float(nimg._data[y][x]);
                sum_pos   += float(x) * float(nimg._data[y][x]);
            }

        }
    }
    return res;
}

float * gauss_kernel(int & sz, float sigma)
{
    sz =int( 1 + 2 * ceil(5 * sigma));
    if (sz < 5) sz = 5;
    float * kernel = new float[sz];
    int center = sz / 2;

    float sum = 0;
    for( int i=0; i <sz; i++ )
    {
        float x  = float(i - center);
        float fx = pow(Eu_Number, -0.5*x*x/(sigma*sigma)) / (sigma * sqrt(2*M_PI));
        kernel[i] = fx;
        sum += fx;
    }

    //std::cout << "gauss sz="<< sz << " center=" << center << std::endl;

    for( int i=0; i < sz; i++)
    {
        kernel[i] /= sum;
        //std::cout << i-center << " " <<  kernel[i] << std::endl;
    }
    return kernel;
}

void get_edge_points_smoothed_1D(std::vector<CIPixel> & inpnts)
{

    int ksz;
    float * kernel = gauss_kernel(ksz, CGlobalSettings::_gauss_sigma);
    int center = ksz / 2;
    std::vector<CIPixel> outpnts;


    //std::cout << "start smooth " << std::endl;
    //std::cout << kernel[0] << " " << kernel[1] << std::endl;

    std::vector<CIPixel>::iterator it = inpnts.begin();
    int pnts_count = inpnts.size();
    for(int i = 0 ; it !=  inpnts.end(); it++, i++)
    {

        CIPixel cpix = *it;

        if( (i >= center) && (i < (pnts_count - center)) )
        {
            float dot = 0.0;
            float sum = 0.0;
            for(int cc=(-center); cc<=center; cc++)
            {
                if(((i+cc) >= 0) && ((i+cc) < pnts_count))
                {
                    dot += kernel[center+cc] * inpnts[i+cc]._u ;
                    sum += kernel[center+cc];
                }
            }
            cpix._u = dot/sum;
        }

        outpnts.push_back(cpix);
    }

    inpnts = outpnts;
    delete kernel;

}

std::vector<CIPixel> get_edge_points_lines_hills_1D(QImage * img)
{

    std::vector<CIPixel> res;

    int xs = img->width();
    int ys = img->height();

    CArray2D<double> nimg = convert_RGB_to_gray(img);

    double sum_weights = 0;
    double sum_pos = 0;


    //#pragma omp parallel for
    for(int y = 0 ; y < ys; y++)
    {

        bool emax_found = false;
        float emax_x = 0;
        float emax_y = y;
        float emax_c = 0;

        for(int x = 0; x < xs; x++)
        {

            float curr_col = nimg._data[y][x];
            if ( curr_col > CGlobalSettings::_lcthresh && curr_col > emax_c)
            {
                emax_x = x;
                emax_c = curr_col;
                emax_found = true;
            }
        }
        if(emax_found)
        {
            float sum_weights = 0.0;
            float sum_pos = 0.0;
            int i = emax_x;
            while(nimg._data[y][i]>= CGlobalSettings::_lcthresh) // go left
            {
                sum_weights += float(nimg._data[y][i]);
                sum_pos   += float(i) * float(nimg._data[y][i]);
                i--;
                if(i < 0 ) break;
            }

            i = emax_x+1;
            while(nimg._data[y][i]>= CGlobalSettings::_lcthresh) // go right
            {
                sum_weights += float(nimg._data[y][i]);
                sum_pos   += float(i) * float(nimg._data[y][i]);
                i++;
                if(i >= xs) break;
            }

            // FIX: donot add points from right image side
            //if(sum_pos/sum_weights < xs/2)
            //#pragma omp critical
            res.push_back(CIPixel(sum_pos/sum_weights, float(y)));
        }
    }

    if(CGlobalSettings::_gauss_sigma > 0)
        get_edge_points_smoothed_1D(res);
    return res;
}

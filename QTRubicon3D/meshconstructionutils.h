#ifndef MESHCONSTRUCTIONUTILS_H
#define MESHCONSTRUCTIONUTILS_H

#include "GraphicUtils.h"
#include "./normals/box3.h"
#include "./normals/normal_extrapolation.h"
using namespace vcg;

#include "./normals/vertex.h"

class CNormalEstimationThread;
std::vector<C3DPoint> compute_normals(std::vector<C3DPoint> & points, int neighbors, bool flip, CNormalEstimationThread * cthread);

#endif // MESHCONSTRUCTIONUTILS_H

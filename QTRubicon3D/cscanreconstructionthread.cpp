#include "cscanreconstructionthread.h"

#include "./settings/psettings.h"
#include "./img2space/edgedetection.h"
#include "./img2space/image2spacemodel.h"
#include <iostream>
#include <ctime>
#include <omp.h>

#include "./img2space/edgedetection.h"
#include "./img2space/image2spacemodel.h"
#include "./png/lodepng.h"
#include <fstream>

#include <QTime>
#include <QMessageBox>
#include "messages.h"
//#define __WINDOWS_TYPES__
extern "C"
{
#include "driver_aux_utils.h"
}

extern FT_HANDLE glbFtHandle;

CScanReconstructionThread::CScanReconstructionThread(QObject *parent) :
    QThread(parent), _cur_step(STEP_MOTOR_0)
{
    _abort = false;
    _use_left  = false;
    _use_right = false;
    _mpmax = 200;
    _mstep = 20;

}

CScanReconstructionThread::~CScanReconstructionThread()
{
    _mutex.lock();
    _abort = true;
    _mutex.unlock();
    wait();
}

void CScanReconstructionThread::start_process(bool useL, bool useR, int mmax, int mstep,
                                              int paus, bool scan_tex)
{
    _abort = false;
    _use_left = useL;
    _use_right = useR;
    _mpmax = mmax;
    _mstep = mstep;
    _pause = paus;
    _scan_tex = scan_tex;

    start();
}


void CScanReconstructionThread::stop_process()
{
    _mutex.lock();
    _abort = true;
    _mutex.unlock();
}

void CScanReconstructionThread::save_image(QImage * img, QString fname)
{
    // QString fileName ("./from_dev.png");

    //generate some image
    unsigned width  = img->width();
    unsigned height = img->height();

    std::vector<unsigned char> saving_image;
    saving_image.resize(width * height * 4);

    for(unsigned y = 0; y < height; y++)
    {
        for(unsigned x = 0; x < width; x++)
        {
            QRgb value = img->pixel(x,y);

            saving_image[4 * width * y + 4 * x + 0] = qRed(value);
            saving_image[4 * width * y + 4 * x + 1] = qGreen(value);
            saving_image[4 * width * y + 4 * x + 2] = qBlue(value);
            saving_image[4 * width * y + 4 * x + 3] = 255;
        }
    }

    unsigned error = lodepng::encode(fname.toStdString(), saving_image, width, height);
   // if(error)
     //   QMessageBox::information(this, tr("Warning"), err_cannotSaveFile );
}

QImage * CScanReconstructionThread::snap_raw_img_from_device(unsigned char * buffer)
{

    // set led and laser parameters


    // get camera image
    doSnapshot(buffer);

    // form resulting image
    int width  = getSnapshotWidth();
    int height = getSnapshotHeight();

    QImage * img = new QImage(height, width,  QImage::Format_RGB32);

    #pragma omp parallel for
    for (int x = 0; x < width-1; x += 2)
    {
      QRgb * lineX1   = (QRgb*)img->scanLine(width - x - 1);
      QRgb * lineX2   = (QRgb*)img->scanLine(width - x - 2);

      for (int y = 0; y < height-1; y += 2)
      {
        u16 g1 = getSnapshotPixelValue(buffer, x,     y,     width);
        u16 r  = getSnapshotPixelValue(buffer, x + 1, y,     width);
        u16 b  = getSnapshotPixelValue(buffer, x,     y + 1, width);
        u16 g2 = getSnapshotPixelValue(buffer, x + 1, y + 1, width);

        u16 g18 = g1 >> 4;
        u16 r8  = r  >> 4;
        u16 b8  = b  >> 4;
        u16 g28 = g2 >> 4;

        /* QRgb p00 = qRgb( 0, g18,  0);
        QRgb p10 = qRgb(r8,   0,  0);
        QRgb p01 = qRgb( 0,   0, b8);
        QRgb p11 = qRgb( 0, g28,  0);
        */
        // image mirroring / change y and y + 1? due to green?
        /*
        img->setPixel(height - y-1, width - x -1,        p00);
        img->setPixel(height - y-1,  width - ( x + 1) - 1,    p10);
        img->setPixel(height - (y + 1)-1, width - x -1,      p01);
        img->setPixel(height - (y + 1)-1, width - (x + 1) - 1,  p11);
        */

        /*img->setPixel( y + 1, width -   x - 1,      p00);
        img->setPixel( y + 1, width - ( x + 1) - 1, p10);
        img->setPixel(     y, width -   x -1,       p01);
        img->setPixel(     y, width - ( x + 1) - 1, p11);
            */
        lineX1  [y + 1] = qRgb( 0, g18,  0);
        lineX2  [y + 1] = qRgb(r8,   0,  0);
        lineX1  [y    ] = qRgb( 0,   0, b8);
        lineX2  [y    ] = qRgb( 0, g28,  0);
      }
    }
    return img;
}


void CScanReconstructionThread::snap_raw_img_from_device_fast(unsigned char * buffer, QImage *& img)
{

    // set led and laser parameters
    //clock_t sstart,  ffinish;

    //sstart = clock();
    // get camera image
    doSnapshot(buffer);
    //ffinish = clock();
    //snap1  += (ffinish - sstart);


    //sstart = clock();

    // form resulting image
    int width  = getSnapshotWidth();
    int height = getSnapshotHeight();

    if (img == NULL)
        img = new QImage(height, width,  QImage::Format_RGB32);

    #pragma omp parallel for
    for (int x = 0; x < width-1; x += 2)
    {
      QRgb * lineX1   = (QRgb*)img->scanLine(width - x - 1);
      QRgb * lineX2   = (QRgb*)img->scanLine(width - x - 2);

      for (int y = 0; y < height-1; y += 2)
      {
        u16 g1 = getSnapshotPixelValue(buffer, x,     y,     width);
        u16 r  = getSnapshotPixelValue(buffer, x + 1, y,     width);
        u16 b  = getSnapshotPixelValue(buffer, x,     y + 1, width);
        u16 g2 = getSnapshotPixelValue(buffer, x + 1, y + 1, width);

        u16 g18 = g1 >> 4;
        u16 r8  = r  >> 4;
        u16 b8  = b  >> 4;
        u16 g28 = g2 >> 4;


        lineX1  [y + 1] = qRgb( 0, g18,  0);
        lineX2  [y + 1] = qRgb(r8,   0,  0);
        lineX1  [y    ] = qRgb( 0,   0, b8);
        lineX2  [y    ] = qRgb( 0, g28,  0);
      }
    }

    //ffinish = clock();
    //snap2  += (ffinish - sstart);


}



void CScanReconstructionThread::release_motor()
{
    stepMotorSetValue(STEP_M1, 0);
    stepMotorSetValue(STEP_M2, 0);
    stepMotorSetValue(STEP_M3, 0);
    stepMotorSetValue(STEP_M4, 0);
}


QString convert_to_time(int usec)
{

    int secs = usec / 1000;
    int mins = (secs / 60) % 60;
    int hours = (secs / 3600);
    secs = secs % 60;
    return QString("%1:%2:%3").arg(hours, 2, 10, QLatin1Char('0'))
                                      .arg(mins, 2, 10, QLatin1Char('0'))
                                      .arg(secs, 2, 10, QLatin1Char('0'));
}

void CScanReconstructionThread::run()
{
   std::ofstream log_file;
   log_file.open ("./Rubicon3D_log.txt");

   u8 * _buffer = NULL;
   FT_STATUS ftStatus = connectToModule();
   if (ftStatus != FT_OK)
   {
     log_file << "Cannot detect scanning device. Abbort reconstruction." << std::endl;
     disconnectFromModule();
     emit send_finished();
     return;
   }
   else
   {
      int size = getSnapshotBufferSize() + 16;
      _buffer = (u8*)calloc(size, 1);
      initCameraRegs();
   }

   int first_idx = 1;
   float last = cPI2/ CGlobalSettings::_scan_step_angle + 0.1;
   int last_idx  = int(last);

   int each_tex = last_idx / 20;
   //  int last_idx  = first_idx+4;

   QTime time_elased;
   time_elased.start();

   pwmSetPrescaler(10);

   clock_t sstart,  ffinish;
   clock_t msstart,  mffinish;

   int sec_move = 0;
   int sec_bckg = 0;
   int sec_proc = 0;
   int sec_conv = 0;
   int sec_def1 = 0;
   int sec_def2 = 0;
   int sec_bckg_snap = 0;
   int sec_bckg_proc = 0;

   QImage * raw_img_bckg = NULL;
   QImage * raw_img_lasr = NULL;
   QImage * raw_img_lasr_right = NULL;
   QImage * proc_img_bckg = NULL;
   QImage * proc_img_lasr = NULL;
   QImage * proc_img_lasr_right = NULL;
   QImage * defish_aux = NULL;
   QImage * defish_bckg = NULL;
   QImage * defish_diff = NULL;
   QImage * defish_diff_right = NULL;
   int * defish_aux_map = NULL;

    for(int i = first_idx; i <= last_idx; i++ )
    {

        log_file << std::endl;
        log_file << "Begin new scan cycle: " << i << std::endl;

        log_file << "Setting exposure: " << CCCDCamera::_exposure << std::endl;
        writeCameraReg(CCCDCamera::regExposure, CCCDCamera::_exposure);


        /////////////////////////////////////////////////////////////
        sstart = clock();
        log_file << "turn on both lasers" << std::endl;
        pwmSetBrightness(PWM_LASER1_BRIGHTNESS,  100);
        pwmSetBrightness(PWM_LASER2_BRIGHTNESS,  100);
        log_file << "move motor" << std::endl;
        int msteps = 0.5+(CGlobalSettings::_scan_step_angle/BGM(0.45));
        for (int s = 0 ; s < msteps; s++)
            make_motor_step_8_fast(_mpmax, _mstep);

        ///msleep(_pause);
        log_file << "turn off both lasers" << std::endl;
        pwmSetBrightness(PWM_LASER1_BRIGHTNESS,  0);
        pwmSetBrightness(PWM_LASER2_BRIGHTNESS,  0);
        ffinish = clock();
        sec_move += (ffinish - sstart);

        /////////////////////////////////////////////////////////////
        sstart = clock();
        log_file << "get bckg image" << std::endl;
        ///QImage * raw_img_bckg = new QImage("./00_raw_backg.png");
        snap_raw_img_from_device_fast(_buffer, raw_img_bckg);
        //save_image(raw_img_bckg, "./00_raw_backg.png");
        ffinish = clock();
        sec_bckg_snap  += (ffinish - sstart);

        sstart = clock();
        log_file << "compute processed bckg image" << std::endl;
        CCCDCamera::processed_image_fast(raw_img_bckg, proc_img_bckg);
        //save_image(proc_img_bckg, "./01_backg.png");
        /// std::cout << "bckg is processed" << std::endl;
        ffinish = clock();
        sec_bckg_proc += (ffinish - sstart);

        /////////////////////////////////////////////////////////////

        sstart = clock();

        if(_use_left)
        {
            log_file << "snap image with left laser" << std::endl;
            pwmSetBrightness(PWM_LASER1_BRIGHTNESS,  100);
            ///raw_img_lasr = new QImage("./00_raw_img_left.png");
            snap_raw_img_from_device_fast(_buffer, raw_img_lasr);
            // save_image(raw_img_lasr, "./00_lef.png");
            pwmSetBrightness(PWM_LASER1_BRIGHTNESS,  0);
            //std::cout << "load left" << std::endl;

        }

        if(_use_right)
        {
            log_file << "snap image with right laser" << std::endl;
            pwmSetBrightness(PWM_LASER2_BRIGHTNESS,  100);
            ///raw_img_lasr_right = new QImage("./00_raw_img_left.png");
            snap_raw_img_from_device_fast(_buffer, raw_img_lasr_right);
            //save_image(raw_img_lasr, "./00_raw_img_lasr.png");
            pwmSetBrightness(PWM_LASER2_BRIGHTNESS,  0);
            //std::cout << "load reft" << std::endl;
        }

        if(_use_left)
        {
            log_file << "process left laser image" << std::endl;
            CCCDCamera::processed_image_fast(raw_img_lasr, proc_img_lasr);
            //save_image(proc_img_lasr, "./_left_laser_proc.png");
        }

        if(_use_right)
        {
            log_file << "process right laser image" << std::endl;
            CCCDCamera::processed_image_fast(raw_img_lasr_right, proc_img_lasr_right);
            // save_image(proc_img_lasr_right, "./_right_laser_proc.png");
        }

        CCCDCamera::defisheye_image_fast(proc_img_bckg, defish_aux, defish_bckg, defish_aux_map);
        // defisheye_image_fast(QImage * in, QImage *& res_img, QImage *& res2_img)

        /*
        save_image(defish_bckg, "./_defish_left_laser.png");
        QImage * defish_bckg_r = CCCDCamera::defisheye_image(proc_img_lasr_right);
        save_image(defish_bckg_r, "./_defish_right_laser.png");
        */

        log_file << "compute differences" << std::endl;
        if(_use_left)
            CCCDCamera::difference_image_fast(proc_img_lasr, proc_img_bckg);
            //proc_img_lasr = new QImage("_diff_LEFT.png");

        if(_use_right)
            CCCDCamera::difference_image_fast(proc_img_lasr_right, proc_img_bckg);
            //proc_img_lasr_right = new QImage("_diff_RIGHT.png");

        //QString diff_fname;
        //diff_fname.sprintf("dev_diff_%.4d.png", i);
        //save_image(proc_img_lasr, diff_fname);

        log_file << "compute defish images" << std::endl;
        msstart = clock();

        if(_use_left)
            CCCDCamera::defisheye_image_fast(proc_img_lasr, defish_aux, defish_diff, defish_aux_map);

        if(_use_right)
            CCCDCamera::defisheye_image_fast(proc_img_lasr_right, defish_aux, defish_diff_right, defish_aux_map);
        // save_image(defish_diff, "./04_defish_diff.png");
        mffinish = clock();
        sec_conv += (mffinish - msstart);

        //////// left laser
        //// read somehow a difference image
        //QString fname = "./diff.png";
        //fname.sprintf("D:\\_Running_Projects\\_Elance_ScanRecoSoft\\TestImages\\ma\\left\\ma_%.4d.png", i);
        //QImage diff_img(fname);

        if(_use_left)
        {
            log_file << "compute edges (left)" << std::endl;

            std::vector<CIPixel> pix = get_edge_points_lines_hills_1D(defish_diff);

            log_file << "compute 3d points (left)" << std::endl;

            // magic constant is -15
            float _magic_constant_dy = -15;
            std::vector<C3DPoint> cpoints = image2space_conv(pix, CGlobalSettings::_d,
            CGlobalSettings::_h, CGlobalSettings::_defish_alpha_x, CGlobalSettings::_defish_alpha_z,
            CGlobalSettings::_delta_x, _magic_constant_dy, /*-CCCDCamera::_defish_deltay/2*/
            CGlobalSettings::_betta, CGlobalSettings::_scan_step_angle,
            defish_diff->width(), defish_diff->height(), i-first_idx, false,
            CGlobalSettings::_maximal_radius_pnts, defish_bckg, _scan_tex);

            log_file << "send result (left)" << std::endl;
            std::vector<C3DPoint> * points_left = new std::vector<C3DPoint> ;
            points_left->insert(points_left->end(), cpoints.begin(), cpoints.end());
            emit send_result_left(points_left);
        }

        if(_use_right)
        {
            log_file << "compute edges (right)" << std::endl;

            std::vector<CIPixel> pix_r = get_edge_points_lines_hills_1D(defish_diff_right);

            log_file << "compute 3d points (right)" << std::endl;

            // magic constant is -15
            float _magic_constant_dy = -15;
            std::vector<C3DPoint> cpoints_r = image2space_conv(pix_r, CGlobalSettings::_d,
            CGlobalSettings::_h, CGlobalSettings::_defish_alpha_x, CGlobalSettings::_defish_alpha_z,
            CGlobalSettings::_delta_x, _magic_constant_dy, /*-CCCDCamera::_defish_deltay/2*/
            CGlobalSettings::_betta, CGlobalSettings::_scan_step_angle,
            defish_diff_right->width(), defish_diff_right->height(), i-first_idx, true,
            CGlobalSettings::_maximal_radius_pnts, defish_bckg, _scan_tex);


           log_file << "send result (right)" << std::endl;
            std::vector<C3DPoint> * points_right = new std::vector<C3DPoint> ;
            points_right->insert(points_right->end(), cpoints_r.begin(), cpoints_r.end());
            emit send_result_right(points_right);
        }

        ffinish = clock();
        sec_proc += (ffinish - sstart);

        /*if(_scan_tex && !((i-1)%each_tex))
        {

            log_file << "Setting exposure: " << CCCDCamera::_exposure_tex << std::endl;
            writeCameraReg(CCCDCamera::regExposure, CCCDCamera::_exposure_tex);

            log_file << "capturing texture" << std::endl;
            pwmSetBrightness(PWM_LED1_BRIGHTNESS,  CGlobalSettings::_left_led_bright);
            pwmSetBrightness(PWM_LED2_BRIGHTNESS,  CGlobalSettings::_right_led_bright);
            QImage * raw_tex = snap_raw_img_from_device(_buffer);
            ///QImage * raw_tex  = new QImage("./00_raw_backg.png");
            pwmSetBrightness(PWM_LED1_BRIGHTNESS,  0);
            pwmSetBrightness(PWM_LED2_BRIGHTNESS,  0);

            QImage * proc_tex  = CCCDCamera::processed_image(raw_tex);
            QString tex_name = QString("tex_") + QString::number(i*msteps*0.45)+ QString(".png");
            // proc_tex->save(tex_name);
            // delete proc_tex;
            delete raw_tex;

            log_file << "sending texture" << std::endl;

            if(_use_right && _use_left)
            {

                CRawTexture * tex = new CRawTexture;
                tex->_img = proc_tex;
                tex->_name = tex_name;
                send_tex_left (tex);

                CRawTexture * texr = new CRawTexture;
                texr->_img = new QImage(*proc_tex);
                texr->_name = tex_name;
                send_tex_right(texr);
            }
            else
            {
                CRawTexture * tex = new CRawTexture;
                tex->_img = proc_tex;
                tex->_name = tex_name;
                if(_use_left)  send_tex_left (tex);
                if(_use_right) send_tex_right(tex);
            }

        }*/

        log_file << "check abort button" << std::endl;

        log_file << "time measurements:" << "table move: " << sec_move <<
                    " clcs, read from dev/converse bckg image: " << sec_bckg_snap
                 << "/" << sec_bckg_proc << " clcs, " <<  " proc: "<< sec_proc << " clcs, where defish left: " <<
                    sec_conv  << std::endl;

        if (_abort)
        {
            log_file << "aborting " << std::endl;

            //////// ADD CLEANING IMAGES HERE
            if(_buffer != NULL)
                free(_buffer);
            if(raw_img_bckg != NULL) delete raw_img_bckg;
            if(raw_img_lasr != NULL) delete raw_img_lasr;
            if(raw_img_lasr_right   != NULL) delete raw_img_lasr_right;
            if(proc_img_bckg        != NULL) delete proc_img_bckg;
            if(proc_img_lasr        != NULL) delete proc_img_lasr;
            if(proc_img_lasr_right  != NULL) delete proc_img_lasr_right;
            if(defish_diff          != NULL) delete defish_diff;
            if(defish_diff_right    != NULL) delete defish_diff_right;
            if(defish_bckg          != NULL) delete defish_bckg;
            if(defish_aux           != NULL) delete defish_aux;
            if(defish_aux_map       != NULL) delete [] defish_aux_map;

            release_motor();
            disconnectFromModule();

            emit send_back(0);
            emit send_time_elapsed("00:00:00");
            emit send_time_ETA("00:00:00");
           // emit send_clean_result();
             emit send_finished();
            return;
        }
        else
        {
            log_file << "send progress and ETA" << std::endl;
            emit send_back(i * 100 / (last_idx-first_idx+1));
            int usec_elap = time_elased.elapsed();
            emit send_time_elapsed(convert_to_time(usec_elap));
            int usec_ETA = (last_idx - i) * usec_elap / i;
            emit send_time_ETA(convert_to_time(usec_ETA));
        }
    }

    if(raw_img_bckg != NULL) delete raw_img_bckg;
    if(raw_img_lasr != NULL) delete raw_img_lasr;
    if(raw_img_lasr_right   != NULL) delete raw_img_lasr_right;
    if(proc_img_bckg        != NULL) delete proc_img_bckg;
    if(proc_img_lasr        != NULL) delete proc_img_lasr;
    if(proc_img_lasr_right  != NULL) delete proc_img_lasr_right;
    if(defish_diff          != NULL) delete defish_diff;
    if(defish_diff_right    != NULL) delete defish_diff_right;
    if(defish_bckg          != NULL) delete defish_bckg;
    if(defish_aux           != NULL) delete defish_aux;
    if(defish_aux_map       != NULL) delete [] defish_aux_map;


    log_file << "done with scanning " << std::endl;

    release_motor();

    if(_buffer != NULL)
        free(_buffer);
    disconnectFromModule();

    emit send_back(100);
    emit send_finished();
}



void CScanReconstructionThread::make_motor_step_8()
{
    int dInt = 20;
    int vMax = 200;
    int fi_channel = STEP_M1;
    int se_channel = STEP_M2;
    int th_channel = STEP_M3;
    int fo_channel = STEP_M4;
    stepMotorSetPrescaler(10);

    switch(_cur_step)
    {
        case STEP_MOTOR_0:
        for ( int i = 0; i<=vMax; i +=dInt )
        {
            stepMotorSetValue(fi_channel, 0);
            stepMotorSetValue(se_channel, vMax);
            stepMotorSetValue(th_channel, vMax);
            stepMotorSetValue(fo_channel, i);
       }
        break;

        case STEP_MOTOR_1:
        for ( int i = 0; i<=vMax; i=i+dInt )
        {

            stepMotorSetValue(fi_channel, 0);
            stepMotorSetValue(se_channel, vMax);
            stepMotorSetValue(th_channel, vMax-i);
            stepMotorSetValue(fo_channel, vMax);
      }
        break;

        case STEP_MOTOR_2:
        for ( int i = 0; i<=vMax; i=i+dInt )
        {
            stepMotorSetValue(fi_channel, i);
            stepMotorSetValue(se_channel, vMax);
            stepMotorSetValue(th_channel, 0);
            stepMotorSetValue(fo_channel, vMax);
        }
        break;

        case STEP_MOTOR_3:
        for ( int i = 0; i<=vMax; i=i+dInt )
        {
            stepMotorSetValue(fi_channel, vMax);
            stepMotorSetValue(se_channel, vMax-i);
            stepMotorSetValue(th_channel, 0);
            stepMotorSetValue(fo_channel, vMax);
        }
        break;

        case STEP_MOTOR_4:
        for ( int i = 0; i<=vMax; i=i+dInt )
        {
            stepMotorSetValue(fi_channel, vMax);
            stepMotorSetValue(se_channel, 0);
            stepMotorSetValue(th_channel, i);
            stepMotorSetValue(fo_channel, vMax);
        }
        break;

        case STEP_MOTOR_5:
        for ( int i = 0; i<=vMax; i=i+dInt )
        {
            stepMotorSetValue(fi_channel, vMax);
            stepMotorSetValue(se_channel, 0);
            stepMotorSetValue(th_channel, vMax);
            stepMotorSetValue(fo_channel, vMax-i);
        }
        break;

        case STEP_MOTOR_6:
        for ( int i = 0; i<=vMax; i=i+dInt )
        {
            stepMotorSetValue(fi_channel, vMax);
            stepMotorSetValue(se_channel, i);
            stepMotorSetValue(th_channel, vMax);
            stepMotorSetValue(fo_channel, 0);
        }
        break;

        case STEP_MOTOR_7:
        for ( int i = 0; i<=vMax; i=i+dInt )
        {
            stepMotorSetValue(fi_channel, vMax-i);
            stepMotorSetValue(se_channel, vMax);
            stepMotorSetValue(th_channel, vMax);
            stepMotorSetValue(fo_channel, 0);
        }
        break;

    }
    _cur_step = (_cur_step + 1)%8;
}


void CScanReconstructionThread::make_motor_step_8_fast(int mmax, int mstep)
{
    int dInt = mmax/mstep;
    int vMax = mmax;
    int fi_channel = STEP_M1;
    int se_channel = STEP_M2;
    int th_channel = STEP_M3;
    int fo_channel = STEP_M4;
    stepMotorSetPrescaler(10);

    switch(_cur_step)
    {
        case STEP_MOTOR_0:
        for ( int i = 0; i<=vMax; i +=dInt )
        {
            writeMemory(glbFtHandle, STEP_BASE + fi_channel, 0);
            writeMemory(glbFtHandle, STEP_BASE + se_channel, vMax);
            writeMemory(glbFtHandle, STEP_BASE + th_channel, vMax);
            writeMemory(glbFtHandle, STEP_BASE + fo_channel, i);
       }
        break;

        case STEP_MOTOR_1:
        for ( int i = 0; i<=vMax; i=i+dInt )
        {

            writeMemory(glbFtHandle, STEP_BASE + fi_channel, 0);
            writeMemory(glbFtHandle, STEP_BASE + se_channel, vMax);
            writeMemory(glbFtHandle, STEP_BASE + th_channel, vMax-i);
            writeMemory(glbFtHandle, STEP_BASE + fo_channel, vMax);
      }
        break;

        case STEP_MOTOR_2:
        for ( int i = 0; i<=vMax; i=i+dInt )
        {
            writeMemory(glbFtHandle, STEP_BASE + fi_channel, i);
            writeMemory(glbFtHandle, STEP_BASE + se_channel, vMax);
            writeMemory(glbFtHandle, STEP_BASE + th_channel, 0);
            writeMemory(glbFtHandle, STEP_BASE + fo_channel, vMax);
        }
        break;

        case STEP_MOTOR_3:
        for ( int i = 0; i<=vMax; i=i+dInt )
        {
            writeMemory(glbFtHandle, STEP_BASE + fi_channel, vMax);
            writeMemory(glbFtHandle, STEP_BASE + se_channel, vMax-i);
            writeMemory(glbFtHandle, STEP_BASE + th_channel, 0);
            writeMemory(glbFtHandle, STEP_BASE + fo_channel, vMax);
        }
        break;

        case STEP_MOTOR_4:
        for ( int i = 0; i<=vMax; i=i+dInt )
        {
            writeMemory(glbFtHandle, STEP_BASE + fi_channel, vMax);
            writeMemory(glbFtHandle, STEP_BASE + se_channel, 0);
            writeMemory(glbFtHandle, STEP_BASE + th_channel, i);
            writeMemory(glbFtHandle, STEP_BASE + fo_channel, vMax);
        }
        break;

        case STEP_MOTOR_5:
        for ( int i = 0; i<=vMax; i=i+dInt )
        {
            writeMemory(glbFtHandle, STEP_BASE + fi_channel, vMax);
            writeMemory(glbFtHandle, STEP_BASE + se_channel, 0);
            writeMemory(glbFtHandle, STEP_BASE + th_channel, vMax);
            writeMemory(glbFtHandle, STEP_BASE + fo_channel, vMax-i);
        }
        break;

        case STEP_MOTOR_6:
        for ( int i = 0; i<=vMax; i=i+dInt )
        {
            writeMemory(glbFtHandle, STEP_BASE + fi_channel, vMax);
            writeMemory(glbFtHandle, STEP_BASE + se_channel, i);
            writeMemory(glbFtHandle, STEP_BASE + th_channel, vMax);
            writeMemory(glbFtHandle, STEP_BASE + fo_channel, 0);
        }
        break;

        case STEP_MOTOR_7:
        for ( int i = 0; i<=vMax; i=i+dInt )
        {
            writeMemory(glbFtHandle, STEP_BASE + fi_channel, vMax-i);
            writeMemory(glbFtHandle, STEP_BASE + se_channel, vMax);
            writeMemory(glbFtHandle, STEP_BASE + th_channel, vMax);
            writeMemory(glbFtHandle, STEP_BASE + fo_channel, 0);
        }
        break;

    }
    _cur_step = (_cur_step + 1)%8;
}

void CScanReconstructionThread::make_motor_step()
{
    int dInt = 20;
    int vMax = 200;
    int fi_channel = STEP_M1;
    int se_channel = STEP_M2;
    int th_channel = STEP_M3;
    int fo_channel = STEP_M4;
    stepMotorSetPrescaler(10);

    switch(_cur_step)
    {
        case STEP_MOTOR_0:
        for ( int i = 0; i<=vMax; i +=dInt )
        {
            stepMotorSetValue(fi_channel, vMax-i);
            stepMotorSetValue(se_channel, 0);
            stepMotorSetValue(th_channel, i);
            stepMotorSetValue(fo_channel, 0);
       }
        break;

        case STEP_MOTOR_1:
        for ( int i = 0; i<=vMax; i=i+dInt )
        {

            stepMotorSetValue(fi_channel, 0);
            stepMotorSetValue(se_channel, i);
            stepMotorSetValue(th_channel, vMax-i);
            stepMotorSetValue(fo_channel, 0);

            //mwait(delayMin);
        }
        break;

        case STEP_MOTOR_2:
        for ( int i = 0; i<=vMax; i=i+dInt )
        {
            stepMotorSetValue(fi_channel, 0);
            stepMotorSetValue(se_channel, vMax-i);
            stepMotorSetValue(th_channel, 0);
            stepMotorSetValue(fo_channel, i);
        }
        break;

        case STEP_MOTOR_3:
        for ( int i = 0; i<=vMax; i=i+dInt )
        {
            stepMotorSetValue(fi_channel, i);
            stepMotorSetValue(se_channel, 0);
            stepMotorSetValue(th_channel, 0);
            stepMotorSetValue(fo_channel, vMax-i);
        }
        break;
    }
    _cur_step = (_cur_step + 1)%4;
}

#ifndef CMESHCONSTRUCTIONDLG_H
#define CMESHCONSTRUCTIONDLG_H

#include <QDialog>

class CGLWidget;
class CTriangularMesh;
class CMeshConstructionThread ;
namespace Ui {
class CMeshConstructionDlg;
}

class CMeshConstructionDlg : public QDialog
{
    Q_OBJECT

public:
    explicit CMeshConstructionDlg(CGLWidget * glWidget, QWidget *parent = 0);
    ~CMeshConstructionDlg();

protected:
    void showEvent(QShowEvent * event);

    void init_meshlist();

private:
    Ui::CMeshConstructionDlg *ui;

    int	_octree_d;
    int _octree_d_min;
    int _octree_d_max;

    int _solvdiv;
    int _solvdiv_min;
    int _solvdiv_max;

    float _sampl_per_node;
    float _sampl_per_node_min;
    float _sampl_per_node_max;

    float _surf_offset;
    float _surf_offset_min;
    float _surf_offset_max;

    CGLWidget *_glWidget;

    bool _in_process;
    bool _on_apply;
    bool _is_result_valid;
    CMeshConstructionThread * _thread;
    bool _expanded;

private slots:
    void onCancelBtn();
    void onPreviewBtn();
    void onApplyBtn();

    void onMakeProgressStep(int val);
    void onCompleteProcessing(CTriangularMesh * );
    void onStopProcessing();

    ///void expand_dlg();

    void n_octree_d(QString );
    void n_solvdiv(QString);
    void n_sampl_per_node(QString);
    void n_surf_offset(QString);

    void close_dialog();

    void checkedOneMesh();
    void checkedOtherMesh();
    void addSelMesh();
    void delSelMesh();
};

#endif // CMESHCONSTRUCTIONDLG_H

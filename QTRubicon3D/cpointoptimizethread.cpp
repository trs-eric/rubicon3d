#include "cpointoptimizethread.h"
#include "cpointcloud.h"


CPointOptimizeThread::CPointOptimizeThread(QObject *parent) :
    QThread(parent), _in_cloud(NULL)
{
    _abort = false;
}

CPointOptimizeThread::~CPointOptimizeThread()
{
    _mutex.lock();
    _abort = true;
    _mutex.unlock();
    wait();
}

void CPointOptimizeThread::start_process(CPointCloud * in, float thres)
{
    _abort = false;
    _in_cloud = in;
    _dist_thres = thres;

    start();
}

void CPointOptimizeThread::stop_process()
{
    _mutex.lock();
    _abort = true;
    _mutex.unlock();
}

void CPointOptimizeThread::run()
{
    std::vector<C3DPoint> final_res;

    float dist_thres_sq =  _dist_thres * _dist_thres;
    std::vector<C3DPoint> orig_points =  _in_cloud->get_points_cpy();

    int sz  = orig_points.size();
    bool * visit = new bool[sz]();
    for (int k =0; k<sz; k++) visit[k] = false;

    std::vector<C3DPoint>::iterator it = orig_points.begin();
    for( int i = 0 ; it != orig_points.end(); it++, i++)
    {
        // skip considered point
        if(visit[i]) continue;

        std::vector<C3DPoint> small_point_pull;
        small_point_pull.push_back(*it);

        std::vector<C3DPoint>::iterator itn = it+1;
        for( int j = i+1  ; itn != orig_points.end(); itn++, j++)
        {

            if(!visit[j])
            {
                if(     ((it->_x - itn->_x)*(it->_x - itn->_x) +
                         (it->_y - itn->_y)*(it->_y - itn->_y) +
                         (it->_z - itn->_z)*(it->_z - itn->_z)) < dist_thres_sq)
                {
                    small_point_pull.push_back(*itn);
                    visit[j] = true;
                }

            }
        }

        if(small_point_pull.size() == 1)
            final_res.push_back(*it);
        else
            final_res.push_back(compute_avg_pnt(small_point_pull));

        visit[i] = true;

        //setup progress value
        emit send_back(i * 100 / (sz-1));

        if (_abort)
        {
            emit send_back(0);
            return;
        }
    }
    delete [] visit;
//	return final_res;
    CPointCloud * res_cld = new CPointCloud;
    res_cld->set_points(final_res);
    emit send_result(res_cld);
}

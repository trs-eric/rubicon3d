#include "cclusteringdecimdlg.h"
#include "ui_cclusteringdecimdlg.h"

#include "cclusteringdecimthread.h"

#include "cglwidget.h"
#include "messages.h"

#include <QMessageBox>

// do not forgwt to preserve mesh - Apply, Preview, Cancel

const std::string mesh_dump_str("./mesh.dump");

CClusteringDecimDlg::CClusteringDecimDlg(CGLWidget * glWidget, QWidget *parent) :
   _glWidget(glWidget), QDialog(parent),
    ui(new Ui::CClusteringDecimDlg)
{
    setWindowFlags(Qt::WindowStaysOnTopHint);
    ui->setupUi(this);

    ui->progressBar->setMaximum(100);

    ui->cellSizeEdt->setText(QString::number(0.1));


    connect(ui->previewBtn, SIGNAL(clicked()), this, SLOT(onPreviewBtn()));
    connect(ui->applyBtn,   SIGNAL(clicked()), this, SLOT(onApplyBtn()));

    _thread = new CClusteringDecimThread;

    // user pressed cancel button
    connect(ui->cancelBtn,  SIGNAL(clicked()), _thread, SLOT(stop_process()));
    connect(ui->cancelBtn,  SIGNAL(clicked()), this, SLOT(onCancelBtn()));

    // user closed dialg
    connect(this,  SIGNAL(finished(int)), _thread, SLOT(stop_process()));

    // working with a thread
    connect(_thread, SIGNAL(send_back(int)), this, SLOT(onMakeProgressStep(int)));
    connect(_thread, SIGNAL(send_result(CTriangularMesh*)), this, SLOT(onCompleteProcessing(CTriangularMesh*)));

    // add this later
    connect(_thread, SIGNAL(send_result(CNormalCloud*)), _glWidget, SLOT(on_normal_pnts_computed(CNormalCloud*)));
    connect(_thread, SIGNAL(send_result(CTriangularMesh*)), _glWidget, SLOT(on_mesh_computed(CTriangularMesh*)));

    connect(_thread, SIGNAL(finished()), this, SLOT(onStopProcessing()));

}

void CClusteringDecimDlg::onMakeProgressStep(int val)
{
    ui->progressBar->setValue(val);
}

void CClusteringDecimDlg::onCompleteProcessing(CTriangularMesh * m)
{
    if(m == NULL)
    {
        QMessageBox::warning(this, tr("Warning"),wrn_FailedMesh);
        return;
    }
    if(_on_apply)
    {
        close_dialog();
        return;
    }

    _is_result_valid = true;
    ui->progressBar->setValue(100);

}

void CClusteringDecimDlg::onStopProcessing()
{
    ui->applyBtn->setEnabled(true);
    ui->previewBtn->setEnabled(true);
    ui->cellSizeEdt->setEnabled(true);

    _in_process = false;
}

void CClusteringDecimDlg::close_dialog()
{
    _on_apply = false;
    close();
}

CTriangularMesh * CClusteringDecimDlg::restore_mesh()
{
    _glWidget->discard_mesh_of_current_object();
    std::ifstream ifile;
    ifile.open(mesh_dump_str.c_str(), std::ios::in | std::ios::binary);
    CTriangularMesh * mesh = new CTriangularMesh;
    mesh->load(ifile);
    ifile.close();
    _glWidget->on_mesh_computed(mesh);

    return mesh;
}

void CClusteringDecimDlg::onCancelBtn()
{
    if(!_in_process)
    {
        if(_is_result_valid)
            restore_mesh();

        /*if(_inmesh)
        {
            CNormalCloud * ncld = new CNormalCloud ;
            ncld->set_normals(_inmesh->get_tri_mesh()->_points);
            _glWidget->on_normal_pnts_computed(ncld);
            _glWidget->on_mesh_computed(_inmesh);



        }*/

        close_dialog();
    }

    _in_process = false;
    _on_apply = false;

}

void CClusteringDecimDlg::onPreviewBtn()
{
    ui->progressBar->setValue(0);

    ui->applyBtn    ->setEnabled(false);
    ui->previewBtn  ->setEnabled(false);
    ui->cellSizeEdt ->setEnabled(false);

    _in_process = true;


    if(_is_result_valid)
    {
        CTriangularMesh * mesh = restore_mesh();
        _thread->start_process(mesh, ui->cellSizeEdt->text().toFloat() );
    }
    else
        _thread->start_process(_glWidget->get_mesh_of_current_object(),
                               ui->cellSizeEdt->text().toFloat() );

}

void CClusteringDecimDlg::onApplyBtn()
{
    _on_apply = true;
    if(!_is_result_valid)
        onPreviewBtn();
    else
        close_dialog();
}

void CClusteringDecimDlg::showEvent(QShowEvent * event)
{
    ui->progressBar->setValue(0);
    _is_result_valid    = false;
    _on_apply           = false;
    _in_process         = false;

    /*if(_glWidget->get_mesh_of_current_object() == NULL)
    {
        _inmesh = NULL;
        return;
    }*/

    CTriangularMesh * cmesh = _glWidget->get_mesh_of_current_object();
    std::ofstream dfile;
    dfile.open(mesh_dump_str.c_str(), std::ios::out | std::ios::binary);
    cmesh->save(dfile);
    dfile.close();


}

CClusteringDecimDlg::~CClusteringDecimDlg()
{
    delete ui;
    delete _thread;
}

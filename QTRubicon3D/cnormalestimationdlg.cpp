#include "cnormalestimationdlg.h"
#include "ui_cnormalestimationdlg.h"

#include "cglwidget.h"
#include "cnormalestimationthread.h"
#include "cnormalcloud.h"
#include "messages.h"

#include<QMessageBox>
#include <fstream>

CNormalEstimationDlg::CNormalEstimationDlg(CGLWidget * glWidget, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::CNormalEstimationDlg), _glWidget(glWidget), _ncount(10), _nflip(false),
    _on_apply(false), _in_process(false), _thread(NULL), _is_result_valid(false)
{
    setWindowFlags(Qt::WindowStaysOnTopHint);
    ui->setupUi(this);

    ui->progressBar->setMaximum(100);
    ui->ncount->setText(QString::number(_ncount));
    ui->nflip->setChecked(false);

    connect(ui->previewBtn, SIGNAL(clicked()), this, SLOT(onPreviewBtn()));
    connect(ui->applyBtn,   SIGNAL(clicked()), this, SLOT(onApplyBtn()));

    connect(ui->ncount,  SIGNAL(textEdited(QString)),   this, SLOT(ncountNew(QString)));
    connect(ui->nflip,  SIGNAL(clicked(bool)),   this, SLOT(nflipNew(bool)));

    _thread = new CNormalEstimationThread;


    // user pressed cancel button
    connect(ui->cancelBtn,  SIGNAL(clicked()), _thread, SLOT(stop_process()));
    connect(ui->cancelBtn,  SIGNAL(clicked()), this, SLOT(onCancelBtn()));

    // user closed dialg
    connect(this,  SIGNAL(finished(int)), _thread, SLOT(stop_process()));
    //connect(this,  SIGNAL(finished(int)), this, SLOT(onCancelBtn()));

    // working with a thread
    connect(_thread, SIGNAL(send_back(int)), this, SLOT(onMakeProgressStep(int)));
    connect(_thread, SIGNAL(send_result(CNormalCloud*)), this, SLOT(onCompleteProcessing(CNormalCloud*)));
    connect(_thread, SIGNAL(send_result(CNormalCloud*)), _glWidget, SLOT(on_normal_pnts_computed(CNormalCloud*)));
    connect(_thread, SIGNAL(finished()), this, SLOT(onStopProcessing()));
}


CNormalEstimationDlg::~CNormalEstimationDlg()
{
    delete ui;
    delete _thread;
}

void CNormalEstimationDlg::onMakeProgressStep(int val)
{
    ui->progressBar->setValue(val);
}

void CNormalEstimationDlg::onCompleteProcessing(CNormalCloud * ncld)
{
    if(_on_apply)
    {
        close_dialog();
        return;
    }

    _is_result_valid = true;
    ui->progressBar->setValue(100);

}

void CNormalEstimationDlg::onStopProcessing()
{
    ui->applyBtn->setEnabled(true);
    ui->previewBtn->setEnabled(true);
    ui->ncount->setEnabled(true);
    ui->nflip->setEnabled(true);

    _in_process = false;
}

void CNormalEstimationDlg::ncountNew(QString str)
{
    bool ok = false;
    int  cval = str.toInt(&ok);
    if(ok) _ncount = cval;
    _is_result_valid = false;
}

void CNormalEstimationDlg::nflipNew(bool val)
{
    _nflip = val;
    _is_result_valid = false;
}

void CNormalEstimationDlg::close_dialog()
{
    _on_apply = false;
    close();
}

void CNormalEstimationDlg::onCancelBtn()
{
    if(!_in_process)
    {
       //_glWidget->discard_normal_points_of_current_object();
        close_dialog();
    }
    _in_process = false;
    _on_apply = false;
}


void CNormalEstimationDlg::onPreviewBtn()
{


    CPointCloud * pcld = _glWidget->get_optim_points_of_current_object();
    if(pcld == NULL)
    {
       QMessageBox::warning(this, tr("Warning"),wrn_NothingNormal);
       return;
    }

    ui->applyBtn    ->setEnabled(false);
    ui->previewBtn  ->setEnabled(false);
    ui->ncount      ->setEnabled(true);
    ui->nflip       ->setEnabled(true);


    _in_process = true;
    _thread->start_process(pcld, _ncount, _nflip);
}

void CNormalEstimationDlg::onApplyBtn()
{
    _on_apply = true;
    if(!_is_result_valid)
        onPreviewBtn();
    else
        close_dialog();
}


void CNormalEstimationDlg::showEvent(QShowEvent * event)
{
    ui->progressBar->setValue(0);
    _is_result_valid    = false;
    _on_apply           = false;
    _in_process         = false;
}








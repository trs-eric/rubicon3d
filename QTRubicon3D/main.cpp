#include "mainwindow.h"
#include <QApplication>
#include <omp.h>
#include <iostream>
#include  "settings/psettings.h"
CGlobalSettings app_settings;


int main(int argc, char *argv[])
{

    omp_set_nested(1);


    QApplication a(argc, argv);


    MainWindow w;

//    QIcon icon(":icons/icon_app.ico");
 //   w.setWindowIcon(icon);

    w.show();


    return a.exec();
}

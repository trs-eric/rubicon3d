#ifndef CGLSAMPLETRIVTABLE_H
#define CGLSAMPLETRIVTABLE_H

#include "CGLObject.h"

class CGLSampleTrivTable : public CGLObject
{
public:
    CGLSampleTrivTable();
    virtual ~CGLSampleTrivTable();

protected:
    virtual void create_glList();
    void draw_circle(GLfloat radius);
    void draw_tangents(GLfloat radius);
};

#endif // CGLSAMPLETRIVTABLE_H

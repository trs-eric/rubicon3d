#include "cmeshconstructiondlg.h"
#include "ui_cmeshconstructiondlg.h"
#include "cglwidget.h"
#include "cmeshconstructionthread.h"
#include "messages.h"

#include <QMessageBox>
#include<QListWidget>

int cmeshcdlg_width  = 270;
int cmeshcdlg_height = 218;
int cmeshcdlg_eheight = 325;


CMeshConstructionDlg::CMeshConstructionDlg(CGLWidget * glWidget, QWidget *parent) :
    QDialog(parent), _glWidget(glWidget),
    ui(new Ui::CMeshConstructionDlg), _octree_d(12), _octree_d_min(1), _octree_d_max(16),
    _solvdiv(12), _solvdiv_min(1), _solvdiv_max(16),  _sampl_per_node(1.0f), _sampl_per_node_min(1.0),
    _sampl_per_node_max(5.0),  _surf_offset(1.0f), _surf_offset_min(0.5), _surf_offset_max(3.0),
    _expanded(false)
{
    setWindowFlags(Qt::WindowStaysOnTopHint);
    ui->setupUi(this);


    // default edit values
    ui->progressBar->setMaximum(100);
    ui->oct_depth->setText(QString::number(_octree_d));
    ui->solvdiv->setText(QString::number(_solvdiv));
    ui->samplpnode->setText(QString::number(_sampl_per_node));
    ui->soffset->setText(QString::number(_surf_offset));


    connect(ui->previewBtn, SIGNAL(clicked()), this, SLOT(onPreviewBtn()));
    connect(ui->applyBtn,   SIGNAL(clicked()), this, SLOT(onApplyBtn()));

    connect(ui->oct_depth,  SIGNAL(textEdited(QString)),   this, SLOT(n_octree_d(QString)));
    connect(ui->solvdiv,  SIGNAL(textEdited(QString)),   this, SLOT(n_solvdiv(QString)));
    connect(ui->samplpnode,  SIGNAL(textEdited(QString)),   this, SLOT(n_sampl_per_node(QString)));
    connect(ui->soffset,  SIGNAL(textEdited(QString)),   this, SLOT(n_surf_offset(QString)));

     _thread = new CMeshConstructionThread;

    // user pressed cancel button
    connect(ui->cancelBtn,  SIGNAL(clicked()), _thread, SLOT(stop_process()));
    connect(ui->cancelBtn,  SIGNAL(clicked()), this, SLOT(onCancelBtn()));

    // user closed dialg
    connect(this,  SIGNAL(finished(int)), _thread, SLOT(stop_process()));

    // working with a thread
    connect(_thread, SIGNAL(send_back(int)), this, SLOT(onMakeProgressStep(int)));
    connect(_thread, SIGNAL(send_result(CTriangularMesh*)), this, SLOT(onCompleteProcessing(CTriangularMesh*)));

    connect(_thread, SIGNAL(send_result(CTriangularMesh*)), _glWidget, SLOT(on_new_mesh_computed(CTriangularMesh*)));
    connect(_thread, SIGNAL(finished()), this, SLOT(onStopProcessing()));


    this->setFixedSize(cmeshcdlg_width, cmeshcdlg_height);
    //connect(ui->btnMore,  SIGNAL(clicked()), this, SLOT(expand_dlg()));

    ui->chkOne->setChecked(true);
    ui->chkOthr->setChecked(false);
    connect(ui->chkOne, SIGNAL(clicked()), this, SLOT(checkedOneMesh()));
    connect(ui->chkOthr, SIGNAL(clicked()), this, SLOT(checkedOtherMesh()));

    connect(ui->btnAddMesh, SIGNAL(clicked()), this, SLOT(addSelMesh()));
    connect(ui->btnRemMesh, SIGNAL(clicked()), this, SLOT(delSelMesh()));

}

void CMeshConstructionDlg::addSelMesh()
{
    QList<QListWidgetItem *> selMeshes = ui->lvAllMesh->selectedItems();
    if(selMeshes.size() > 0)
    {
        ui->lvSelMesh->addItem(selMeshes.first()->text());
        delete selMeshes.first();
    }

}


void CMeshConstructionDlg::delSelMesh()
{
    QList<QListWidgetItem *> selMeshes = ui->lvSelMesh->selectedItems();
    if(selMeshes.size() > 0)
    {
        ui->lvAllMesh->addItem(selMeshes.first()->text());
        delete selMeshes.first();
    }
}

void CMeshConstructionDlg::init_meshlist()
{

    ui->lvAllMesh->clear();
    ui->lvSelMesh->clear();

    std::vector<CGLCompoundObject*> mlist = _glWidget->get_list_of_valid_meshes();
    std::vector<CGLCompoundObject*>::iterator it =  mlist.begin();
    for( ; it != mlist.end(); it++)
    {
        ui->lvAllMesh->addItem( (*it)->get_title());
    }
}

void CMeshConstructionDlg::checkedOneMesh()
{
    if(ui->chkOne->isChecked())
    {
        ui->chkOthr->setChecked(false);
        this->setFixedSize(cmeshcdlg_width, cmeshcdlg_height);
    }
}

void CMeshConstructionDlg::checkedOtherMesh()
{
    if(ui->chkOthr->isChecked())
    {
        ui->chkOne->setChecked(false);
        this->setFixedSize(cmeshcdlg_width, cmeshcdlg_eheight);
        ui->lvAllMesh->hide();
        ui->lvAllMesh->show();

        ui->lvSelMesh->hide();
        ui->lvSelMesh->show();
    }
}



CMeshConstructionDlg::~CMeshConstructionDlg()
{
    delete ui;
    delete _thread;
}

void CMeshConstructionDlg::onMakeProgressStep(int val)
{
    ui->progressBar->setValue(val);
}

void CMeshConstructionDlg::onCompleteProcessing(CTriangularMesh * m)
{
    if(m == NULL)
    {
        QMessageBox::warning(this, tr("Warning"),wrn_FailedMesh);
        return;
    }
    if(_on_apply)
    {
        close_dialog();
        return;
    }

    _is_result_valid = true;
    ui->progressBar->setValue(100);

}
void CMeshConstructionDlg::onStopProcessing()
{
    ui->applyBtn    ->setEnabled(true);
    ui->previewBtn  ->setEnabled(true);
    ui->oct_depth   ->setEnabled(true);
    ui->solvdiv     ->setEnabled(true);
    ui->samplpnode  ->setEnabled(true);
    ui->soffset     ->setEnabled(true);
    ui->lvAllMesh   ->setEnabled(true);
    ui->lvSelMesh   ->setEnabled(true);
    ui->btnAddMesh  ->setEnabled(true);
    ui->btnRemMesh  ->setEnabled(true);
    ui->chkOne      ->setEnabled(true);
    ui->chkOthr     ->setEnabled(true);


    _in_process = false;
}

void CMeshConstructionDlg::n_octree_d(QString str)
{
    bool ok = false;
    int  cval = str.toInt(&ok);
    if(ok)
    {
        if(cval < _octree_d_min) cval =_octree_d_min;
        if(cval > _octree_d_max) cval =_octree_d_max;
        _octree_d = cval;
        ui->oct_depth->setText(QString::number(cval));
    }
    _is_result_valid = false;
}
void CMeshConstructionDlg::n_solvdiv(QString str)
{
    bool ok = false;
    int  cval = str.toInt(&ok);
    if(ok)
    {
        if(cval < _solvdiv_min) cval =_solvdiv_min;
        if(cval > _solvdiv_max) cval =_solvdiv_max;
        _solvdiv = cval;
        ui->solvdiv->setText(QString::number(cval));
    }
    _is_result_valid = false;
}

void CMeshConstructionDlg::n_sampl_per_node(QString str)
{
    bool ok = false;
    int  cval = str.toInt(&ok);
    if(ok)
    {
        if(cval < _sampl_per_node_min) cval =_sampl_per_node_min;
        if(cval > _sampl_per_node_max) cval =_sampl_per_node_max;
        _sampl_per_node = cval;
        ui->samplpnode->setText(QString::number(cval));
    }
    _is_result_valid = false;
}

void CMeshConstructionDlg::n_surf_offset(QString str)
{
   bool ok = false;
    int  cval = str.toInt(&ok);
    if(ok)
    {
        if(cval < _surf_offset_min) cval =_surf_offset_min;
        if(cval > _surf_offset_max) cval =_surf_offset_max;
        _surf_offset = cval;
        ui->soffset->setText(QString::number(cval));
    }
    _is_result_valid = false;
}


void CMeshConstructionDlg::close_dialog()
{
    _on_apply = false;
    close();
}

void CMeshConstructionDlg::onCancelBtn()
{
    if(!_in_process)
    {
        //_glWidget->discard_mesh_of_current_object();
        close_dialog();
    }
    _in_process = false;
    _on_apply = false;
}

void CMeshConstructionDlg::onPreviewBtn()
{


    CTriangularMesh * tmesh = _glWidget->get_mesh_of_current_object();
    if(tmesh == NULL)
    {
       QMessageBox::warning(this, tr("Warning"),wrn_NothingMesh);
       return;
    }

    ui->applyBtn    ->setEnabled(false);
    ui->previewBtn  ->setEnabled(false);
    ui->oct_depth   ->setEnabled(false);
    ui->solvdiv     ->setEnabled(false);
    ui->samplpnode  ->setEnabled(false);
    ui->soffset     ->setEnabled(false);
    ui->lvAllMesh   ->setEnabled(false);
    ui->lvSelMesh   ->setEnabled(false);
    ui->btnAddMesh  ->setEnabled(false);
    ui->btnRemMesh  ->setEnabled(false);
    ui->chkOne      ->setEnabled(false);
    ui->chkOthr     ->setEnabled(false);

    CNormalCloud * ncld = new CNormalCloud ;
    CTriMesh * trimesh = tmesh->get_tri_mesh();
    ncld->set_normals(trimesh->_points);

    if(!ui->chkOne->isChecked())
    {
        for(int i = 0; i < ui->lvSelMesh->count(); i++)
        {
            QListWidgetItem* item = ui->lvSelMesh->item(i);
            CTriangularMesh * m = _glWidget->find_mesh_by_its_name(item->text());
            if(m != NULL)
            {
                 /// add mesh points to the cloud
                 std::cout << ":::m  " << m << std::endl;
                 ncld->append_normals(m->get_tri_mesh()->_points);
            }
            else
            {
                 std::cout << ":::m " << 0 << std::endl;
            }
        }
     }

    _in_process = true;
    _thread->start_process(ncld, _octree_d,
           _solvdiv, _sampl_per_node, _surf_offset,
      trimesh->_texture, trimesh->_texture_name,
    trimesh->_tex_bb_miny, trimesh->_tex_bb_maxy, trimesh->_tex_init_angle,
              trimesh->_mass_center, trimesh->_trans, trimesh->_scale, trimesh->_rotat);

}

void CMeshConstructionDlg::onApplyBtn()
{
    _on_apply = true;
    if(!_is_result_valid)
        onPreviewBtn();
    else
        close_dialog();
}


void CMeshConstructionDlg::showEvent(QShowEvent * event)
{
    ui->progressBar->setValue(0);
    _is_result_valid    = false;
    _on_apply           = false;
    _in_process         = false;

    init_meshlist();
}


#ifndef CGLOBJECT_H
#define CGLOBJECT_H

#include <map>

#include <gl/gl.h>
#include <gl/glu.h>
#include <gl/glaux.h>


extern const GLuint nonGLlist;

class CGLObject
{
public:
    CGLObject();

    CGLObject(
        GLfloat		rotx,	GLfloat		roty,	GLfloat		rotz,
        GLfloat		tx,		GLfloat		ty,		GLfloat		tz,
        GLfloat		xs,		GLfloat		ys,		GLfloat		zs,
        GLfloat		rx,		GLfloat		ry,		GLfloat		rz,
        GLfloat	red = 1.0f, GLfloat green= 1.0f,	GLfloat	blue = 1.0f);

    virtual ~CGLObject();
    virtual void glDraw();
    virtual void change_vis(){isVisible = (!isVisible);};

    void remake_glList();

protected:
    virtual void create_glList()=0;
    virtual void delete_glLists();

    GLuint _oglid;

public:

    // translation parameters
    GLfloat _tx, _ty, _tz;

    // rotation parameters (degrees)
    GLfloat  _rotx, _roty, _rotz;

    // initial rotation parameters (degrees)
    GLfloat  _irotx, _iroty, _irotz;

    // real size
    GLfloat _xs, _ys, _zs;

    // scaling paramter
    GLfloat _rx, _ry, _rz;

    // color array
    GLfloat _color[3];

    // to be drawn
    bool isVisible;
};

#endif // CGLOBJECT_H

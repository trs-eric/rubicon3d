#ifndef __CARRAY2D_H__	
#define __CARRAY2D_H__


#include <cmath>
#include <iostream>


template <class T> 
class CArray2D
{

public:

    CArray2D() { init_data(); }

    CArray2D(int xs, int ys, T def_val = T(0))
    {
		init_data();
		allocate_data(xs, ys);
        for(int j = 0; j <_ys ; j++)
            for(int i = 0; i <_xs ; i++)
                _data[j][i] = def_val;
    }


    virtual ~CArray2D()
    {
        clear();
    }

	CArray2D(const CArray2D<T>& cpyarr)
	{
		init_data();
	    _xs = cpyarr._xs;
        _ys = cpyarr._ys;
        allocate_data(_xs, _ys);

 	   memcpy(_big_data, cpyarr._big_data, _xs*_ys*sizeof(T));
	}

	CArray2D(const CArray2D<T> * cpyarr)
	{
	   init_data();
	   _xs = cpyarr->_xs;
       _ys = cpyarr->_ys;
       allocate_data(_xs, _ys);
	
 	   memcpy(_big_data, cpyarr._big_data, _xs*_ys*sizeof(T));
	}
 
    void operator =(const CArray2D<T> & cpyarr) 
    {
        if(_data != NULL) clear();
	
        _xs = cpyarr._xs;
        _ys = cpyarr._ys;
        allocate_data(_xs, _ys);

 	   memcpy(_big_data, cpyarr._big_data, _xs*_ys*sizeof(T));

    }


   // 
   void multiply(const CArray2D<T> & arr1, const CArray2D<T> & arr2)
   {
       if(_xs!= arr1._xs || _xs != arr2._xs || 
          _ys!= arr1._ys || _ys != arr2._ys  )
       {
          std::cout << "Error: multiplication failed " << std::endl;
          return;
       }
    
       for(int j = 0; j <_ys ; j++)
           for(int i = 0; i <_xs ; i++)
               _data[j][i] = arr1._data[j][i]* arr2._data[j][i];

   }
 
   void divide(const T val)
   {
       for(int j = 0; j <_ys ; j++)
           for(int i = 0; i <_xs ; i++)
               _data[j][i] /= val;
   }

   // 
   void substraction(const CArray2D<T> & arr1)
   {
       if(_xs!= arr1._xs || _ys!= arr1._ys )
       {
          std::cout << "Error: substaction failed " << std::endl;
          return;
       }
    
       for(int j = 0; j <_ys ; j++)
           for(int i = 0; i <_xs ; i++)
               _data[j][i] = _data[j][i] - arr1._data[j][i];
   }

   void add(const CArray2D<T> & arr1)
   {
       if(_xs!= arr1._xs || _ys!= arr1._ys )
       {
          std::cout << "Error: substaction failed " << std::endl;
          return;
       }
    
       for(int j = 0; j <_ys ; j++)
           for(int i = 0; i <_xs ; i++)
               _data[j][i] = _data[j][i] + arr1._data[j][i];
   }

   void clean_data(T def_val = T(0))
   {
   
       for(int j = 0; j <_ys ; j++)
           for(int i = 0; i <_xs ; i++)
               _data[j][i] = def_val;
   }

   const CArray2D<T> get_subregion(int x0, int y0, int x1, int y1, T val = T(0))
	{
		int nxs = x1-x0+1;
		int nys = y1-y0+1;

		CArray2D<T> res(nxs, nys, val);
		for(int j = 0; j < nys; j++)
		{
		    for(int i = 0; i < nxs;i++)
			{
				if( (y0+j)>=0 && (y0+j) <_ys && (x0+i)>=0 && (x0+i)<_xs)
				{
					res._data[j][i] = this->_data[y0+j][x0+i];
				}
			}
		}
		return 	res;
	};

    T** _data;

    int _xs;
    int _ys;



protected:
	T* _big_data;

    void allocate_data(int xs, int ys)
    {

	    _big_data = new T[xs*ys]; 
        _data = new T*[ys]; 

	    for(int j = 0; j <ys ; j++)
		     _data[j] =  _big_data+j*xs; 
		

        _xs = xs;
        _ys = ys;
	
	
    }

	void init_data()
	{
		_data = NULL;
		_big_data = NULL;
		_xs = 0;
		_ys = 0;
	}

    void clear()
    {

	
        if(_data != NULL) delete [] _data;
        if(_big_data != NULL) delete [] _big_data;

       init_data();
   }



};



#endif

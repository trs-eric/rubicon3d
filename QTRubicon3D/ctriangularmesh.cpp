#include "ctriangularmesh.h"
#include <fstream>
#include <sstream>
#include "cimportmanager.h"
#include <QFileInfo>
#include <qopenglext.h>
#include <QGLWidget>

CTriangularMesh::CTriangularMesh() : _mesh(NULL), _rmode(MESH_TEX)
{
    _color[0] = .8f;
    _color[1] = .8f;
    _color[2] = .8f;

    _rx = 0.05f;
    _ry = 0.05f;
    _rz = 0.05f;

    _wire_glid  = nonGLlist;
    _flat_glid  = nonGLlist;
    _smooth_glid = nonGLlist;
    _tex_glid    = nonGLlist;
    _tex_matid = nonGLlist;

}

CTriangularMesh::~CTriangularMesh()
{
    delete_mesh();
    delete_glLists();
}

void CTriangularMesh::delete_glList_tex()
{
    if(_tex_glid != nonGLlist)
    {
        glDeleteLists(_tex_glid ,1);
        _tex_glid  = nonGLlist;
    }

    if(_tex_matid != nonGLlist)
    {
        glDeleteTextures(1,  &_tex_matid);
        _tex_matid = nonGLlist;
    }
}

void CTriangularMesh::delete_glLists()
{

    if(_wire_glid != nonGLlist)
    {
        glDeleteLists(_wire_glid ,1);
        _wire_glid  = nonGLlist;
    }

    if(_flat_glid != nonGLlist)
    {
        glDeleteLists(_flat_glid ,1);
        _flat_glid  = nonGLlist;
    }

    if(_smooth_glid != nonGLlist)
    {
        glDeleteLists(_smooth_glid ,1);
        _smooth_glid  = nonGLlist;
    }

    delete_glList_tex();

}

void CTriangularMesh::load_tex_matid()
{
    if(_mesh->_texture == NULL) return;

    glEnable(GL_TEXTURE_2D);
    glGenTextures(1, &_tex_matid);
    QImage tximg = QGLWidget::convertToGLFormat(*_mesh->_texture);
    glBindTexture(GL_TEXTURE_2D, _tex_matid);

    glTexImage2D(GL_TEXTURE_2D, 0, 3, tximg.width(), tximg.height(), 0, GL_RGBA, GL_UNSIGNED_BYTE, tximg.bits());
    gluBuild2DMipmaps(GL_TEXTURE_2D, 3, tximg.width(), tximg.height(), GL_RGBA, GL_UNSIGNED_BYTE, tximg.bits());

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S,  GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T,  GL_REPEAT);

    glTexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
    glDisable(GL_TEXTURE_2D);

/*

    int nx = _mesh->_texture->width();
    int ny =  _mesh->_texture->height();
    int byte_count = nx * ny * 4;
    int raw_byte_count = nx * 4;

    uchar * new_bits = new uchar[byte_count];
    uchar * old_bits = _mesh->_texture->bits();

    // OpenGL spec: has to flip image vertically before texture creation
    // std::cout << nx << " " << ny << " " << byte_count  << " " << raw_byte_count << std::endl;
    for( int r = 0; r < ny; r++)
    {
        memcpy ( new_bits + raw_byte_count * r ,
                 old_bits + raw_byte_count * (ny - r - 1),  raw_byte_count );
    }
    // memcpy ( new_bits, old_bits , byte_count);


    glGenTextures(1, &_tex_matid);
    glBindTexture(GL_TEXTURE_2D, _tex_matid);


    /// glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT );
    // glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

    // glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_S,     GL_MIRRORED_REPEAT);
    // glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_T,     GL_MIRRORED_REPEAT);

    ////glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    //glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

     // , GL_MIRRORED_REPEAT, or
    ///
    ////// light texture
    ///glTexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);

    //glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_LINEAR_MIPMAP_LINEAR);
    //glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
    //glTexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
    delete [] new_bits;
*/
}


void CTriangularMesh::create_glList_wire()
{
    _wire_glid = glGenLists(1);
    glNewList(_wire_glid, GL_COMPILE);

    glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);

    glEnable(GL_COLOR_MATERIAL);

    glBegin(GL_TRIANGLES);

    std::cout << "::" << _mesh->_triangles.size() << std::endl;

    std::vector<C3DPointIdx>::iterator itt = _mesh->_triangles.begin();
    for(; itt != _mesh->_triangles.end(); itt++)
    {
        C3DPoint pnt0 = _mesh->_points[ itt->pnt_index[0] ];

        C3DPoint pnt1 = _mesh->_points[ itt->pnt_index[1] ];

        C3DPoint pnt2 = _mesh->_points[ itt->pnt_index[2] ];

        C3DPoint tri_normal =  itt->tri_normal;
        ///std::cout << "create_glList_wire " << tri_normal._x << " " <<   tri_normal._y << " " << tri_normal._z << std::endl;
        glColor3f(_color[0] , _color[1] ,  _color[2] );
        glNormal3f (tri_normal._x, tri_normal._y, tri_normal._z );
        //glNormal3f (pnt0._nx, pnt0._ny, pnt0._nz );
        glVertex3f( pnt0._x, pnt0._y, pnt0._z);

        glColor3f(_color[0] , _color[1] ,  _color[2] );
        glNormal3f (tri_normal._x, tri_normal._y, tri_normal._z );
        //glNormal3f (pnt1._nx, pnt1._ny, pnt1._nz );
        glVertex3f( pnt1._x, pnt1._y, pnt1._z);

        glColor3f(_color[0] , _color[1] ,  _color[2] );
        glNormal3f (tri_normal._x, tri_normal._y, tri_normal._z );
        //glNormal3f (pnt2._nx, pnt2._ny, pnt2._nz );
        glVertex3f( pnt2._x, pnt2._y, pnt2._z);
    }
   glEnd();
   glDisable(GL_COLOR_MATERIAL);

   glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

   glEndList();
}

void CTriangularMesh::create_glList_flat()
{
    _flat_glid = glGenLists(1);
    glNewList(_flat_glid, GL_COMPILE);

    glEnable(GL_COLOR_MATERIAL);
    glBegin(GL_TRIANGLES);

    std::vector<C3DPointIdx>::iterator itt = _mesh->_triangles.begin();
    for(; itt != _mesh->_triangles.end(); itt++)
    {
        C3DPoint pnt0 = _mesh->_points[ itt->pnt_index[0] ];

        C3DPoint pnt1 = _mesh->_points[ itt->pnt_index[1] ];

        C3DPoint pnt2 = _mesh->_points[ itt->pnt_index[2] ];

        C3DPoint tri_normal =  itt->tri_normal;
        ///std::cout << "create_glList_flat " << tri_normal._x << " " <<   tri_normal._y << " " << tri_normal._z << std::endl;

        glColor3f(_color[0] , _color[1] ,  _color[2] );
        // glColor3f(pnt0._r, pnt0._g,  pnt0._b);
        glNormal3f (tri_normal._x, tri_normal._y, tri_normal._z );
        //glNormal3f (pnt0._nx, pnt0._ny, pnt0._nz );
        glVertex3f( pnt0._x, pnt0._y, pnt0._z);

        glColor3f(_color[0] , _color[1] ,  _color[2] );
        //glColor3f(pnt1._r, pnt1._g,  pnt1._b);
        glNormal3f (tri_normal._x, tri_normal._y, tri_normal._z );
        //glNormal3f (pnt1._nx, pnt1._ny, pnt1._nz );
        glVertex3f( pnt1._x, pnt1._y, pnt1._z);

        glColor3f(_color[0] , _color[1] ,  _color[2] );
        // glColor3f(pnt2._r, pnt2._g,  pnt2._b);
        glNormal3f (tri_normal._x, tri_normal._y, tri_normal._z );
        //glNormal3f (pnt2._nx, pnt2._ny, pnt2._nz );
        glVertex3f( pnt2._x, pnt2._y, pnt2._z);
    }
   glEnd();

   glDisable(GL_COLOR_MATERIAL);
   glEndList();

}

void CTriangularMesh::create_glList_smooth()
{
    _smooth_glid = glGenLists(1);
    glNewList(_smooth_glid, GL_COMPILE);

    glEnable(GL_COLOR_MATERIAL);
    glBegin(GL_TRIANGLES);
    std::vector<C3DPointIdx>::iterator itt = _mesh->_triangles.begin();
    for(; itt != _mesh->_triangles.end(); itt++)
    {
        C3DPoint pnt0 = _mesh->_points[ itt->pnt_index[0] ];

        C3DPoint pnt1 = _mesh->_points[ itt->pnt_index[1] ];

        C3DPoint pnt2 = _mesh->_points[ itt->pnt_index[2] ];

        ///std::cout << "create_glList_smooth " << pnt0._nx << " " <<   pnt0._ny << " " << pnt0._nz<< std::endl;

        //C3DPoint tri_normal =  itt->tri_normal;

        glColor3f(_color[0] , _color[1] ,  _color[2] );
        // glColor3f(pnt0._r, pnt0._g,  pnt0._b);
        // glNormal3f (tri_normal._x, tri_normal._y, tri_normal._z );
        glNormal3f (pnt0._nx, pnt0._ny, pnt0._nz );
        glVertex3f( pnt0._x, pnt0._y, pnt0._z);

        glColor3f(_color[0] , _color[1] ,  _color[2] );
        //glColor3f(pnt1._r, pnt1._g,  pnt1._b);
        //glNormal3f (tri_normal._x, tri_normal._y, tri_normal._z );
        glNormal3f (pnt1._nx, pnt1._ny, pnt1._nz );
        glVertex3f( pnt1._x, pnt1._y, pnt1._z);

        glColor3f(_color[0] , _color[1] ,  _color[2] );
        // glColor3f(pnt2._r, pnt2._g,  pnt2._b);
        //glNormal3f (tri_normal._x, tri_normal._y, tri_normal._z );
        glNormal3f (pnt2._nx, pnt2._ny, pnt2._nz );
        glVertex3f( pnt2._x, pnt2._y, pnt2._z);
    }
   glEnd();

   glDisable(GL_COLOR_MATERIAL);
   glEndList();

}

void CTriangularMesh::delete_mesh()
{
    if(_mesh)
    {
        delete _mesh;
        _mesh = NULL;
    }
}


void CTriangularMesh::set_mesh(CTriMesh * m)
{   if(_mesh != NULL)
    {
        delete_mesh();
        delete_glLists();
    }
    _mesh = m;
}

void CTriangularMesh::create_glList_tex()
{
    if(_mesh->_texture == NULL)
    {
        //create_glList_smooth();
        _tex_glid = _smooth_glid;
        return;
    }
    load_tex_matid();

    _tex_glid = glGenLists(1);
    glNewList(_tex_glid, GL_COMPILE);


    glEnable(GL_TEXTURE_2D);
    glBindTexture(GL_TEXTURE_2D, _tex_matid);

    glBegin(GL_TRIANGLES);
    std::vector<C3DPointIdx>::iterator itt = _mesh->_triangles.begin();
    for(; itt != _mesh->_triangles.end(); itt++)
    {
        C3DPoint pnt0 = _mesh->_points[ itt->pnt_index[0] ];

        C3DPoint pnt1 = _mesh->_points[ itt->pnt_index[1] ];

        C3DPoint pnt2 = _mesh->_points[ itt->pnt_index[2] ];


        glNormal3f (pnt0._nx, pnt0._ny, pnt0._nz );
        glTexCoord2f(_mesh->_tex_coords[itt->tex_index[0]].x,
                     _mesh->_tex_coords[itt->tex_index[0]].y);
        // glTexCoord2f(pnt0._tex_x, pnt0._tex_y);

        glVertex3f( pnt0._x, pnt0._y, pnt0._z);

        glNormal3f (pnt1._nx, pnt1._ny, pnt1._nz );
        glTexCoord2f(_mesh->_tex_coords[itt->tex_index[1]].x,
                     _mesh->_tex_coords[itt->tex_index[1]].y);
        // glTexCoord2f(pnt1._tex_x, pnt1._tex_y);
        glVertex3f( pnt1._x, pnt1._y, pnt1._z);
        //glColor3f(_color[0] , 1 ,  _color[2] );

        glNormal3f (pnt2._nx, pnt2._ny, pnt2._nz );
        glTexCoord2f(_mesh->_tex_coords[itt->tex_index[2]].x,
                     _mesh->_tex_coords[itt->tex_index[2]].y);
        // glTexCoord2f(pnt2._tex_x, pnt2._tex_y);
        glVertex3f( pnt2._x, pnt2._y, pnt2._z);
        //glColor3f(_color[0] ,1,  _color[2] );
    }
   glEnd();
   glDisable(GL_TEXTURE_2D);
   glEndList();
}


CTriangularMesh::CTriangularMesh(const CTriangularMesh & src)
{
    _mesh = new CTriMesh(*src._mesh);
    _rmode = src._rmode;

    _color[0] = .8f;
    _color[1] = .8f;
    _color[2] = .8f;

    _rx = 0.05f;
    _ry = 0.05f;
    _rz = 0.05f;

    _wire_glid  = nonGLlist;
    _flat_glid  = nonGLlist;
    _smooth_glid = nonGLlist;
    _tex_glid    = nonGLlist;
    _tex_matid = nonGLlist;

}

void CTriangularMesh::set_correct_glid()
{
    switch(_rmode)
    {
    case MESH_WIRE:
        _oglid = _wire_glid;
        break;
    case MESH_FLAT:
        _oglid = _flat_glid;
        break;
    case MESH_SMOOTH:
        _oglid = _smooth_glid;
        break;
    case MESH_TEX:
        _oglid = _tex_glid;
        break;
    }
}

void  CTriangularMesh::set_rendering_mode(RenderingMode rm)
{
    _rmode = rm;
    set_correct_glid();
}

void CTriangularMesh::create_glList()
{
    delete_glLists();

    create_glList_wire();
    create_glList_flat();
    create_glList_smooth();
    create_glList_tex();
    set_correct_glid();
}


void CTriangularMesh::save(std::ofstream & ofile)
{
    // rmode
    ofile.write(reinterpret_cast<const char *>(&_rmode), sizeof(RenderingMode));
    bool have_mesh = (_mesh == NULL)? false : true;
    ofile.write(reinterpret_cast<const char *>(&_mesh), sizeof(bool));
    if(have_mesh) _mesh->save(ofile);
}

void CTriangularMesh::load(std::ifstream & ifile)
{
    // rmode
    ifile.read(reinterpret_cast<char *>(&_rmode), sizeof(RenderingMode));
    delete_mesh();

    bool have_mesh = false;
    ifile.read(reinterpret_cast<char *>(&have_mesh), sizeof(bool));
    if(have_mesh)
    {
        _mesh = new CTriMesh;
        _mesh->load(ifile);
    }
}

bool CTriangularMesh::m_import_obj(QString &file)
{
    if(file.isEmpty()) return false;
    delete_mesh();
    _mesh = load_obj_mesh(file.toStdString());

    return true;
}

bool CTriangularMesh::m_export(QString & fname)
{
    if(fname.isEmpty()) return false;

    bool res = false;
    // off file format
    if(fname.endsWith(".off", Qt::CaseInsensitive))
        res = m_export_off(fname);
    // obj file format
    if(fname.endsWith(".obj", Qt::CaseInsensitive))
        res = m_export_obj(fname);

    return res;

}

bool CTriangularMesh::m_export_off(QString & fname)
{
    std::ofstream ofile;
    ofile.open(fname.toUtf8().constData(), std::ios::out );
    if(! ofile.is_open())
                return false;

    ofile << "OFF" << std::endl;
    ofile << _mesh->_points.size() << " " << _mesh->_triangles.size() << " " << 0 << std::endl;

    std::vector<C3DPoint>::iterator vit = _mesh->_points.begin();
    for( ; vit != _mesh->_points.end(); vit++ )
        ofile << vit->_x << " " << " " << vit->_y<< " " << vit->_z << std::endl;

    std::vector<C3DPointIdx>::iterator fit = _mesh->_triangles.begin();
    for( ; fit != _mesh->_triangles.end(); fit++ )
        ofile << 3 << " " << fit->pnt_index[0] << " " << fit->pnt_index[1] << " " << fit->pnt_index[2]  << std::endl;

    return true;
}

void CTriangularMesh::remove_mesh_texture()
{
    if(is_textured())
    {
        _mesh->remove_texture();
        delete_glList_tex();
    }
}

void CTriangularMesh::replace_mesh_texture(QImage * tex)
{
    if(is_textured())
    {
        _mesh->replace_texture(tex);
        delete_glList_tex();
    }
}

bool CTriangularMesh::is_textured()
{
    if(_mesh != NULL)
        if(_mesh->_texture != NULL)
            return true;
    return false;
}


QString CTriangularMesh::get_texture_name()
{
    if(!is_textured()) return QString("empty");

    return _mesh->_texture_name.c_str();
}

bool CTriangularMesh::m_export_obj(QString & fname)
{
    std::ofstream ofile;

    ///std::cout <<fname.toLocal8Bit().data() << std::endl;

    ofile.open(fname.toLocal8Bit().data(), std::ios::out );
    if(! ofile.is_open())
                return false;

    ///std::cout << "opened" << std::endl;

    QFileInfo info_fname(fname);
    QString working_dir = info_fname.absolutePath();
    QString base_name = info_fname.baseName();

    if(_mesh->_texture != NULL)
    {
        QString mtlfile =  base_name + ".mtl";
        std::string imgfilename = base_name.toStdString()  + ".png";
        ofile << "mtllib " <<  mtlfile.toStdString()  << std::endl << std::endl;

        mtlfile  = working_dir + "/"  + mtlfile  ;

        // std::cout << mtlfile.toLocal8Bit().data() << std::endl;

         // export mtl file
        std::ofstream mtlfilestrem;
        mtlfilestrem.open(mtlfile.toLocal8Bit().data(), std::ios::out );

        mtlfilestrem << "newmtl 01___Default" << std::endl;
        mtlfilestrem << "Ka 0.200000 0.200000 0.200000" << std::endl;
        mtlfilestrem << "Kd 0.000000 0.000000 1.000000" << std::endl;
        mtlfilestrem << "map_Ka " << imgfilename/*_mesh->_texture_name*/ << std::endl;
        mtlfilestrem << "map_Kd " << imgfilename/*_mesh->_texture_name*/ << std::endl;
        mtlfilestrem.close();


        // save image
        std::string imagfile = working_dir.toStdString() + "/"  + imgfilename/*_mesh->_texture_name*/;
        uchar * bits = _mesh->_texture->bits();
        _mesh->_texture->save(imagfile.c_str());

    }
    // output vertices
    std::vector<C3DPoint>::iterator vit = _mesh->_points.begin();
    for( ; vit != _mesh->_points.end(); vit++ )
        ofile <<  "v " << vit->_x << " " <<  vit->_y<< " " << vit->_z << std::endl;

    // output texture coordinates
    if(_mesh->_texture != NULL)
    {
        std::vector<Vector2f>::iterator itt = _mesh->_tex_coords.begin();
        for( ; itt != _mesh->_tex_coords.end(); itt++ )
            ofile <<  "vt " << itt->x << " " << " " << itt->y <<std::endl;
    }

    // output normals
    vit = _mesh->_points.begin();
    for( ; vit != _mesh->_points.end(); vit++ )
        ofile <<  "vn " << vit->_nx << " " << vit->_ny<< " " << vit->_nz << std::endl;


    // output faces
    std::vector<C3DPointIdx>::iterator fit = _mesh->_triangles.begin();
    for( ; fit != _mesh->_triangles.end(); fit++ )
    {
        ofile << "f ";
        for (int i = 0 ; i < 3; i++ )
        {
            ofile <<  fit->pnt_index[i]+1 << "/";
            if(_mesh->_texture != NULL) // if textured add coords
               ofile << fit->tex_index[i]+1;
            ofile <<  "/" << fit->pnt_index[i]+1 << " ";
        }
        ofile << std::endl;
    }
    ofile.close();

    return true;
}


#ifndef CGLSCENE_H
#define CGLSCENE_H

#include<vector>
#include <QString>

#include "cglcompoundobject.h"

class CGLSampleTrivTable;
class CGLCamera;
class QWidget;

class CGLScene
{
public:
    CGLScene(QWidget * treew);
    virtual ~CGLScene();

    void GLRenderScene();

    bool ImportScene(QString &fname);
    void ExportScene(QString &fname);
    CGLCompoundObject * ConstructSingleScan(std::vector<C3DPoint> & pnts);
    bool ConstructNextScan(std::vector<C3DPoint> & pnts);

    void PrepaireNewScan();
    void ConstructNextScanRight(std::vector<C3DPoint> & pnts);
    void ConstructNextScanLeft(std::vector<C3DPoint> & pnts);
    void ConstructNextTextureRight(CRawTexture *);
    void ConstructNextTextureLeft (CRawTexture *);
    void CleanCurrentScan();

    bool load_from_rub_file(QString &fname);
    void save_into_rub_file(QString &fname);

    inline CGLCamera * get_dim_reference(){return _camera;}
    void update_referece_dependant_glRepo();

    void set_raw_poins_of_current_object(CPointCloud *);
    CPointCloud * get_raw_poins_of_current_object();
    void discard_raw_points_of_current_object();

    void set_optim_poins_of_current_object(CPointCloud *);
    CPointCloud * get_optim_points_of_current_object();
    void discard_optim_points_of_current_object();

    void set_normal_poins_of_current_object(CNormalCloud *);
    CNormalCloud * get_normals_of_current_object();
    void discard_normal_points_of_current_object();

    void set_mesh_of_current_object(CTriangularMesh *);
    CTriangularMesh * get_mesh_of_current_object();
    void discard_mesh_of_current_object();

    void change_current_cobject(CGLCompoundObject *);
    void chainge_view_mode_of_current_object(CGLCompoundObject::ViewingMode vm);
    void chainge_mesh_rmode_of_current_object(CTriangularMesh::RenderingMode rm);

    void update_selection_state(QTreeWidgetItem *, int);
    void update_surface_state(QTreeWidgetItem *, int);
    void update_scan_visability_state(QTreeWidgetItem *, int);
    bool is_romove_action(QTreeWidgetItem * , int );
    void update_remove_scan_state(QTreeWidgetItem * , int );
    void on_single_click_treew_switch(QTreeWidgetItem *, int);

    std::vector<CGLCompoundObject*> get_list_of_valid_meshes();
    CGLCompoundObject* get_current_object(){return _currentCObject;}

    void create_new_cobject(CTriangularMesh *);

protected:
    void CreateScene();
    void DeleteScene();
    void DeleteObjects();

    std::vector<CGLCompoundObject *> _cglobjects;
    CGLCompoundObject * _currentCObject;

    // temporal references to left and right scan
    CGLCompoundObject * _cobj_left;
    CGLCompoundObject * _cobj_right;

    CGLSampleTrivTable * _table;

    CGLCamera * _camera;

    QWidget * _treew;

};

#endif // CGLSCENE_H

#include "cpointcloud.h"
#include<QMessageBox>

#include <fstream>
#include <sstream>
#include <iterator>
#include "messages.h"
#include "cglcamera.h"

extern CGLCamera * gReferencePoint;

CPointCloud::CPointCloud()
{
    _color[0] = 1.0f;
    _color[1] = 1.0f;
    _color[2] = 1.0f;

    _rx = 0.05f;
    _ry = 0.05f;
    _rz = 0.05f;
}


void CPointCloud::save(std::ofstream & ofile)
{
    int rpoint_count = _points.size();
    ofile.write(reinterpret_cast<const char *>(&rpoint_count), sizeof(int));

    // std::cout << "CPointCloud::save " << rpoint_count << std::endl;

    std::vector<C3DPoint>::iterator it = _points.begin();
    for( ; it != _points.end(); it++)
        it->save(ofile);
}

void CPointCloud::load(std::ifstream & ifile)
{
    delete_glLists();
    _points.clear();

    int rpoint_count = 0;
    ifile.read(reinterpret_cast<char *>(&rpoint_count), sizeof(int));

    // std::cout << "CPointCloud::load " << rpoint_count << std::endl;

    for(int i = 0; i < rpoint_count ; i++)
    {
        C3DPoint p;
        p.load(ifile);
        _points.push_back(p);
    }

}

bool CPointCloud::import(QString & file)
{
    if(file.isEmpty()) return false;

    bool res = false;
    // xyz file format
    if(file.endsWith(".xyz", Qt::CaseInsensitive))
        res = import_from_file_xyz(file.toStdString());
    return res;


}

bool CPointCloud::pexport(QString & file)
{
    if(file.isEmpty()) return false;

    bool res = false;
    // xyz file format
    if(file.endsWith(".xyz", Qt::CaseInsensitive))
        res = export_xyz_pnts(file.toStdString());
    return res;
}

bool CPointCloud::export_xyz_pnts(std::string fname)
{
    std::ofstream ofile;
    ofile.open(fname.c_str(), std::ios::out );
    if(! ofile.is_open())
                return false;

    // output vertices ony
    std::vector<C3DPoint>::iterator itv =  _points.begin();
    for(  ; itv != _points.end(); itv++ )
        ofile <<  itv->_x <<  " " << itv->_y<< " " << itv->_z << std::endl;

    ofile.close();
    return true;
}

bool CPointCloud::import_from_file_xyz(std::string fname)
{
    delete_glLists();
    _points.clear();

    std::ifstream ifile;
    ifile.open(fname.c_str(), std::ios::in );
    if(ifile.bad())
        return false;

    std::string line;



    while(ifile.good())
    {
        std::getline (ifile, line);
        std::istringstream iss(line);
        std::vector<std::string> tokens;
        std::copy(std::istream_iterator<std::string>(iss),
                    std::istream_iterator<std::string>(),
                    std::back_inserter<std::vector<std::string> >(tokens));

        if(tokens.size() < 3 )
            continue;

        C3DPoint pval;
        pval._x =  atof(tokens[0].c_str());
        pval._y =  atof(tokens[1].c_str());
        pval._z =  atof(tokens[2].c_str());
        _points.push_back(pval);
    }
    return true;
}

void CPointCloud::create_glList()
{
    delete_glLists();

    if(gReferencePoint)
        create_glList_using_greference();
     else
        create_glList_ignoring_greference();


}

void CPointCloud::create_glList_using_greference()
{
    // find radius of the object
    float radi_sq = 0;
    std::vector<C3DPoint>::iterator it = _points.begin();
    for( ; it != _points.end(); it++)
    {
        float tr = sqrt(it->_x * it->_x *_rx *_rx +it->_y * it->_y *_ry *_ry   + it->_z*it->_z*_rz*_rz);
        if (tr > radi_sq) radi_sq = tr;
    }

    float shadow_cyl_radi = 0.2;
    radi_sq *= shadow_cyl_radi;

    float cam_dist = sqrt(  gReferencePoint->_tx * gReferencePoint->_tx  +
                            //gReferencePoint->_ty * gReferencePoint->_ty +
                            gReferencePoint->_tz * gReferencePoint->_tz);

    float null_dist = cam_dist - radi_sq;

    // create new display list
    _oglid = glGenLists(1);
    glNewList(_oglid, GL_COMPILE);

     // TODO remake
    glDisable(GL_LIGHTING);

    glBegin(GL_POINTS);
    it = _points.begin();
    for( ; it != _points.end(); it++)
    {



        float dp = sqrt(
                  (gReferencePoint->_tx - it->_x*_rx ) * (gReferencePoint->_tx - it->_x*_rx ) +
                  //(gReferencePoint->_ty - it->_y*_ry ) * (gReferencePoint->_ty - it->_y*_ry ) +
                  (gReferencePoint->_tz - it->_z*_rz ) * (gReferencePoint->_tz - it->_z*_rz )
                  );

        float c = (dp - null_dist)/ (2*radi_sq);
        c = 1 - c;
        if(c>1.0f) c = 1.0f;
        if(c<0.0f) c = 0.0f;

        GLfloat red = it->_r;
        GLfloat gre = it->_g;
        GLfloat blu = it->_b;

        glColor3f(c * red, c * gre, c * blu);
        glVertex3f(it->_x, it->_y, it->_z);
    }
    glEnd();

    glEnable(GL_LIGHTING);
    glEndList();

}

void CPointCloud::create_glList_ignoring_greference()
{
    // create new display list
    _oglid = glGenLists(1);
    glNewList(_oglid, GL_COMPILE);

 // TODO remake
 glDisable(GL_LIGHTING);

    glBegin(GL_POINTS);
    std::vector<C3DPoint>::iterator it = _points.begin();
    for( ; it != _points.end(); it++)
    {
       glColor3f(it->_r, it->_g, it->_b);
       glVertex3f(it->_x, it->_y, it->_z);
    }
    glEnd();

// TODO remake
glEnable(GL_LIGHTING);
    glEndList();
}

void CPointCloud::rotateX(float cos_alpha, float sin_alpha, float * x, float * y, float * z)
{
    //float x0 = *x;
    float y0 = *y;
    float z0 = *z;

    //*x =  cos_alpha * x0 + sin_alpha * z0;
    *y =  cos_alpha * y0 - sin_alpha * z0;
    *z =  sin_alpha * y0 + cos_alpha * z0;
}

void CPointCloud::rotateY(float cos_alpha, float sin_alpha, float * x, float * y, float * z)
{
    float x0 = *x;
    //float y0 = *y;
    float z0 = *z;

    *x =  cos_alpha * x0 + sin_alpha * z0;
    //*y = ;
    *z = -sin_alpha * x0 + cos_alpha * z0;
}

void CPointCloud::rotateZ(float cos_alpha, float sin_alpha, float * x, float * y, float * z)
{
    float x0 = *x;
    float y0 = *y;
    //float z0 = *z;

    *x = cos_alpha * x0 - sin_alpha * y0;
    *y = sin_alpha * x0 + cos_alpha * y0;
    //*z = -sin_alpha * x0 + cos_alpha * z0;

}

void CPointCloud::rotate(float rx, float ry, float rz)
{
    float sin_rx = sin(rx);
    float cos_rx = cos(rx);

    float sin_ry = sin(ry);
    float cos_ry = cos(ry);

    float sin_rz = sin(rz);
    float cos_rz = cos(rz);

    std::vector<C3DPoint>::iterator it = _points.begin();
    for( ; it != _points.end(); it++)
    {
        rotateX(cos_rx, sin_rx, &(it->_x), &(it->_y), &(it->_z));
        rotateZ(cos_rz, sin_rz, &(it->_x), &(it->_y), &(it->_z));
        rotateY(cos_ry, sin_ry, &(it->_x), &(it->_y), &(it->_z));


       // it->_x *=  sx;
       // it->_y *=  sy;
       // it->_z *=  sz;
    }
    delete_glLists();
}

void CPointCloud::scale(float sx, float sy, float sz)
{
    std::vector<C3DPoint>::iterator it = _points.begin();
    for( ; it != _points.end(); it++)
    {
        it->_x *=  sx;
        it->_y *=  sy;
        it->_z *=  sz;
    }
    delete_glLists();

}

void CPointCloud::translate(float dx, float dy, float dz)
{
    std::vector<C3DPoint>::iterator it = _points.begin();
    for( ; it != _points.end(); it++)
    {
        it->_x += dx; ///_rx);
        it->_y += dy; ///_ry);
        it->_z += dz; ///_rz);
    }
    delete_glLists();
}

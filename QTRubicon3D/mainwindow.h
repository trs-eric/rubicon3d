#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

QT_BEGIN_NAMESPACE
class QAction;
class QMenu;
class QToolBar;
QT_END_NAMESPACE

class CGLWidget;
class CPointOptimizeDlg;
class CNormalEstimationDlg;
class CMeshConstructionDlg;
class CConvexMeshConstructionDlg;
class CClusteringDecimDlg;
class CScanReconstructionDlg;
class CCropDiskDlg;
class CMovRotScaDlg;

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

    void closeEvent(QCloseEvent *event);

private slots:
    void new_scene();
    void about();
    void tobeimplented();
    void scanning();
    void device_calibration();
    void transform_mesh();

    void delaunay_mesh_test();
    void mesh_decimation();
    void optimizePntsDlg();
    void normalsEstDlg();
    void meshConstDlg();
    void cropDiskDlg();

    void import_scene();
    void export_scene();

    void load_project();
    void save_project();
    void save_as_project();
    void import_photogrammetry();

private:
    void createActions();
    void createMenus();
    void createToolBars();
    void createStatusBar();
    void createAuxDialogs();


    // menu
    QMenu *fileMenu;
    QMenu *actMenu;
    QMenu *viewMenu;
    QMenu *helpMenu;

    // actions
    QAction *newAct;
    QAction *openAct;
    QAction *saveAct;
    QAction *saveAsAct;
    QAction *importAct;
    QAction *exportAct;
    QAction *pptAct;

    QAction *scanAct;
    QAction *optimAct;
    QAction *pnormAct;
    QAction *meshAct;
    QAction *meshActS;
    QAction *meshActD;
    QAction *meshOpt;
    QAction *meshLSmooth;
    QAction *texAct;
    QAction *calibAct;

    QAction *movAct;
    QAction *rotAct;
    QAction *cropAct;
    QAction *erasAct;
    QAction *aligAct;


    QAction *exitAct;
    QAction* aboutAct;

    // toolbars
    QToolBar * mainToolBar;
    QToolBar * toolsToolBar;
    ///QToolBar * toolsAuxToolBar;


    // OpenGL scene widget
    QWidget *centralWidget;
    CGLWidget *glWidget;

    CPointOptimizeDlg * _poptDlg;
    CNormalEstimationDlg *_normeDlg;
    CMeshConstructionDlg * _meshCDlg;
    CConvexMeshConstructionDlg * _cvxMeshCDlg;
    CClusteringDecimDlg * _clDesimDlg;
    CScanReconstructionDlg * _scanDlg;
    CCropDiskDlg * _cropDlg;
    CMovRotScaDlg * _mrsDlg;

    bool _is_actul_scene;
    QString _appName;
    QString _current_project;
    QString _title;

private:
    Ui::MainWindow *ui;
};

#endif // MAINWINDOW_H

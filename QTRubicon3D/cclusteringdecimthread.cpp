#include "cclusteringdecimthread.h"
#include "ctriangularmesh.h"
#include "cnormalcloud.h"
#include "graphicutils.h"

#include "meshlab/mesho.h"
#include "settings/psettings.h"

CClusteringDecimThread::CClusteringDecimThread(QObject *parent) :
    QThread(parent), _inmesh(NULL), _csize(1)
{
    _abort = false;
}

CClusteringDecimThread::~CClusteringDecimThread()
{
    _mutex.lock();
    _abort = true;
    _mutex.unlock();
    wait();
}

void CClusteringDecimThread::start_process(CTriangularMesh * mesh, float csz)
{
    _abort = false;
    _inmesh     = mesh;
    _csize      = csz;

    start();
}

void CClusteringDecimThread::stop_process()
{
    _mutex.lock();
    _abort = true;
    _mutex.unlock();
}

void CClusteringDecimThread::emit_send_back(int i)
{
     emit send_back(i);
}

void CClusteringDecimThread::run()
{
     if(_inmesh == NULL) return;

     // convert CTriangularMesh to Mesh0
      CMeshO cm;
      ///cm.bbox.SetNull();
{      CTriMesh * tri_mesh = _inmesh->get_tri_mesh();

      float min_x = 0, min_y = 0, min_z = 0;
      float max_x = 0, max_y = 0, max_z = 0;

      int n_vert = tri_mesh->_points.size();
      CMeshO::VertexIterator v_iter =  vcg::tri::Allocator<CMeshO>::AddVertices(cm, n_vert);
      for (unsigned int i=0; i < n_vert; i++, v_iter++)
      {
          (*v_iter).P()[0] = tri_mesh->_points[i]._x;
          (*v_iter).P()[1] = tri_mesh->_points[i]._y;
          (*v_iter).P()[2] = tri_mesh->_points[i]._z;

          (*v_iter).N()[0] = tri_mesh->_points[i]._nx;
          (*v_iter).N()[1] = tri_mesh->_points[i]._ny;
          (*v_iter).N()[2] = tri_mesh->_points[i]._nz;

          ////cm.bbox.Add((*v_iter).P()); // update bound box
          if(min_x > tri_mesh->_points[i]._x) min_x = tri_mesh->_points[i]._x;
          if(min_y > tri_mesh->_points[i]._y) min_y = tri_mesh->_points[i]._y;
          if(min_z > tri_mesh->_points[i]._z) min_z = tri_mesh->_points[i]._z;

          if(max_x < tri_mesh->_points[i]._x) max_x = tri_mesh->_points[i]._x;
          if(max_y < tri_mesh->_points[i]._y) max_y = tri_mesh->_points[i]._y;
          if(max_z < tri_mesh->_points[i]._z) max_z = tri_mesh->_points[i]._z;

          //cm.bbox.min
         // std::cout << (*v_iter).P()[0] << " " << (*v_iter).P()[1] << " " << (*v_iter).P()[2] << " ";
         // std::cout << (*v_iter).N()[0] << " " << (*v_iter).N()[1] << " " << (*v_iter).N()[2] << std::endl;
      }

      if (_abort)
      {
          emit send_back(0);
          return;
      }

      unsigned int n_face = tri_mesh->_triangles.size();
      vcg::tri::Allocator<CMeshO>::AddFaces(cm, n_face);
      for (unsigned int f=0; f < n_face; f++)
      {
          cm.face[f].V(0) = &(cm.vert[tri_mesh->_triangles[f].pnt_index[0]]);
          cm.face[f].V(1) = &(cm.vert[tri_mesh->_triangles[f].pnt_index[1]]);
          cm.face[f].V(2) = &(cm.vert[tri_mesh->_triangles[f].pnt_index[2]]);
      }

      //std::cout << cm.bbox.Diag() << std::endl;


      emit send_back(20);


     // clustering decimation

      //std::cout << "treshold " << _csize<< std::endl;
      //std::cout << "before v " << cm.vert.size() << " f " << cm.face.size() << std::endl;

      vcg::Box3f bb;
      bb.min = vcg::Point3f(min_x, min_y, min_z);
      bb.max = vcg::Point3f(max_x, max_y, max_z);

     //std::cout << bb.Diag() << std::endl;
     //std::cout << min_x << " " << min_y << " " << min_z  << std::endl;
     //std::cout << max_x << " " << max_y << " " << max_z  << std::endl;

      vcg::tri::Clustering<CMeshO, vcg::tri::AverageColorCell<CMeshO> > ClusteringGrid;
      ClusteringGrid.Init(bb,100000,_csize); // bbox
      ClusteringGrid.AddMesh(cm);
      ClusteringGrid.ExtractMesh(cm);
      //vcg::tri::UpdateNormals<CMeshO>::PerVertexNormalizedPerFace(cm);
      if (_abort)
      {
          emit send_back(0);
          return;
      }

      emit send_back(70);
}
      /// std::cout << "after v " << cm.vert.size() << " f " << cm.face.size() << std::endl;

      // convert Mesh0 to CTriangularMesh
      CTriMesh * rmesh = new CTriMesh ;

      int n_overt = cm.vert.size();


      ///std::cout << " points " << n_overt << std::endl;
      // insert points
      std::vector<C3DPoint>	 pnts;
      for (unsigned int i=0; i < n_overt; i++)
      {
          C3DPoint xpp;
          xpp._x  = cm.vert[i].P()[0];
          xpp._y  = cm.vert[i].P()[1];
          xpp._z  = cm.vert[i].P()[2];

          pnts.push_back(xpp);
      }
      ///std::cout << " points inserted " << std::endl;

      if (_abort)
      {
          emit send_back(0);
          return;
      }

      // insert triangles

      unsigned int n_oface = cm.face.size();
      rmesh->_triangles.resize(n_oface);

     /// std::cout << " trinagles " << n_oface   << std::endl;
      /// std::vector<C3DPointIdx> fvec(n_oface);
      for (unsigned int f=0; f < n_oface; f++)
      {
          C3DPointIdx tri;

          tri.pnt_index[0] = cm.face[f].V(0)-(&*(cm.vert.begin()));
          tri.pnt_index[1] = cm.face[f].V(1)-(&*(cm.vert.begin()));
          tri.pnt_index[2] = cm.face[f].V(2)-(&*(cm.vert.begin()));

          ///std::cout <<"fcomp " << tri.pnt_index[0] << " " << tri.pnt_index[1] << " " << tri.pnt_index[2] << std::endl;
          rmesh->_triangles[f] = tri;
     }
     ////std::cout << " triangles inserted " << std::endl;


      if (_abort)
      {
          emit send_back(0);
          return;
      }

      /// std::cout << " iterate over triangles " << rmesh->_triangles.size()<< std::endl;

      std::vector< std::vector <int> *> v(n_overt, NULL);
      ///////// update mesh normals per face
      std::vector<C3DPointIdx>::iterator itt = rmesh->_triangles.begin();
      for(int fidx = 0 ; itt != rmesh->_triangles.end(); itt++, fidx++)
      {

          ///std::cout << "vidx: " <<  itt->pnt_index[0] << " " << itt->pnt_index[1] << " " << itt->pnt_index[2] << std::endl;

          if(v[itt->pnt_index[0]] == NULL)
          {
                v[ itt->pnt_index[0] ] = new std::vector <int>;
                v[ itt->pnt_index[0] ]-> push_back(fidx);

               /// std :: cout << "1 " <<  itt->pnt_index[0]  << std::endl;
          }
          else
          {
                v[ itt->pnt_index[0] ]->push_back(fidx);
               /// std :: cout << "1 " <<  itt->pnt_index[0]  << std::endl;
          }
          /// rmesh->_points[ itt->pnt_index[0] ]._faceidx.push_back(fidx);
          C3DPoint pnt0 = pnts[ itt->pnt_index[0] ];

          if(v[itt->pnt_index[1]] == NULL)
          {
                v[ itt->pnt_index[1] ] = new std::vector <int>;
                v[ itt->pnt_index[1] ]-> push_back(fidx);
          }
          else
                v[ itt->pnt_index[1] ]->push_back(fidx);
          /// rmesh->_points[ itt->pnt_index[1] ]._faceidx.push_back(fidx);
          C3DPoint pnt1 = pnts[ itt->pnt_index[1] ];

          if(v[itt->pnt_index[2]] == NULL)
          {
                v[ itt->pnt_index[2] ] = new std::vector <int>;
                v[ itt->pnt_index[2] ]-> push_back(fidx);
          }
          else
                v[ itt->pnt_index[2] ]->push_back(fidx);
          /// rmesh->_points[ itt->pnt_index[2] ]._faceidx.push_back(fidx);
          C3DPoint pnt2 = pnts[ itt->pnt_index[2] ];

          itt->tri_normal = compute_normal( pnt2, pnt1, pnt0);

          ///if(!(fidx %10))
          ///    std::cout << fidx << std::endl;
      }

      if (_abort)
      {
          emit send_back(0);
          return;
      }

      //// std::cout << " update mesh" << std::endl;

      //////// update mesh normals per verteces
      int pnts_count  = pnts.size();
      std::vector<C3DPoint>::iterator itv = pnts.begin();
      for(int p = 0; itv != pnts.end(); itv++, p++)
      {

          C3DPoint xpp;
          xpp._x  = itv->_x;
          xpp._y  = itv->_y;
          xpp._z  = itv->_z;


          float fnx =0;
          float fny =0;
          float fnz =0;

          if(v[p] != NULL)
          {
             /// std::cout << "Entering " << std::endl;
            std::vector<int>::iterator fit = v[p]->begin();
            for( ; fit != v[p]->end(); fit++)
            {

                fnx += rmesh->_triangles[(*fit)].tri_normal._x;
                fny += rmesh->_triangles[(*fit)].tri_normal._y;
                fnz += rmesh->_triangles[(*fit)].tri_normal._z;

                xpp._faceidx.push_back(*fit);
            }
            normalize3(fnx, fny, fnz);
          }

          xpp._nx = fnx;
          xpp._ny = fny;
          xpp._nz = fnz;

          rmesh->_points.push_back(xpp);

         // std:: cout << "f:" << xpp._faceidx.size() << " " << xpp._nx << " " << xpp._ny << " " << xpp._nz << std::endl;
      }

      std::cout << " done points " << rmesh->_points.size() << std::endl;

      std::vector< std::vector <int> *>::iterator cit = v.begin();
      for( ; cit != v.end(); cit++)
          if((*cit) != NULL) delete *cit;



//////////////// working with a texture

     if(_inmesh->is_textured())
     {

         CTriMesh * itmesh = _inmesh->get_tri_mesh();
         undo_transform(rmesh);

         /// /////////////////
         QImage * teximg = new QImage(itmesh->_texture->copy());
         rmesh->_texture = teximg;
         rmesh->_texture_name = itmesh->_texture_name;

         float bbymin = _inmesh->get_tri_mesh()->_tex_bb_miny;
         float bbymax = _inmesh->get_tri_mesh()->_tex_bb_maxy;
         float stangl = _inmesh->get_tri_mesh()->_tex_init_angle;

         rmesh->_tex_bb_miny = bbymin;
         rmesh->_tex_bb_maxy = bbymax;
         rmesh->_tex_init_angle = stangl;

         int tot_pnts = rmesh->_points.size();
         for( int k = 0; k < tot_pnts; k++)
         {
             Vector2f tex_coord;
             float angle_norm = circular_modulo(stangl + cPI - atan2(rmesh->_points[k]._x, rmesh->_points[k]._z))/cPI2;

             tex_coord.x = angle_norm;
             tex_coord.y = (rmesh->_points[k]._y - bbymin)/( bbymax - bbymin);
             if(tex_coord.y > 1) tex_coord.y = 1;
             if(tex_coord.y < 0) tex_coord.y = 0;

             rmesh->_tex_coords.push_back(tex_coord);
         }

         std::vector<C3DPointIdx>::iterator itt = rmesh->_triangles.begin();
         for( ; itt != rmesh->_triangles.end(); itt++)
         {
             itt->tex_index[0] = itt->pnt_index[0];
             itt->tex_index[1] = itt->pnt_index[1];
             itt->tex_index[2] = itt->pnt_index[2];
         }

         redo_transform(rmesh);

     }
///////////////////////////////////////

     CTriangularMesh * outmesh =  new CTriangularMesh;
     outmesh -> set_mesh(rmesh);

     emit send_result(outmesh);
     emit send_back(100);


}

void CClusteringDecimThread::undo_transform(CTriMesh * rmesh)
{
    CTriMesh * itmesh = _inmesh->get_tri_mesh();

    rmesh->_mass_center.x = itmesh->_mass_center.x;
    rmesh->_mass_center.y = itmesh->_mass_center.y;
    rmesh->_mass_center.z = itmesh->_mass_center.z;

    rmesh->_trans.x = itmesh->_trans.x;
    rmesh->_trans.y = itmesh->_trans.y;
    rmesh->_trans.z = itmesh->_trans.z;
    rmesh->translate(-itmesh->_mass_center.x -itmesh ->_trans.x,
                     -itmesh->_mass_center.y -itmesh ->_trans.y,
                     -itmesh->_mass_center.z -itmesh ->_trans.z);

    rmesh->_rotat.x = itmesh->_rotat.x;
    rmesh->_rotat.y = itmesh->_rotat.y;
    rmesh->_rotat.z = itmesh->_rotat.z;
    rmesh->rotate_rev( BGM(-itmesh->_rotat.x) ,
                       BGM(-itmesh->_rotat.y) ,
                       BGM(-itmesh->_rotat.z) );

    rmesh->_scale.x = itmesh->_scale.x;
    rmesh->_scale.y = itmesh->_scale.y;
    rmesh->_scale.z = itmesh->_scale.z;
    rmesh->scale(  1.0/itmesh->_scale.x,
                   1.0/itmesh->_scale.y,
                   1.0/itmesh->_scale.z);


    rmesh->translate(itmesh->_mass_center.x ,
                     itmesh->_mass_center.y ,
                     itmesh->_mass_center.z );
}

void CClusteringDecimThread::redo_transform(CTriMesh * rmesh)
{
    CTriMesh * itmesh = _inmesh->get_tri_mesh();

    rmesh->translate(-itmesh->_mass_center.x ,
                     -itmesh->_mass_center.y ,
                     -itmesh->_mass_center.z );

    rmesh->scale(  itmesh->_scale.x,
                   itmesh->_scale.y,
                   itmesh->_scale.z);

    rmesh->rotate( BGM(itmesh->_rotat.x) ,
                   BGM(itmesh->_rotat.y) ,
                   BGM(itmesh->_rotat.z) );

    rmesh->translate(itmesh->_mass_center.x + itmesh ->_trans.x,
                     itmesh->_mass_center.y + itmesh ->_trans.y,
                     itmesh->_mass_center.z + itmesh ->_trans.z);

}

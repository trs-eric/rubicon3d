#include "cglwidget.h"
#include <cmath>
#include <algorithm>

#include<QMouseEvent>
#include<QMessageBox>
#include<QTreeWidgetItem>
#include "cglsampletrivtable.h"

#include "cglcamera.h"
#include "messages.h"

#include "cimportmanager.h"

extern CGLCamera * gReferencePoint;


CGLWidget::CGLWidget(QWidget * treew, QWidget *parent) :
    QGLWidget(parent),_treew(treew),  _scene(NULL)
{
    setFocusPolicy(Qt::StrongFocus);

    _mouseLastPos = QPoint(0,0);


}

std::vector<CGLCompoundObject *> CGLWidget::get_list_of_valid_meshes()
{
    if(_scene != NULL)
    {
        std::vector<CGLCompoundObject *>  res = _scene->get_list_of_valid_meshes();
        std::vector<CGLCompoundObject *> ::iterator pos = std::find(res.begin(), res.end(), _scene->get_current_object());
        if (pos != res.end())  res.erase(pos);

        return res;

    }
    else
        return std::vector<CGLCompoundObject *> ();
}

CTriangularMesh * CGLWidget::find_mesh_by_its_name(QString ttl)
{
    std::vector<CGLCompoundObject *>  res = _scene->get_list_of_valid_meshes();
    std::vector<CGLCompoundObject *>::iterator it =  res.begin();
    for( ; it != res.end(); it ++)
    {
        if((*it)->get_title() == ttl)
            return (*it)->get_mesh();
    }
    return NULL;
}

void CGLWidget::on_double_click_treew_item(QTreeWidgetItem * item, int col)
{
   _scene->update_selection_state(item,col);
   updateGL();
}


void CGLWidget::on_single_click_treew_item(QTreeWidgetItem * item, int col)
{
    _scene->on_single_click_treew_switch(item,col);
    updateGL();
}
CGLWidget::~CGLWidget()
{
    makeCurrent();
}

void CGLWidget::make_new_scene()
{
    delete_scene();
    updateGL();
}

void CGLWidget::delete_scene()
{
    if(_scene)
    {
        delete _scene;
        _scene = NULL;
    }
}

void CGLWidget::initializeGL()
{
    static const GLfloat lightPos[4] = { 5.0f, 5.0f, 10.0f, 1.0f };
    static const GLfloat reflectance1[4] = { 0.8f, 0.1f, 0.0f, 1.0f };
    static const GLfloat reflectance2[4] = { 0.0f, 0.8f, 0.2f, 1.0f };
    static const GLfloat reflectance3[4] = { 0.2f, 0.2f, 1.0f, 1.0f };

    glLightfv(GL_LIGHT0, GL_POSITION, lightPos);
    glEnable(GL_LIGHTING);
    glEnable(GL_LIGHT0);
    glEnable(GL_DEPTH_TEST);

    glEnable(GL_NORMALIZE);

    //glEnable(GL_COLOR_MATERIAL);


    glClearColor(0.196f, 0.196f, 0.258f, 0.0f);
}

GLuint texid;
void Load_texture()
{

    QString tex_file("eggtex.jpg"/*tex_check.png"*/);
    QImage * img = new QImage(tex_file);
    uchar * bits = img->bits();



    glGenTextures(1, &texid);
    glBindTexture(GL_TEXTURE_2D, texid);
    glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_LINEAR); // scale linearly when image bigger than texture
    glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_LINEAR); // scale linearly when image smalled than texture


    glTexImage2D(GL_TEXTURE_2D, 0, 4, img->width(), img->height(), 0, GL_BGRA, GL_UNSIGNED_BYTE, bits);
}

void Draw_texture()
{
    int h = 2;
        glEnable(GL_TEXTURE_2D);
    glBindTexture(GL_TEXTURE_2D, texid);
    glBegin(GL_QUADS);

        //glNormal3f (0, 0, 10);
        //glColor3f(1, 0, 0);
        glTexCoord2f(0.0f, 1.0f);
        glVertex3f(-h,  -h, 0);

        //glNormal3f (0, 0, 10);
        //glColor3f(0, 1, 0);
        glTexCoord2f(0.0f, 0.0f);
        glVertex3f(-h, h, 0);


        //glNormal3f (0, 0, 10);
        //glColor3f(0, 1, 0);
        glTexCoord2f(1.0f, 0.0f);
        glVertex3f(h, h, 0);


        //glNormal3f (0, 0, 10);
        //glColor3f(1, 0, 0);
        glTexCoord2f(1.0f, 1.0f);
        glVertex3f(h, -h, 0);
    glEnd();
        glDisable(GL_TEXTURE_2D);
}

void CGLWidget::paintGL()
{
    if(_scene == NULL)
    {
        _scene = new CGLScene(_treew);
      //Load_texture();
    }
    _scene->GLRenderScene();
    //Draw_texture();
}


void CGLWidget::resizeGL(int width, int height)
{
    if(width ==0)  height = 1;
    glViewport(0,0,width ,height );
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluPerspective(45,GLfloat(width) /height ,0.5,500);
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
}


void CGLWidget::mousePressEvent(QMouseEvent *event)
{
    _mouseLastPos = event->pos();
    updateGL();
}

void CGLWidget::mouseMoveEvent(QMouseEvent *event)
{
    float diffx = event->x() - _mouseLastPos.x();
    float diffy = event->y() - _mouseLastPos.y();
    _mouseLastPos = event->pos();

    float vtr  = 0.02f;
    float vrot = 0.09f;
    bool pos_changed  = false;

    if( event->buttons() & Qt::RightButton )
    {
        gReferencePoint->turn_right (2*vrot*diffx);
        gReferencePoint->turn_down  (2*vrot*diffy);
        pos_changed = true;
    } else 	if( event->buttons() & Qt::MiddleButton)
    {
        gReferencePoint->move_left (vtr*diffx) ;
        gReferencePoint->move_up (vtr*diffy);
        pos_changed = true;
    }
    if (pos_changed)
    {
        _scene->update_referece_dependant_glRepo();
        updateGL();
    }
}


void CGLWidget::udate_gl()
{
    updateGL();
}

void CGLWidget::wheelEvent ( QWheelEvent* event)
{
    float vtr  = 0.01f;
    gReferencePoint->move_forward(vtr*event->delta());

    // should we remake gl?
    // _scene->update_referece_dependant_glRepo();
    updateGL();
}

void CGLWidget::import_scene(QString &fname)
{
    if(!_scene->ImportScene(fname))
        QMessageBox::warning(this, tr("Warning"), err_cannotLoadFile);

    compute_proper_col_width();
    updateGL();
}

void CGLWidget::construct_single_scan(std::vector<C3DPoint> & pnts)
{
    _scene->ConstructSingleScan(pnts);

    compute_proper_col_width();
    updateGL();
}

void CGLWidget::prepaire_new_scan()
{
    _scene->PrepaireNewScan();
}

void CGLWidget::construct_next_scan_left(std::vector<C3DPoint> * pnts)
{
    _scene->ConstructNextScanLeft(*pnts);
    delete pnts;
    compute_proper_col_width();
    updateGL();
}

void CGLWidget::construct_next_scan_right(std::vector<C3DPoint> * pnts)
{
    _scene->ConstructNextScanRight(*pnts);
    delete pnts;
    compute_proper_col_width();
    updateGL();
}

void CGLWidget::construct_next_texture_left(CRawTexture * tex)
{
    _scene->ConstructNextTextureLeft(tex);
    compute_proper_col_width();
    updateGL();
}

void CGLWidget::construct_next_texture_right(CRawTexture * tex)
{
    _scene->ConstructNextTextureRight(tex);
    compute_proper_col_width();
    updateGL();
}

void CGLWidget::construct_next_scan(std::vector<C3DPoint> * pnts)
{
    _scene->ConstructNextScan(*pnts);
    delete pnts;
    compute_proper_col_width();
    updateGL();
}


void CGLWidget::export_scene(QString &fname)
{
    _scene->ExportScene(fname);
}


void CGLWidget::load_project(QString &fname)
{
    if(!_scene->load_from_rub_file(fname))
        QMessageBox::warning(this, tr("Warning"), err_cannotLoadFile);

    compute_proper_col_width();
    updateGL();
}


void CGLWidget::compute_proper_col_width()
{
    ((QTreeWidget*)_treew)->resizeColumnToContents(0);
}
void CGLWidget::save_project(QString &fname)
{
    _scene->save_into_rub_file(fname);
}

void CGLWidget::on_raw_pnts_computed(CPointCloud* pnts)
{
    if(pnts == NULL) return;
    _scene->set_raw_poins_of_current_object(pnts);
    compute_proper_col_width();
    updateGL();
}

void CGLWidget::on_optim_pnts_computed(CPointCloud* pnts)
{
    if(pnts == NULL) return;
    _scene->set_optim_poins_of_current_object(pnts);
    compute_proper_col_width();
    updateGL();
}

void CGLWidget::on_normal_pnts_computed(CNormalCloud* nrms)
{
    if(nrms == NULL) return;
    _scene->set_normal_poins_of_current_object(nrms);
    updateGL();
}
void CGLWidget::on_mesh_computed(CTriangularMesh * mesh)
{
    if(mesh == NULL) return;
    _scene->set_mesh_of_current_object(mesh);
    updateGL();
}

void CGLWidget::on_new_mesh_computed(CTriangularMesh * mesh)
{
    if(mesh == NULL) return;
    _scene->create_new_cobject(mesh);
    compute_proper_col_width();
    updateGL();
}

CPointCloud * CGLWidget::get_raw_points_of_current_object()
{
    return _scene->get_raw_poins_of_current_object();
}

void CGLWidget::discard_raw_points_of_current_object()
{
    _scene->discard_raw_points_of_current_object();
    updateGL();
}

void CGLWidget::clean_current_scan()
{
    _scene->CleanCurrentScan();
    updateGL();
}

CPointCloud * CGLWidget::get_optim_points_of_current_object()
{
    return _scene->get_optim_points_of_current_object();
}

void CGLWidget::discard_optim_points_of_current_object()
{
   _scene->discard_optim_points_of_current_object();
   updateGL();
}


void CGLWidget::discard_mesh_of_current_object()
{
    _scene->discard_mesh_of_current_object();
    updateGL();
}


CTriangularMesh * CGLWidget::get_mesh_of_current_object()
{
    return _scene->get_mesh_of_current_object();
}

CNormalCloud * CGLWidget::get_normals_of_current_object()
{
    return _scene->get_normals_of_current_object();
}

void CGLWidget::discard_normal_points_of_current_object()
{
    _scene->discard_normal_points_of_current_object();
    updateGL();
}
void CGLWidget::keyPressEvent(QKeyEvent *event)
{
    switch(event->key())
    {
    case Qt::Key_R:
        _scene->chainge_view_mode_of_current_object(CGLCompoundObject::RAW_POINTS);
        updateGL();
        break;
    /* case Qt::Key_O:
        _scene->chainge_view_mode_of_current_object(CGLCompoundObject::OPTIM_POINTS);
        updateGL();
        break;
    case Qt::Key_N:
        _scene->chainge_view_mode_of_current_object(CGLCompoundObject::NORMALS);
        updateGL();
        break;*/
    case Qt::Key_M:
        _scene->chainge_view_mode_of_current_object(CGLCompoundObject::MESH);
        updateGL();
        break;
    case Qt::Key_Z:
        _scene->chainge_view_mode_of_current_object(CGLCompoundObject::NONE);
        updateGL();
        break;
    case Qt::Key_1:
        _scene->chainge_mesh_rmode_of_current_object(CTriangularMesh::MESH_WIRE);
        updateGL();
        break;
    case Qt::Key_2:
        _scene->chainge_mesh_rmode_of_current_object(CTriangularMesh::MESH_FLAT);
        updateGL();
        break;
    case Qt::Key_3:
        _scene->chainge_mesh_rmode_of_current_object(CTriangularMesh::MESH_SMOOTH);
        updateGL();
        break;
    case Qt::Key_4:
        _scene->chainge_mesh_rmode_of_current_object(CTriangularMesh::MESH_TEX);
        updateGL();
        break;

    }
}

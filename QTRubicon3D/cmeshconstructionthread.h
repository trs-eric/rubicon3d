#ifndef CMESHCONSTRUCTIONTHREAD_H
#define CMESHCONSTRUCTIONTHREAD_H

#include <QThread>
#include <QMutex>
#include "graphicutils.h"

class CNormalCloud;
class CTriangularMesh;
class CMeshConstructionThread : public QThread
{
    Q_OBJECT
public:
    explicit CMeshConstructionThread(QObject *parent = 0);
    ~CMeshConstructionThread();

    void start_process(CNormalCloud *, int, int, float, float, QImage *,
                   std::string ,    float, float, float,
                       Vector3f & , Vector3f &, Vector3f &, Vector3f &);
    bool is_abort(){return _abort;}
    void emit_send_back(int i);

signals:
    void send_back(const int val);
    void send_result(CTriangularMesh *);

protected:
    void run();

public slots:
    void stop_process();

private:
    bool    _abort;
    QMutex  _mutex;

    CNormalCloud * _normals;
    int	_octree_d;
    int _solvdiv;
    float _sampl_per_node;
    float _surf_offset;

    Vector3f _mass_center;
    Vector3f _trans;
    Vector3f _scale;
    Vector3f _rotat;

    QImage * _texture;
    std::string _tex_name;
    float _tex_bb_miny;
    float _tex_bb_maxy;
    float _tex_init_angle;

    void undo_transform(CTriMesh * rmesh);
    void redo_transform(CTriMesh * rmesh);
};

#endif // CMESHCONSTRUCTIONTHREAD_H

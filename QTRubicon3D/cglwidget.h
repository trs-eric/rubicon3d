#ifndef CGLWIDGET_H
#define CGLWIDGET_H


#include <QGLWidget>
#include <vector>
#include "cglscene.h"

class CGLWidget : public QGLWidget
{
    Q_OBJECT
public slots:
    void discard_raw_points_of_current_object();

public:
    explicit CGLWidget(QWidget * treew, QWidget *parent = 0);
    ~CGLWidget();

    void import_scene(QString &fname);
    void export_scene(QString &fname);
    void construct_single_scan(std::vector<C3DPoint> & pnts);


    void load_project(QString &fname);
    void save_project(QString &fname);

    void udate_gl();

    CPointCloud * get_raw_points_of_current_object();

    void discard_optim_points_of_current_object();
    CPointCloud * get_optim_points_of_current_object();

    void discard_normal_points_of_current_object();
    CNormalCloud * get_normals_of_current_object();

    void discard_mesh_of_current_object();
    CTriangularMesh * get_mesh_of_current_object();

    std::vector<CGLCompoundObject *> get_list_of_valid_meshes();
    CTriangularMesh * find_mesh_by_its_name(QString);

    void make_new_scene();
    void delete_scene();

    void compute_proper_col_width();

signals:

public slots:
    void on_raw_pnts_computed(CPointCloud*);
    void on_optim_pnts_computed(CPointCloud*);
    void on_normal_pnts_computed(CNormalCloud*);
    void on_mesh_computed(CTriangularMesh *);
    void on_new_mesh_computed(CTriangularMesh *);

    void on_double_click_treew_item(QTreeWidgetItem *, int);
    void on_single_click_treew_item(QTreeWidgetItem *, int);
    void construct_next_scan(std::vector<C3DPoint> * pnts);

    void prepaire_new_scan();
    void construct_next_scan_left(std::vector<C3DPoint> * pnts);
    void construct_next_scan_right(std::vector<C3DPoint> * pnts);
    void construct_next_texture_left(CRawTexture *);
    void construct_next_texture_right(CRawTexture *);

    void clean_current_scan();



protected:
    void initializeGL();
    void paintGL();
    void resizeGL(int width, int height);
    void mousePressEvent(QMouseEvent *event);
    void mouseMoveEvent(QMouseEvent *event);
    void wheelEvent ( QWheelEvent* event);
    // void	closeEvent	( QCloseEvent* event	);
    void keyPressEvent( QKeyEvent *event);

    CGLScene * _scene;
    QPoint _mouseLastPos;
    QWidget * _treew;


};

#endif // CGLWIDGET_H

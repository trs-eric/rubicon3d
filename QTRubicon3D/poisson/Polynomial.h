/*
Copyright (c) 2006, Michael Kazhdan and Matthew Bolitho
All rights reserved.

Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of
conditions and the following disclaimer. Redistributions in binary form must reproduce
the above copyright notice, this list of conditions and the following disclaimer
in the documentation and/or other materials provided with the distribution. 

Neither the name of the Johns Hopkins University nor the names of its contributors
may be used to endorse or promote products derived from this software without specific
prior written permission. 

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO THE IMPLIED WARRANTIES 
OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT
SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
TO, PROCUREMENT OF SUBSTITUTE  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH
DAMAGE.
*/

#ifndef POLYNOMIAL_INCLUDED
#define POLYNOMIAL_INCLUDED

#include <vector>


#ifndef DBL_MAX 
#define DBL_MAX         1.7976931348623158e+308
#endif 



template<int Degree>
class Polynomial{
public:
	double coefficients[Degree+1];

	Polynomial(void)
		{memset(coefficients,0,sizeof(double)*(Degree+1));}
	template<int Degree2>
	Polynomial(const Polynomial<Degree2>& P)
	{
		memset(coefficients,0,sizeof(double)*(Degree+1));
		for(int i=0;i<=Degree && i<=Degree2;i++){coefficients[i]=P.coefficients[i];}
	}

	double operator()(const double& t) const
	{
	double temp=1;
	double v=0;
	for(int i=0;i<=Degree;i++){
		v+=temp*coefficients[i];
		temp*=t;
	}
	return v;
	}

	double integral(const double& tMin,const double& tMax) const
	{
	double v=0;
	double t1,t2;
	t1=tMin;
	t2=tMax;
	for(int i=0;i<=Degree;i++){
		v+=coefficients[i]*(t2-t1)/(i+1);
		if(t1!=-DBL_MAX && t1!=DBL_MAX){t1*=tMin;}
		if(t2!=-DBL_MAX && t2!=DBL_MAX){t2*=tMax;}
	}
	return v;
	}


	int operator == (const Polynomial& p) const
{
	for(int i=0;i<=Degree;i++){if(coefficients[i]!=p.coefficients[i]){return 0;}}
	return 1;
}
	int operator != (const Polynomial& p) const
		{
	for(int i=0;i<=Degree;i++){if(coefficients[i]==p.coefficients[i]){return 0;}}
	return 1;
}
	int isZero(void) const
{
	for(int i=0;i<=Degree;i++){if(coefficients[i]!=0){return 0;}}
	return 1;
}
	void setZero(void){memset(coefficients,0,sizeof(double)*(Degree+1));}

	template<int Degree2>
	Polynomial& operator  = (const Polynomial<Degree2> &p)
	{
		int d=Degree<Degree2?Degree:Degree2;
		memset(coefficients,0,sizeof(double)*(Degree+1));
		memcpy(coefficients,p.coefficients,sizeof(double)*(d+1));
		return *this;
	}

	Polynomial& operator += (const Polynomial& p)
				{
	for(int i=0;i<=Degree;i++){coefficients[i]+=p.coefficients[i];}
	return *this;
}

	Polynomial& operator -= (const Polynomial& p)
				{
	for(int i=0;i<=Degree;i++){coefficients[i]-=p.coefficients[i];}
	return *this;
}

	Polynomial  operator -  (void) const
	{	Polynomial q=*this;
	for(int i=0;i<=Degree;i++){q.coefficients[i]=-q.coefficients[i];}
	return q;
	}
	Polynomial  operator +  (const Polynomial& p) const
	{
	Polynomial q;
	for(int i=0;i<=Degree;i++){q.coefficients[i]=(coefficients[i]+p.coefficients[i]);}
	return q;
	}
	Polynomial  operator -  (const Polynomial& p) const
				{
	Polynomial q;
	for(int i=0;i<=Degree;i++)	{q.coefficients[i]=coefficients[i]-p.coefficients[i];}
	return q;
	}
	template<int Degree2>
	Polynomial<Degree+Degree2>  operator *  (const Polynomial<Degree2>& p) const
	{	Polynomial<Degree+Degree2> q;
	for(int i=0;i<=Degree;i++){for(int j=0;j<=Degree2;j++){q.coefficients[i+j]+=coefficients[i]*p.coefficients[j];}}
	return q;
	}


	Polynomial& operator += (const double& s)
	{
			coefficients[0]+=s;
			return *this;
	}

	Polynomial& operator -= (const double& s)
	{
			coefficients[0]-=s;
	return *this;
	}

	Polynomial& operator *= (const double& s)
	{	for(int i=0;i<=Degree;i++){coefficients[i]*=s;}
	return *this;
	}
	Polynomial& operator /= (const double& s)
	{	for(int i=0;i<=Degree;i++){coefficients[i]/=s;}
	return *this;
	}
	Polynomial  operator +  (const double& s) const
	{	Polynomial<Degree> q=*this;
	q.coefficients[0]+=s;
	return q;
	}
	
	Polynomial  operator -  (const double& s) const
	{
		Polynomial q=*this;
	q.coefficients[0]-=s;
	return q;
	}

	Polynomial  operator *  (const double& s) const
	{	Polynomial q;
	for(int i=0;i<=Degree;i++){q.coefficients[i]=coefficients[i]*s;}
	return q;
}
	Polynomial  operator /  (const double& s) const{
			Polynomial q(this->degree());
	for(int i=0;i<=Degree;i++){q.coefficients[i]=coefficients[i]/s;}
	return q;
}

	Polynomial scale(const double& s) const
	{	Polynomial q=*this;
	double s2=1.0;
	for(int i=0;i<=Degree;i++){
		q.coefficients[i]*=s2;
		s2/=s;
	}
	return q;
	}
	Polynomial shift(const double& t) const
	{	Polynomial<Degree> q;
	for(int i=0;i<=Degree;i++){
		double temp=1;
		for(int j=i;j>=0;j--){
			q.coefficients[j]+=coefficients[i]*temp;
			temp*=-t*j;
			temp/=(i-j+1);
		}
	}
	return q;
	}

	Polynomial<Degree-1> derivative(void) const
	{
		Polynomial<Degree-1> p;
		for(int i=0;i<Degree;i++){p.coefficients[i]=coefficients[i+1]*(i+1);}
		return p;
	}	

	Polynomial<Degree+1> integral(void) const
	{
	Polynomial<Degree+1> p;
	p.coefficients[0]=0;
	for(int i=0;i<=Degree;i++){p.coefficients[i+1]=coefficients[i]/(i+1);}
	return p;
	}

	void printnl(void) const
	{
	for(int j=0;j<=Degree;j++){
		printf("%6.4f x^%d ",coefficients[j],j);
		if(j<Degree && coefficients[j+1]>=0){printf("+");}
	}
	printf("\n");
	}

	Polynomial& addScaled(const Polynomial& p,const double& scale)
		{
    for(int i=0;i<=Degree;i++){coefficients[i]+=p.coefficients[i]*scale;}
	return *this;
}


	static void Negate(const Polynomial& in,Polynomial& out)
	{	out=in;
	for(int i=0;i<=Degree;i++){out.coefficients[i]=-out.coefficients[i];}
	}
	static void Subtract(const Polynomial& p1,const Polynomial& p2,Polynomial& q)
	{
			for(int i=0;i<=Degree;i++){q.coefficients[i]=p1.coefficients[i]-p2.coefficients[i];}
}
	static void Scale(const Polynomial& p,const double& w,Polynomial& q)
		{
	for(int i=0;i<=Degree;i++){q.coefficients[i]=p.coefficients[i]*w;}
}
	static void AddScaled(const Polynomial& p1,const double& w1,const Polynomial& p2,const double& w2,Polynomial& q)
	{
			for(int i=0;i<=Degree;i++){q.coefficients[i]=p1.coefficients[i]*w1+p2.coefficients[i]*w2;}
}
	static void AddScaled(const Polynomial& p1,const Polynomial& p2,const double& w2,Polynomial& q)	
	{
            for(int i=0;i<=Degree;i++){q.coefficients[i]=p1.coefficients[i]+p2.coefficients[i]*w2;}
}

	 void getSolutions(const double& c,std::vector<double>& roots,const double& EPS) const
		 {	double r[4][2];
	int rCount=0;
	roots.clear();
	switch(Degree){
	case 1:
		rCount=Factor(coefficients[1],coefficients[0]-c,r,EPS);
		break;
	case 2:
		rCount=Factor(coefficients[2],coefficients[1],coefficients[0]-c,r,EPS);
		break;
	case 3:
		rCount=Factor(coefficients[3],coefficients[2],coefficients[1],coefficients[0]-c,r,EPS);
		break;
//	case 4:
//		rCount=Factor(coefficients[4],coefficients[3],coefficients[2],coefficients[1],coefficients[0]-c,r,EPS);
//		break;
	default:
		printf("Can't solve polynomial of degree: %d\n",Degree);
	}
	for(int i=0;i<rCount;i++){
		if(fabs(r[i][1])<=EPS){
			roots.push_back(r[i][0]);
//printf("%d] %f\t%f\n",i,r[i][0],(*this)(r[i][0])-c);
		}
	}
	}

	static void AddScaled(const Polynomial& p1,const double& w1,const Polynomial& p2,Polynomial& q)
	{
        for(int i=0;i<=Degree;i++){q.coefficients[i]=p1.coefficients[i]*w1+p2.coefficients[i];}
	}

// void getSolutions(const double& c,std::vector<double>& roots,const double& EPS) const;
		/*{	double r[4][2];
	int rCount=0;
	roots.clear();
	switch(Degree){
	case 1:
		rCount=Factor(coefficients[1],coefficients[0]-c,r,EPS);
		break;
	case 2:
		rCount=Factor(coefficients[2],coefficients[1],coefficients[0]-c,r,EPS);
		break;
	case 3:
		rCount=Factor(coefficients[3],coefficients[2],coefficients[1],coefficients[0]-c,r,EPS);
		break;
//	case 4:
//		rCount=Factor(coefficients[4],coefficients[3],coefficients[2],coefficients[1],coefficients[0]-c,r,EPS);
//		break;
	default:
		printf("Can't solve polynomial of degree: %d\n",Degree);
	}
	for(int i=0;i<rCount;i++){
		if(fabs(r[i][1])<=EPS){
			roots.push_back(r[i][0]);
//printf("%d] %f\t%f\n",i,r[i][0],(*this)(r[i][0])-c);
		}
	}
	}*/
};

/*template<int Degree>
void Polynomial<Degree>::getSolutions(const double& c,std::vector<double>& roots,const double& EPS) const {
	double r[4][2];
	int rCount=0;
	roots.clear();
	switch(Degree){
	case 1:
		rCount=Factor(coefficients[1],coefficients[0]-c,r,EPS);
		break;
	case 2:
		rCount=Factor(coefficients[2],coefficients[1],coefficients[0]-c,r,EPS);
		break;
	case 3:
		rCount=Factor(coefficients[3],coefficients[2],coefficients[1],coefficients[0]-c,r,EPS);
		break;
//	case 4:
//		rCount=Factor(coefficients[4],coefficients[3],coefficients[2],coefficients[1],coefficients[0]-c,r,EPS);
//		break;
	default:
		printf("Can't solve polynomial of degree: %d\n",Degree);
	}
	for(int i=0;i<rCount;i++){
		if(fabs(r[i][1])<=EPS){
			roots.push_back(r[i][0]);
//printf("%d] %f\t%f\n",i,r[i][0],(*this)(r[i][0])-c);
		}
	}
}*/
//#include "Polynomial.inl"
#endif // POLYNOMIAL_INCLUDED

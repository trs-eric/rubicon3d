#-------------------------------------------------
#
# Project created by QtCreator 2014-03-13T19:14:22
#
#-------------------------------------------------

QT       += core gui opengl

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = QTRubicon3D
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    cglwidget.cpp \
    cglobject.cpp \
    cglsampletrivtable.cpp \
    cglscene.cpp \
    cimportmanager.cpp \
    cexportmanager.cpp \
    cglcompoundobject.cpp \
    cpointcloud.cpp \
    cdimreference.cpp \
    cglcamera.cpp \
    cfreeglcamera.cpp \
    cpointoptimizedlg.cpp \
    cpointoptimizethread.cpp \
    graphicutils.cpp \
    mechconstructionutils.cpp \
    cnormalestimationdlg.cpp \
    cnormalcloud.cpp \
    cnormalestimationthread.cpp \
    cmeshconstructiondlg.cpp \
    poisson/Factor.cpp \
    poisson/Geometry.cpp \
    poisson/MarchingCubes.cpp \
    poisson/MultiGridOctest.cpp \
    ctriangularmesh.cpp \
    cmeshconstructionthread.cpp \
    commonutils.cpp \
    ciostate.cpp \
    cphotogrammetrydlg.cpp \
    cphotogrammetrythread.cpp \
    cdevicecontroldlg.cpp \
    driver/fr101.c \
    driver_aux_utils.c \
    ccameraadjustmentdlg.cpp \
    png/lodepng.cpp \
    settings/psettings.cpp \
    img2space/edgedetection.cpp \
    img2space/image2spacemodel.cpp \
    cscanreconstructionthread.cpp \
    cscanreconstructiondlg.cpp \
    ccropdiskdlg.cpp \
    ccropdiskthread.cpp \
    ctexpreview.cpp \
    crawtexture.cpp \
    delaunay/Clarkson-Delaunay.cpp \
    cconvexmeshconstructiondlg.cpp \
    cconvexmeshconstructionthread.cpp \
    cclusteringdecimdlg.cpp \
    cclusteringdecimthread.cpp \
    cmovrotscadlg.cpp

HEADERS  += mainwindow.h \
    messages.h \
    cglwidget.h \
    cglobject.h \
    cglsampletrivtable.h \
    graphicutils.h \
    cglscene.h \
    cimportmanager.h \
    cexportmanager.h \
    cglcompoundobject.h \
    cpointcloud.h \
    cdimreference.h \
    cglcamera.h \
    cfreeglcamera.h \
    cpointoptimizedlg.h \
    cpointoptimizethread.h \
    normals/vertex.h \
    normals/base.h \
    normals/box3.h \
    normals/deprecated_matrix33.h \
    normals/disjoint_set.h \
    normals/lin_algebra.h \
    normals/meshmodel.h \
    normals/normal_extrapolation.h \
    normals/octree.h \
    normals/octree_template.h \
    normals/point3.h \
    meshconstructionutils.h \
    cnormalestimationdlg.h \
    cnormalcloud.h \
    cnormalestimationthread.h \
    cmeshconstructiondlg.h \
    poisson/Allocator.h \
    poisson/BinaryNode.h \
    poisson/Factor.h \
    poisson/FunctionData.h \
    poisson/Geometry.h \
    poisson/Hash.h \
    poisson/MarchingCubes.h \
    poisson/MultiGridOctest.h \
    poisson/MultiGridOctreeData.h \
    poisson/Octree.h \
    poisson/PoissonParam.h \
    poisson/Polynomial.h \
    poisson/PPolynomial.h \
    poisson/SparseMatrix.h \
    poisson/Vector.h \
    poisson/PPolynomialI.h \
    ctriangularmesh.h \
    cmeshconstructionthread.h \
    commonutils.h \
    ciostate.h \
    cphotogrammetrydlg.h \
    cphotogrammetrythread.h \
    cdevicecontroldlg.h \
    driver/WinTypes.h \
    driver/fr101.h \
    driver/ftd2xx.h \
    driver/mt9p006.h \
    driver/types.h \
    driver_aux_utils.h \
    ccameraadjustmentdlg.h \
    png/lodepng.h \
    settings/psettings.h \
    carray2d.h \
    img2space/edgedetection.h \
    img2space/image2spacemodel.h \
    cscanreconstructionthread.h \
    cscanreconstructiondlg.h \
    ccropdiskdlg.h \
    ccropdiskthread.h \
    crawtexture.h \
    ctexpreview.h \
    delaunay/Clarkson-Delaunay.h \
    cconvexmeshconstructiondlg.h \
    cconvexmeshconstructionthread.h \
    cclusteringdecimdlg.h \
    cclusteringdecimthread.h \
    meshlab/mesho.h \
    cmovrotscadlg.h

FORMS    += mainwindow.ui \
    cpointoptimizedlg.ui \
    cnormalestimationdlg.ui \
    cmeshconstructiondlg.ui \
    cphotogrammetrydlg.ui \
    cdevicecontroldlg.ui \
    ccameraadjustmentdlg.ui \
    cscanreconstructiondlg.ui \
    ccropdiskdlg.ui \
    ctexpreview.ui \
    cconvexmeshconstructiondlg.ui \
    cclusteringdecimdlg.ui \
    cmovrotscadlg.ui

OTHER_FILES += \
    ico_new.png

RESOURCES += \
    res.qrc

QMAKE_CXXFLAGS +=-fpermissive -I'../MechLABsrc/vcglib/' -O6

LIBS += -L. -lftd2xx

win32:RC_ICONS += ./icons/rubitech.ico

QMAKE_LIBS      += -lgomp -lpthread
QMAKE_CXXFLAGS  +=-fopenmp -msse

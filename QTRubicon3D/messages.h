#ifndef MESSAGES_H
#define MESSAGES_H

#include <QObject>
#include <QString>

const QString msg_newScene = QObject::tr("Create a new scene");

const QString err_WrongRUBVer = QObject::tr("Wrong version of RUB file.");

const QString err_cannotLoadFile = QObject::tr("Cannot load requested file");

const QString err_cannotSaveFile = QObject::tr("Cannot save requested file");

const QString msg_PointsBeforeAfter = QObject::tr("Points before : %s / Points after : %s");

const QString wrn_NothingOptim = QObject::tr("There are no points to optimize.");

const QString wrn_NothingNormal = QObject::tr("There are no optimum points.");

const QString wrn_NothingMesh = QObject::tr("There are no mesh normals to compute closed mesh.");

const QString wrn_FailedMesh = QObject::tr("For given parameters a mesh cannot be constructed.");

const QString wrn_CannotExportCObject = QObject::tr("Cannot export anything: current object is undefined.");

const QString wrn_CannotExportMesh = QObject::tr("Cannot find surface to be exported.");

const QString wrn_CannotExportOptimXYZ = QObject::tr("Cannot find optim points to be exported.");

const QString wrn_CannotExportRawXYZ = QObject::tr("Cannot find raw points to be exported.");

const QString wrn_DiscardSceneChanges = QObject::tr("Discard current changes in the scene?");

const QString err_DeviceConnection = QObject::tr("Device connection failure");

const QString err_computeProcImage = QObject::tr("Compute processed image first.");

const QString err_loadRawImage = QObject::tr("Load raw image first.");

const QString wrn_RemoveScan = QObject::tr("Are you sure this scan has to be deleted?");

const QString wrn_NoRawPoints = QObject::tr("There are no raw points.");

const QString wrn_NoMesh = QObject::tr("There are no mesh on the selected scan.");


const QString wrn_TexDel = QObject::tr("Pressing this button removes mesh texture.");


#endif // MESSAGES_H

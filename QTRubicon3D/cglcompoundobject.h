#ifndef CGLCOMPOUNDOBJECT_H
#define CGLCOMPOUNDOBJECT_H

#include<QString>
#include <QFont>

#include "cpointcloud.h"
#include "cnormalcloud.h"
#include "ctriangularmesh.h"
#include "ciostate.h"
#include "crawtexture.h"

class QWidget;
class QTreeWidget;
class QTreeWidgetItem;

class CGLCompoundObject : public CIOState
{
public:
    CGLCompoundObject(QWidget * treew, bool select=false, QString ttl=QString(""));
    virtual ~CGLCompoundObject();

    enum ViewingMode{NONE=0, RAW_POINTS=1, OPTIM_POINTS=2, NORMALS=4, MESH=8};

    bool import_raw_points(QString & file);
    bool import_mesh(QString & file);
    void export_mesh(QString & file);
    void export_raw_points(QString & file);
    void export_optim_points(QString & file);

    void delete_raw_points();
    void set_raw_points(CPointCloud * npnts);
    bool set_raw_points(std::vector<C3DPoint> & pnts);
    bool add_raw_points(std::vector<C3DPoint> & pnts);

    CPointCloud * get_raw_points(){return  _raw_points;}

    CPointCloud * get_optim_points(){return _optim_points;}
    void delete_optim_points();
    void set_optim_points(CPointCloud *);

    CNormalCloud *get_normals(){return _normal_points;}
    void delete_normal_points();
    void set_normal_points(CNormalCloud *);

    CTriangularMesh * get_mesh(){return _mesh;}
    void delete_mesh();
    void set_mesh(CTriangularMesh *);


    void glDraw();
    bool set_view_mode(ViewingMode mode);
    void set_mesh_render_mode(CTriangularMesh::RenderingMode rm);

    void update_refdependant_glRepo();
    void update_selection_state(QTreeWidgetItem *, int);
    void update_surface_state(QTreeWidgetItem *, int);
    void update_visability_state(QTreeWidgetItem *, int);
    bool is_update_child_selection_state(QTreeWidgetItem *item, int col);
    bool is_update_root_selection_state(QTreeWidgetItem *, int);
    bool is_remove_state(QTreeWidgetItem *, int);
    void select_object(bool v);
    void update_treew_children();

    bool is_current(){return _is_current;}

    void save(std::ofstream & ofile);
    void load(std::ifstream & ifile);

    void extend_title(QString add);

    void delete_raw_textures();

    void delete_tree_raw_textures_childs();
    void create_tree_raw_textures_childs();


    void add_raw_texture(CRawTexture *);
    void replace_texture(QImage *);


    QString get_title(){return _title;}
    void set_title(QString s){_title = s;}
protected:
    void init_treew_root();
    void init_treew_children();
    void delete_trew_structure();

protected:
    ViewingMode _vmode;
    ViewingMode _prev_vmode;

    CPointCloud * _raw_points;
    CPointCloud * _optim_points;
    CNormalCloud * _normal_points;
    CTriangularMesh * _mesh;
    std::vector<CRawTexture*> _raw_textures;

    QString _title;
    bool _is_current;
    QTreeWidget * _treew;
    QTreeWidgetItem * _root_item;
    QTreeWidgetItem * _rawp_item;
    //QTreeWidgetItem * _raw_tex_item;
    QTreeWidgetItem * _proc_tex_item;
    //QTreeWidgetItem * _proc_tex_subitem;
    //QTreeWidgetItem * _optim_item;
    //QTreeWidgetItem * _norm_item;
    QTreeWidgetItem * _surf_item;

    QFont _normal_font;
    QFont _div_font;
    QFont _sel_font;

};

#endif // CGLCOMPOUNDOBJECT_H

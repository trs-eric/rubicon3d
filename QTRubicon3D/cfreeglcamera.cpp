#include "cfreeglcamera.h"
#include "graphicutils.h"

CFreeGLCamera::CFreeGLCamera()
{
    _tx = 0.0f;
    _ty = 5.0f; // 10
    _tz = 15.0f;

    _focal_len = sqrt(_tx * _tx + _ty * _ty + _tz * _tz);

    _cx = 0;
    _cy = 0;
    _cz = 0;

    _rotx = 0.0f;
    _roty = 90.0f;
    _rotz =-IBGM(asin((_ty - _cy)/_focal_len ));

}

CFreeGLCamera::~CFreeGLCamera()
{
}

void CFreeGLCamera::move_forward   (float v)
{
    if ((_focal_len - v < 1.0) || (_focal_len - v > 100.0)) return;
    _focal_len -= v;

    _tx  = _cx - _focal_len * cos(BGM(_roty)) * cos(BGM(_rotz));
    _ty =  _cy - _focal_len * sin(BGM(_rotz));
    _tz = _cz + _focal_len * sin(BGM(_roty)) * cos(BGM(_rotz)) ;

}

void CFreeGLCamera::move_backward  (float v)
{
    move_forward(-v);
}

void CFreeGLCamera::move_right	(float v)
{
    move_left(-v);
}

void CFreeGLCamera::move_left	(float v)
{
    _tx -= v * sin(BGM(_roty ));
    _tz -= v * cos(BGM(_roty ));

    update_target();
}

void CFreeGLCamera::move_up		(float v)
{
    _ty += v;
    update_target();
}

void CFreeGLCamera::move_down	(float v)
{
    move_up(-v);
}

void CFreeGLCamera::turn_left	(float v)
{
    turn_right(-v);
}

void CFreeGLCamera::turn_right	(float v)
{
    if ( ((_roty - v ) > -179 ) && ((_roty - v)< 179) )
    {
        _roty -= v;
    }
    else if ((_roty - v ) < -179 )
    {
        _roty = 360 + (_roty - v ) ;
    }
    else
    {
        _roty = (_roty - v ) - 360 ;
    }

    float ntx = (cos(BGM(v))*(_tx - _cx) - sin(BGM(v))*(_tz - _cz)) + _cx;
    float ntz = (sin(BGM(v))*(_tx - _cx) + cos(BGM(v))*(_tz - _cz)) + _cz;
    _tx =  ntx;
    _tz =  ntz;
}

void CFreeGLCamera::turn_up	     (float v)
{
    turn_down(-v);
}

void CFreeGLCamera::turn_down    (float v)
{
    if(_rotz - v > -70.0 && (_rotz - v < 70.0) )
    {
        if (_roty < 0 ) v = -v;

        float nty = (sin(BGM(v))*(_tz - _cz) + cos(BGM(v))*(_ty - _cy)) + _cy;
        float ntz = (cos(BGM(v))*(_tz - _cz) - sin(BGM(v))*(_ty - _cy)) + _cz;

        _ty =  nty;
        _tz =  ntz;

        _rotz =-IBGM(asin((_ty - _cy)/_focal_len ));
    }
}



void CFreeGLCamera::update_pos()
{
}

void CFreeGLCamera::set_view()
{
    update_pos();

    gluLookAt(	_tx,	_ty,	 _tz,
                _cx,	_cy,	 _cz,
                _ux,	_uy,	 _uz);
}

void CFreeGLCamera::update_target()
{
    _cx = _tx + _focal_len * cos(BGM(_roty)) * cos(BGM(_rotz));
    _cy = _ty + _focal_len * sin(BGM(_rotz));
    _cz = _tz - _focal_len * sin(BGM(_roty)) * cos(BGM(_rotz));
}

#include "psettings.h"
#include <cmath>
#include "graphicutils.h"
#include <iostream>
#include <QSettings>
#include <QFile>
#include <ctime>


extern "C"
{
#include "driver_aux_utils.h"
}

float CCCDCamera::_focal_legth = 830;
int CCCDCamera::_exposure = 250;
int CCCDCamera::_exposure_tex = 400;

int CCCDCamera::_red_gain = 30;
int CCCDCamera::_gre_gain = 25;
int CCCDCamera::_blu_gain = 30;


QImage * CCCDCamera::defisheye_image(QImage * in)
{

    QSize szold = in->size();
    int xsold   = szold.width();
    int ysold   =  szold.height();
    int cx_corr = xsold * _defish_extend/2;
    int cy_corr = ysold * _defish_extend/2;


    QSize sz = (1 + _defish_extend) * in->size() * _defish_scale;
    int xs = sz.width();
    int ys = sz.height();

    QImage * res_img = new QImage(sz, in->format());

    //std::cout << xs << " " << ys << " " << xsold << " " << ysold << std::endl;
    //std::cout << _defish_extend << " " << cx_corr << " " << cy_corr<< std::endl;

    for(int y = 0; y < ys; y++)
    {
        for(int x =0; x < xs; x++ )
        {

            float magic_dx = 4; // TODO why is this?
            float xx = (float(x)- CGlobalSettings::_delta_x_proc + magic_dx)/_defish_scale - _focal_cx - cx_corr /*- CGlobalSettings::_delta_x_proc*/;
            float yy = float(y)/_defish_scale - _focal_cy - cy_corr + _defish_deltay;
            float r = sqrt(float(xx*xx) + float(yy*yy));

            float rx = _focal_legth*atan(r/_focal_legth) * xx / r +_focal_cx  ;
            float ry = _focal_legth*atan(r/_focal_legth) * yy / r +_focal_cy  ;

            if(rx >= 0 && rx < xsold  && ry >= 0 && ry < ysold )
            {
                //// add nearest interpolation and check result
                int x0 = floor(rx);
                int y0 = floor(ry);
                //int x1 = x0  + 1;
                //int y1 = y0  + 1;

                QRgb p0 = in->pixel(x0, y0);

                res_img->setPixel(x , y , p0);

            }
        }
    }


    QImage * res2_img = new QImage(sz/_defish_scale, in->format());
    for(int y = 0; y < ys/_defish_scale; y++)
    {
        for(int x =0; x < xs/_defish_scale; x++ )
        {
            QRgb p0 = res_img->pixel(2*x,2*y);
            int p0r = qRed(p0);
            int p0g = qGreen(p0);
            int p0b = qBlue(p0);

            QRgb p1 = res_img->pixel(2*x,2*y+1);
            int p1r = qRed(p1);
            int p1g = qGreen(p1);
            int p1b = qBlue(p1);

            QRgb p2 = res_img->pixel(2*x+1,2*y);
            int p2r = qRed(p2);
            int p2g = qGreen(p2);
            int p2b = qBlue(p2);

            QRgb p3 = res_img->pixel(2*x+1,2*y+1);
            int p3r = qRed(p3);
            int p3g = qGreen(p3);
            int p3b = qBlue(p3);

            float nred = ( p0r + p1r + p2r + p3r)/4;
            float ngre = ( p0g + p1g + p2g + p3g)/4;
            float nblu = ( p0b + p1b + p2b + p3b)/4;


            QRgb p =  qRgb(nred, ngre, nblu);
            res2_img->setPixel(x , y , p);
        }
    }

    delete res_img;

    return res2_img;
}



void CCCDCamera::defisheye_image_fast(QImage * in, QImage *& res_img, QImage *& res2_img, int * & defish_map)
{
    QSize szold = in->size();
    const int xsold   =  szold.width();
    const int ysold   =  szold.height();
    const int cx_corr = xsold * _defish_extend/2;
    const int cy_corr = ysold * _defish_extend/2;

    QSize sz = (1 + _defish_extend) * in->size() * _defish_scale;
    int xs = sz.width();
    int ys = sz.height();

    // do we need cleaning ? fill
    if(res_img == NULL)
        res_img = new QImage(sz, in->format());
    //else
    //    res_img->fill(0);

    if( res2_img == NULL)
        res2_img = new QImage(sz/_defish_scale, in->format());

//clock_t msstart,  mffinish;
//msstart = clock();

    if(defish_map == NULL)
    {
        int line_size = xs * ys ;
        defish_map = new int [line_size];

        const float magic_dx = 4;
        const float ax = - CGlobalSettings::_delta_x_proc + magic_dx;
        const float dx = -_focal_cx - cx_corr;

        #pragma omp parallel
        {
            #pragma omp for
            for(int y = 0; y < ys; y++)
            {
                float yy  = float(y)/_defish_scale - _focal_cy - cy_corr + _defish_deltay;
                float yy2 = yy*yy;

                for(int x =0; x < xs; x++)
                {

                    float xx = (float(x)+ax)/_defish_scale + dx ;
                    float r =  sqrt(float(xx*xx) + yy2);
                    float rf = r/_focal_legth;

                    float atan_rf = atan(rf) / rf;

                    float rx =  atan_rf * xx +_focal_cx  ;
                    float ry =  atan_rf * yy +_focal_cy  ;
                    int irx = int(rx);
                    int iry = int(ry);

                    if((irx >= 0) && (irx < xsold) && (iry >= 0) && (iry < ysold) )
                        defish_map[y * xs + x] = xsold * iry + irx;
                    else
                        defish_map[y * xs + x] = 0;
                }
            }
        }

    }

    QRgb * iBits = (QRgb *) in->bits();
    #pragma omp parallel
    {
        #pragma omp for
        for(int y = 0; y < ys; y++)
        {
            QRgb * oYLine   = (QRgb*)res_img->scanLine(y);
            int * def_line = (defish_map + y * xs);
            for(int x =0; x < xs; x++,  oYLine++, def_line++)
            {
                *oYLine = iBits[*def_line];
            }
        }
    }
//mffinish = clock();
//tot1 += (mffinish - msstart);

//msstart = clock();
    int yss = ys/_defish_scale;
    int xss = xs/_defish_scale;

    #pragma omp parallel
    {
        #pragma omp for
        for(int y = 0; y < yss; y++)
        {

            int dy = 2*y;
            QRgb * iYLine     = (QRgb*)res_img->scanLine(dy);
            QRgb * iYLinep1   = (QRgb*)res_img->scanLine(dy+1);
            QRgb * oYLine     = (QRgb*)res2_img->scanLine(y);

            for(int x =0; x < xss; x++,  oYLine++)
            {
                int dx = 2*x;

                QRgb p0 = iYLine[dx];
                int p0r = qRed(p0);
                int p0g = qGreen(p0);
                int p0b = qBlue(p0);

                QRgb p1 = iYLinep1[dx];
                int p1r = qRed(p1);
                int p1g = qGreen(p1);
                int p1b = qBlue(p1);

                QRgb p2 = iYLine[dx+1];
                int p2r = qRed(p2);
                int p2g = qGreen(p2);
                int p2b = qBlue(p2);

                QRgb p3 = iYLinep1[dx+1];
                int p3r = qRed(p3);
                int p3g = qGreen(p3);
                int p3b = qBlue(p3);

                int nred = ( p0r + p1r + p2r + p3r)>>2; // not tested
                int ngre = ( p0g + p1g + p2g + p3g)>>2;
                int nblu = ( p0b + p1b + p2b + p3b)>>2;

                *oYLine =  qRgb(nred, ngre, nblu);
            }
        }
    }

//mffinish = clock();
//tot2 += (mffinish - msstart);

}


void CCCDCamera::difference_image_fast(QImage * dest, QImage * src)
{
    unsigned width  = src->width();
    unsigned height = src->height();
    #pragma omp parallel for
    for (int y = 0; y < height; y++)
    {
        QRgb * oYLine   = (QRgb*)dest->scanLine(y);
        QRgb * iYLine   = (QRgb*)src->scanLine(y);
      for (int x = 0; x < width; x++, iYLine++, oYLine++)
      {
          QRgb vsrc  = *iYLine;
          QRgb vdest = *oYLine;

          int red   = (  qRed(vdest) >   qRed(vsrc))? (  qRed(vdest) -   qRed(vsrc)) : 0;
          int green = (qGreen(vdest) > qGreen(vsrc))? (qGreen(vdest) - qGreen(vsrc)) : 0;
          int blue  = ( qBlue(vdest) >  qBlue(vsrc))? ( qBlue(vdest) -  qBlue(vsrc)) : 0;

          *oYLine = qRgb( red, green,  blue);
      }
    }
}

void CCCDCamera::difference_image(QImage * dest, QImage * src)
{
    unsigned width  = src->width();
    unsigned height = src->height();
    for (int y = 0; y < height; y++)
    {      
      for (int x = 0; x < width; x++ )
      {

          QRgb vsrc  =  src->pixel(x,y);
          QRgb vdest = dest->pixel(x,y);

          int red   = (  qRed(vdest) >   qRed(vsrc))? (  qRed(vdest) -   qRed(vsrc)) : 0;
          int green = (qGreen(vdest) > qGreen(vsrc))? (qGreen(vdest) - qGreen(vsrc)) : 0;
          int blue  = ( qBlue(vdest) >  qBlue(vsrc))? ( qBlue(vdest) -  qBlue(vsrc)) : 0;

          QRgb dp = qRgb( red, green,  blue);
          dest->setPixel(x,y, dp);
      }
    }
}

QImage * CCCDCamera::processed_image(QImage * in)
{
    QSize sz = in->size();

    int xs = sz.width()/2;
    int ys = sz.height()/2;

    QImage * res_img = new QImage(sz/2, in->format());
    for(int y = 0; y < ys; y++)
    {
        for(int x =0; x < xs; x++ )
        {

            // camera is rotated
            QRgb p00 = in->pixel(2*x,   2*y);
            QRgb p10 = in->pixel(2*x+1, 2*y);
            QRgb p01 = in->pixel(2*x,  2*y+1);
            QRgb p11 = in->pixel(2*x+1,2*y+1);


            int r  = qRed (p10);
            int g1 = qGreen(p00);
            int g2 = qGreen(p11);
            int b  = qBlue  (p01);

            QRgb value = qRgb(r, (g1+g2)/2, b);
            if(((x + CGlobalSettings::_delta_x_proc) < xs) &&
               ((x + CGlobalSettings::_delta_x_proc) >= 0))
                res_img->setPixel(x + CGlobalSettings::_delta_x_proc , y, value);
        }
    }
    return res_img;
}

void CCCDCamera::processed_image_fast(QImage * in, QImage *& res_img)
{
    QSize sz = in->size();

    int xs = sz.width()/2;
    int ys = sz.height()/2;

    if(res_img == NULL)
        res_img = new QImage(sz/2, in->format());


    #pragma omp parallel for
    for(int y = 0; y < ys; y++)
    {
        QRgb * rYLine   = (QRgb*)res_img->scanLine(y);
        QRgb * iYLine   = (QRgb*)in->scanLine(2*y);
        QRgb * iYLinep1 = (QRgb*)in->scanLine(2*y+1);

        for(int x =0; x < xs; x++ )
        {
            int dbx = 2*x;

            // camera is rotated
            QRgb p00 = iYLine  [dbx  ];
            QRgb p10 = iYLine  [dbx+1];
            QRgb p01 = iYLinep1[dbx  ];
            QRgb p11 = iYLinep1[dbx+1];

            int r  = qRed  (p10);
            int g1 = qGreen(p00);
            int g2 = qGreen(p11);
            int b  = qBlue (p01);

            QRgb value = qRgb(r, (g1+g2)/2, b);
            int nx = (x + CGlobalSettings::_delta_x_proc);
            if((nx < xs) && (nx >= 0))
                rYLine[nx] = value;
        }
    }

}


float CGlobalSettings::_gauss_sigma_max = 5.0;
float CGlobalSettings::_gauss_sigma = 0.65;

float CGlobalSettings::_lcthresh = 16;
float CGlobalSettings::_min_opt_dist = 1.0;
float CGlobalSettings::_d=180;
float CGlobalSettings::_h= 80;
float CGlobalSettings::_alpha_x=BGM(63);
float CGlobalSettings::_alpha_z=BGM(86);
/// MTP H 86 grad V 63 grad !!! right values !!
float CGlobalSettings::_defish_alpha_x=BGM(63.64);
float CGlobalSettings::_defish_alpha_z=BGM(79.37);
float CGlobalSettings::_delta_x=0;
float CGlobalSettings::_delta_y=0;
float CGlobalSettings::_betta = BGM(30);           // real BGM(35);
float CGlobalSettings::_scan_step_angle = BGM(0.45); // BGM(0.45);
float CGlobalSettings::_delta_x_proc = 8;
float CGlobalSettings::_maximal_radius_pnts=100;

int CGlobalSettings::_left_led_bright  = 20;
int CGlobalSettings::_right_led_bright = 20;


CGlobalSettings::CGlobalSettings()
{
    _ini_name =  QString("./QTRubicon3D.ini");
    load_from_INI();

    /// send exposure to device
    FT_STATUS ftStatus = connectToModule();
    if (ftStatus == FT_OK)
    {
        initCameraRegs();
        writeCameraReg(CCCDCamera::regExposure, CCCDCamera::_exposure);

        writeCameraReg(CCCDCamera::regRed,    CCCDCamera::_red_gain);
        writeCameraReg(CCCDCamera::regGreen1, CCCDCamera::_gre_gain);
        writeCameraReg(CCCDCamera::regGreen2, CCCDCamera::_gre_gain);
        writeCameraReg(CCCDCamera::regBlue,   CCCDCamera::_blu_gain);
    }
    disconnectFromModule();
}

CGlobalSettings::~CGlobalSettings()
{
    save_to_INI();
}

void CGlobalSettings::save_to_INI()
{
    QSettings settings(_ini_name, QSettings::IniFormat);
    settings.setValue("exposure", CCCDCamera::_exposure);
    settings.setValue("exposure_tex", CCCDCamera::_exposure_tex);
    settings.setValue("lcthresh", double(_lcthresh));
    settings.setValue("delta_x_proc", double(_delta_x_proc));

    settings.setValue("redgain", CCCDCamera::_red_gain);
    settings.setValue("gregain", CCCDCamera::_gre_gain);
    settings.setValue("blugain", CCCDCamera::_blu_gain);

    settings.setValue("gausssigm", double(_gauss_sigma));


    //std::cout << "save_to_INI()" << std::endl;
}

void CGlobalSettings::load_from_INI()
{
    QFile inif(_ini_name);
    if(!inif.exists()) return;


    QSettings settings(_ini_name, QSettings::IniFormat);
    CCCDCamera::_exposure = settings.value("exposure",CCCDCamera::_exposure).toInt();
    CCCDCamera::_exposure_tex = settings.value("exposure_tex",CCCDCamera::_exposure_tex).toInt();
    _lcthresh       = settings.value("lcthresh").toFloat();
    _delta_x_proc   = settings.value("delta_x_proc").toFloat();

    CCCDCamera::_red_gain = settings.value("redgain",CCCDCamera::_red_gain).toInt();
    CCCDCamera::_gre_gain = settings.value("gregain",CCCDCamera::_gre_gain).toInt();
    CCCDCamera::_blu_gain = settings.value("blugain",CCCDCamera::_blu_gain).toInt();

    _gauss_sigma = settings.value("gausssigm", double(_gauss_sigma)).toFloat();

}

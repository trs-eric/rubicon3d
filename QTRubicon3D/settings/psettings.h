#ifndef PSETTINGS_H
#define PSETTINGS_H

#include <QImage>

struct CCCDCamera
{
    static float _focal_legth;
    //old image center before defishing and extension
    static const float _focal_cx    = 486;
    static const float _focal_cy    = 648;

    static const float _defish_scale = 2.0;
    static const float _defish_extend = 0.15;
    static const int  _defish_deltay  = 80;
    //static const int  _defish_deltay  = 0;
    static QImage * processed_image(QImage * );
    static void processed_image_fast(QImage * in, QImage *& res_img);

    static QImage * defisheye_image(QImage * );
    static void defisheye_image_fast(QImage * in, QImage *& aux, QImage *& out, int * &);

    static void difference_image(QImage * , QImage * );
    static void difference_image_fast(QImage * , QImage * );

    // exposure for laser images
    static int _exposure;
    // exposure for texture images
    static int _exposure_tex;


    // colors settings
    static int _red_gain;
    static int _gre_gain;
    static int _blu_gain;

    static const int regExposure = 9;
    static const int regRed = 45;
    static const int regGreen1 = 43;
    static const int regGreen2 = 46;
    static const int regBlue = 44;
};

struct CGlobalSettings
{
    // gaussian sigma edge
    static float _gauss_sigma_max;
    static float _gauss_sigma;

    // line color threshold
    static float _lcthresh;

    // optimizing point distance
    static float _min_opt_dist;

    // camera to table rotation ceneter distance (mm)
    static float _d;
    // camera elevation (mm)
    static float _h;

    // horizontal camera FOV (rad)
    static float _alpha_x;
    // vertical camera FOV (rad)
    static float _alpha_z;

    // horizontal camera FOV (rad) - after defishis
    static float _defish_alpha_x;
    // vertical camera FOV (rad) - after defishis
    static float _defish_alpha_z;


    // image center and rotation center horizontal offset (pix)
    static float _delta_x_proc;

    // image center and rotation center horizontal offset (pix)
    static float _delta_x;
    // image center and rotation center vertical offset (pix)
    static float _delta_y;
    // laser angle (rad)
    static float _betta;

    // scanning angle step (rad)
    static float _scan_step_angle;

    // maximal points radius to be in a cloud
    static float _maximal_radius_pnts;

    // left led brightness
    static int _left_led_bright;

    // right led brightness
    static int _right_led_bright;

    // ini file name
    QString _ini_name;

    CGlobalSettings();
    virtual ~CGlobalSettings();
protected:
    void save_to_INI();
    void load_from_INI();


};

#endif // PSETTINGS_H

#ifndef CIMPORTMANAGER_H
#define CIMPORTMANAGER_H

#include <QString>
#include "graphicutils.h"

class CImportManager
{
public:
    CImportManager();

    static const QString importFileTypes;
};

CTriMesh * load_obj_mesh(std::string fname);

#endif // CIMPORTMANAGER_H

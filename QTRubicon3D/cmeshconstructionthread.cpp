#include "cmeshconstructionthread.h"
#include "poisson/MultiGridOctest.h"
#include "ctriangularmesh.h"
#include "cnormalcloud.h"

// reading obj http://www.limegarden.net/2010/03/02/wavefront-obj-mesh-loader/
CMeshConstructionThread::CMeshConstructionThread(QObject *parent) :
    QThread(parent), _normals(NULL), _octree_d(0), _solvdiv(0),
    _sampl_per_node(0), _surf_offset(0), _texture(NULL),_tex_name(""),
    _tex_bb_miny(0), _tex_bb_maxy(0),_tex_init_angle(0)
{
    _abort = false;
}

CMeshConstructionThread::~CMeshConstructionThread()
{
    _mutex.lock();
    _abort = true;
    _mutex.unlock();
    wait();
}


void CMeshConstructionThread::start_process(CNormalCloud * nrmls,
     int octdepth, int solvdiv, float sampl_per_node, float surf_offset,
              QImage * texture, std::string tex_name,  float tex_bb_miny, float tex_bb_maxy, float tex_init_angle,
        Vector3f & mc, Vector3f & tr, Vector3f  & sc, Vector3f & rt)
{
    _abort = false;

    _normals    = nrmls;
    _octree_d   = octdepth;
    _solvdiv    = solvdiv;
    _sampl_per_node = sampl_per_node;
    _surf_offset    = surf_offset;

    _texture = texture;
    _tex_bb_miny = tex_bb_miny;
    _tex_bb_maxy = tex_bb_maxy;
    _tex_init_angle = tex_init_angle;
    _tex_name = tex_name;

    _mass_center = mc;
    _trans = tr;
    _scale = sc;
    _rotat = rt;
    start();
}

void CMeshConstructionThread::stop_process()
{
    _mutex.lock();
    _abort = true;
    _mutex.unlock();
}

void CMeshConstructionThread::emit_send_back(int i)
{
     emit send_back(i);
}

void CMeshConstructionThread::run()
{
    CTriMesh * rmesh = NULL;
    CTriangularMesh * mmesh = NULL;

    try
    {
    std::vector<Point3D<float> > Pts;
    std::vector<Point3D<float> > Nor;

    std::vector<C3DPoint> norm = _normals->get_normal_cpy();
    std::vector<C3DPoint>::iterator it = norm.begin();
    for( ; it != norm.end(); it++)
    {

        Point3D<float> point;
        point.coords[0] = it->_x;
        point.coords[1] = it->_y;
        point.coords[2] = it->_z;

        Point3D<float> normal;
        normal.coords[0] = it->_nx;
        normal.coords[1] = it->_ny;
        normal.coords[2] = it->_nz;

        Pts.push_back(point);
        Nor.push_back(normal);
    }


    PoissonParam pparam;
    pparam.Depth			= _octree_d;
    pparam.SolverDivide		= _solvdiv;
    pparam.SamplesPerNode	= _sampl_per_node;
    pparam.Offset			= _surf_offset;

    CoredVectorMeshData mesh;
    Point3D<float> center;
    float scale;

    bool failed = false;
    int ret= Execute2(pparam, Pts, Nor, mesh, center, scale, this, &failed);
    if(failed)
    {
        emit send_back(0);
        return;
    }

    rmesh = new CTriMesh ;

    mesh.resetIterator();
    int outcore_pnts_count = mesh.outOfCorePointCount() ;
    int intcore_pnts_count = mesh.inCorePoints.size();
    int vm = outcore_pnts_count + intcore_pnts_count ;
    int fm = mesh.triangleCount();

    Point3D<float> p;
    for (int i=0; i < intcore_pnts_count; i++)
    {
        p = mesh.inCorePoints[i];

        C3DPoint xpp;
        xpp._x = p.coords[0]*scale+center.coords[0];
        xpp._y = p.coords[1]*scale+center.coords[1];
        xpp._z = p.coords[2]*scale+center.coords[2];
        rmesh->_points.push_back(xpp);

    }

    for (int ii=0; ii <outcore_pnts_count; ii++)
    {
        mesh.nextOutOfCorePoint(p);

        C3DPoint xpp;
        xpp._x = p.coords[0]*scale+center.coords[0];
        xpp._y = p.coords[1]*scale+center.coords[1];
        xpp._z = p.coords[2]*scale+center.coords[2];
        rmesh->_points.push_back(xpp);
    }

    TriangleIndex tIndex;
    int inCoreFlag;
    for ( i = 0; i < fm; i++)
    {

        mesh.nextTriangle(tIndex,inCoreFlag);
        if(!(inCoreFlag & CoredMeshData::IN_CORE_FLAG[0]))
                tIndex.idx[0]+= intcore_pnts_count;
        if(!(inCoreFlag & CoredMeshData::IN_CORE_FLAG[1]))
                tIndex.idx[1]+=intcore_pnts_count;
        if(!(inCoreFlag & CoredMeshData::IN_CORE_FLAG[2]))
                tIndex.idx[2]+=intcore_pnts_count;

        C3DPointIdx tri;
        tri.pnt_index[0] = tIndex.idx[0];
        tri.pnt_index[1] = tIndex.idx[1];
        tri.pnt_index[2] = tIndex.idx[2];

        rmesh->_triangles.push_back(tri);
    }

    ///////// update mesh normals per face
    std::vector<C3DPointIdx>::iterator itt = rmesh->_triangles.begin();
    for(int fidx = 0 ; itt != rmesh->_triangles.end(); itt++, fidx++)
    {
        rmesh->_points[ itt->pnt_index[0] ]._faceidx.push_back(fidx);
        C3DPoint pnt0 = rmesh->_points[ itt->pnt_index[0] ];

        rmesh->_points[ itt->pnt_index[1] ]._faceidx.push_back(fidx);
        C3DPoint pnt1 = rmesh->_points[ itt->pnt_index[1] ];

        rmesh->_points[ itt->pnt_index[2] ]._faceidx.push_back(fidx);
        C3DPoint pnt2 = rmesh->_points[ itt->pnt_index[2] ];

        itt->tri_normal = compute_normal( pnt2, pnt1, pnt0);
    }

    //////// update mesh normals per vertec
    std::vector<C3DPoint>::iterator itv = rmesh->_points.begin();
    for(; itv != rmesh->_points.end(); itv++)
    {
        itv->_nx =0;
        itv->_ny =0;
        itv->_nz =0;

        std::vector<int>::iterator fit = itv->_faceidx.begin();
        for( ; fit != itv->_faceidx.end(); fit++)
        {

            itv->_nx += rmesh->_triangles[(*fit)].tri_normal._x;
            itv->_ny += rmesh->_triangles[(*fit)].tri_normal._y;
            itv->_nz += rmesh->_triangles[(*fit)].tri_normal._z;
        }
        normalize3(itv->_nx, itv->_ny, itv->_nz);
    }

    delete _normals;


    //////////////// working with a texture
    if(_texture != NULL)
    {
        QImage * teximg = new QImage(_texture->copy());
        undo_transform(rmesh);

        rmesh->_texture = teximg;
        rmesh->_texture_name = _tex_name;

        rmesh->_tex_bb_miny = _tex_bb_miny;
        rmesh->_tex_bb_maxy = _tex_bb_maxy;
        rmesh->_tex_init_angle = _tex_init_angle;


        int tot_pnts = rmesh->_points.size();
        for( int k = 0; k < tot_pnts; k++)
        {

            Vector2f tex_coord;
            float angle_norm = circular_modulo(_tex_init_angle + cPI - atan2(rmesh->_points[k]._x, rmesh->_points[k]._z))/cPI2;

            tex_coord.x = angle_norm;
            tex_coord.y = (rmesh->_points[k]._y - _tex_bb_miny)/(_tex_bb_maxy - _tex_bb_miny);
            if(tex_coord.y > 1)  tex_coord.y = 1;
            if(tex_coord.y < 0) tex_coord.y = 0;

            rmesh->_tex_coords.push_back(tex_coord);
        }

        std::vector<C3DPointIdx>::iterator itt = rmesh->_triangles.begin();
        for( ; itt != rmesh->_triangles.end(); itt++)
        {
            itt->tex_index[0] = itt->pnt_index[0];
            itt->tex_index[1] = itt->pnt_index[1];
            itt->tex_index[2] = itt->pnt_index[2];
        }
        redo_transform(rmesh);
    }
    ///////////////////////////////////////

    mmesh = new CTriangularMesh;
    mmesh->set_mesh(rmesh);
    emit send_result(mmesh);
    emit send_back(100);

    }
    catch(...)
    {
        // the procedure failes
        if(rmesh) delete rmesh;
        if(mmesh) delete mmesh;
        emit send_result(NULL);
        emit send_back(0);
        return;
    }

}


void CMeshConstructionThread::undo_transform(CTriMesh * rmesh)
{

    rmesh->_mass_center.x = _mass_center.x;
    rmesh->_mass_center.y = _mass_center.y;
    rmesh->_mass_center.z = _mass_center.z;

    rmesh->_trans.x = _trans.x;
    rmesh->_trans.y = _trans.y;
    rmesh->_trans.z = _trans.z;
    rmesh->translate(-_mass_center.x -_trans.x,
                     -_mass_center.y -_trans.y,
                     -_mass_center.z -_trans.z);

    rmesh->_rotat.x = _rotat.x;
    rmesh->_rotat.y = _rotat.y;
    rmesh->_rotat.z = _rotat.z;
    rmesh->rotate_rev( BGM(-_rotat.x) ,
                       BGM(-_rotat.y) ,
                       BGM(-_rotat.z) );

    rmesh->_scale.x = _scale.x;
    rmesh->_scale.y = _scale.y;
    rmesh->_scale.z = _scale.z;
    rmesh->scale(  1.0/_scale.x,
                   1.0/_scale.y,
                   1.0/_scale.z);


    rmesh->translate(_mass_center.x ,
                     _mass_center.y ,
                     _mass_center.z );
}

void CMeshConstructionThread::redo_transform(CTriMesh * rmesh)
{

    rmesh->translate(-_mass_center.x ,
                     -_mass_center.y ,
                     -_mass_center.z );

    rmesh->scale(  _scale.x,
                   _scale.y,
                   _scale.z);

    rmesh->rotate( BGM(_rotat.x) ,
                   BGM(_rotat.y) ,
                   BGM(_rotat.z) );

    rmesh->translate(_mass_center.x + _trans.x,
                     _mass_center.y + _trans.y,
                     _mass_center.z + _trans.z);

}

#include "commonutils.h"


QString get_separated_int(int input_number)
{
    QString stringNumber = QString::number(input_number);
    for(int point = 0, i = (stringNumber.lastIndexOf('.') == -1 ? stringNumber.length() : stringNumber.lastIndexOf('.')); i > 0; --i, ++point)
    {
     if(point != 0 && point % 3 == 0)
           stringNumber.insert(i, '.');
    }
    return stringNumber;
}

#ifndef CGLCAMERA_H
#define CGLCAMERA_H

#include "cglobject.h"
#include "cdimreference.h"

class CGLCamera : public CGLObject, public CDimReference
{
public:

    CGLCamera();
    virtual ~CGLCamera();

    virtual void glDraw();
    virtual void set_view();

protected:
    virtual void update_pos() = 0;
    virtual void create_glList();

    // orientation
    GLfloat _cx, _cy, _cz;

    // up_vector
    GLfloat _ux, _uy, _uz;
};

#endif // CGLCAMERA_H

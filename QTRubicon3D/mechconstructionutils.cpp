#include "meshconstructionutils.h"


std::vector<C3DPoint> compute_normals(std::vector<C3DPoint> & points, int neighbors, bool flip, CNormalEstimationThread * cthread)
{

    std::vector<CVertex> vertex_list;

    std::vector<C3DPoint>::iterator it = points.begin();
    for( ; it != points.end(); it++)
    {

        CVertex vertex;
        vertex._coord.x = it->_x;
        vertex._coord.y = it->_y;
        vertex._coord.z = it->_z;

        vertex_list.push_back(vertex);
    }

   bool is_succeeded = false;
   NormalExtrapolation<std::vector<CVertex> >::ExtrapolateNormals
        (vertex_list.begin(), vertex_list.end(), neighbors,  &is_succeeded, cthread);

   std::vector<C3DPoint> normals;

   if(!is_succeeded) return normals; // exit right away

   std::vector<CVertex>::iterator itv = vertex_list.begin();
   for ( ; itv != vertex_list.end(); itv++)
   {
       C3DPoint normal;
       normal._x = itv->_coord.x;
       normal._y = itv->_coord.y;
       normal._z = itv->_coord.z;

       float len = sqrt(itv->_normal.x*itv->_normal.x + itv->_normal.y*itv->_normal.y + itv->_normal.z*itv->_normal.z);
       normal._nx = (flip)?itv->_normal.x/len:-(itv->_normal.x/len);
       normal._ny = (flip)?itv->_normal.y/len:-(itv->_normal.y/len);
       normal._nz = (flip)?itv->_normal.z/len:-(itv->_normal.z/len);
       normals.push_back(normal);
   }
   return normals;
}

